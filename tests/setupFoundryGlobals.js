/**
 * Setting up Foundry Globals
 */

// Seems like auto-import of jest does not work on 'setupFiles'
import { jest } from "@jest/globals";

// Foundry uses Handlebars to render templates
import Handlebars from "handlebars";

import * as fs from "fs";

global.Application = jest.fn(() => {
  return class Application {};
});

/**
 * Transform Foundry's dot declaration for updating objects into an actual object
 *
 * @param {string} dotDeclaration An string with dot declarations (e.g. "foo.bar.value")
 * @param {any} finalValue The value that goes at the very end of the nested object.
 * @returns {{[key: string]: any}} The resulting object.
 */
const _dotDeclarationToObject = (dotDeclaration, finalValue) => {
  const fields = dotDeclaration.split(".");
  // If dotDeclaration comes with no dot we return the resulting object
  if (fields.length === 1) {
    return { [fields[0]]: finalValue };
  }
  // If it has more than one value is nested, so we need to dive
  return {
    [fields[0]]: _dotDeclarationToObject(fields.slice(1).join("."), finalValue),
  };
};

/**
 * We get a dot declaration key and transform it into a nested value in the given object.
 *
 * @param {string} fieldOrFields The field or fields (if it comes with dot declaration).
 * @param {any} value The value to update (actual value at the end of dot declaration).
 * @param {{[key: string]: any}} obj The object where to update this dot declaration.
 * @returns {{[key: string]: any}} The updated object.
 */
const _updateObject = (fieldOrFields, value, obj) => {
  // If there's a dot declaration within the key we need to nest
  if (fieldOrFields.includes(".")) {
    const resultingObj = _dotDeclarationToObject(fieldOrFields, value);
    obj = { ...obj, ...resultingObj };
    // Delete the old key "X.Y.Z"
    delete obj[fieldOrFields];
  } else {
    obj[fieldOrFields] = value;
  }
  return obj;
};

global.Actor = {
  // Since we only use the `create` and the methods on the created actor nothing
  // else is needed within this class
  create: jest.fn().mockImplementation(async (actor, _options) => ({
    ...actor,
    id: "DummyActor",
    update: jest.fn().mockImplementation((/** @param {{[key: string]: any}} valuesToUpdate */ valuesToUpdate) => {
      for (const fieldOrFields in valuesToUpdate) {
        const value = valuesToUpdate[fieldOrFields];
        // Sometime we can update multiple values by giving them
        // as `["value", "system.value", "system.notes.value", ...]`
        for (const fieldToUpdate of fieldOrFields.split(",")) {
          // We transform dot (.) references into objects
          valuesToUpdate = _updateObject(fieldToUpdate, value, valuesToUpdate);
        }
      }
      return {
        ...actor,
        ...valuesToUpdate,
      };
    }),
    delete: jest.fn().mockImplementation(() => {
      return;
    }),
  })),
};

global.Hooks = {
  once: jest.fn(),
  on: jest.fn(),
};

const gameMock = {
  modules: {
    // This is used for checking if 'roll-bonuses-pf1', we don't need that now
    get: jest.fn(() => {
      // eslint-disable-next-line no-unused-labels
      isActive: false;
    }),
  },
  actors: {
    getName: jest.fn().mockImplementation((actorID) => {
      if (actorID === "sbc | Actor Template") {
        return undefined;
      }
      return {};
    }),
  },
  settings: {
    get: jest.fn().mockImplementation((namespace, key) => {
      if (namespace === "core" && key === "language") {
        return "en";
      }
    }),
  },
};

global.game = gameMock;

// FoundryVTT comes with a bunch of helpers for Handlebars.
// We don't need to add all of them, just those that are being used
Handlebars.registerHelper("gt", (a, b) => a > b);
global.renderTemplate = jest
  .fn()
  .mockImplementation(async (/** @type {string} */ template, /** @type {{[key: string]: any}} */ context) => {
    // We don't need to refer to the full folder
    template = template.replace("modules/pf1-statblock-converter/", "");
    const fileContent = fs.readFileSync(template).toString();
    const templateRender = Handlebars.compile(fileContent);
    return templateRender(context);
  });

// jQuery is used within Foundry
global["$"] = jest.fn(() => ({
  css: jest.fn(),
}));

// Seems like `string` is modified by Foundry
global.String.prototype.capitalize = (S) => S;
