import { jest } from "@jest/globals";

import * as sbcDataModule from "../../../scripts/sbcData.js";
import * as sbcParserModule from "../../../scripts/sbcParser.js";
import { sbcUtils } from "../../../scripts/sbcUtils.js";
import { SimpleParser } from "../../../scripts/Parsers/Universal/simple-parser.js";

const sbcData = sbcDataModule.sbcData;

describe("SimpleParser Suite", () => {
  test("empty constructor fails", () => {
    // Due to the nature of the testing framework constructors need to be wrapped
    // withing anonymous functions
    expect(() => new SimpleParser()).toThrow("The 'targetFields'  and 'supportedType' arguments are mandatory.");
  });

  test("only targetFields given", () => {
    expect(() => new SimpleParser([])).toThrow("The 'targetFields'  and 'supportedType' arguments are mandatory.");
  });

  test("invalid type given", () => {
    expect(() => new SimpleParser([], "boolean")).toThrow("Only 'string' or 'number' are valid values.");
  });
});

describe("SimpleParser.parse Suite", () => {
  const parseValueToDocPathSpy = jest.spyOn(sbcParserModule, "parseValueToDocPath");
  const sbcErrorSpy = jest.spyOn(sbcDataModule, "sbcError");

  afterAll(() => {
    parseValueToDocPathSpy.mockRestore();
    sbcErrorSpy.mockRestore();
  });

  beforeEach(async () => {
    await sbcUtils.resetCharacterData();
    // Reset mock calls before each test start
    parseValueToDocPathSpy.mockReset();
    sbcErrorSpy.mockReset();
  });

  // Dummy parser, needs to be adjusted per test
  let parser = new SimpleParser(["name"], "string");

  test("undefined arguments throw error", () => {
    expect(parser.parse()).rejects.toThrow("The arguments 'value' and 'line' are mandatory");
    expect(parser.parse("valueArg")).rejects.toThrow("The arguments 'value' and 'line' are mandatory");
  });

  test("empty value returns false", async () => {
    expect(await parser.parse("", 0)).toBeFalsy();
  });

  test("null value returns false", async () => {
    expect(await parser.parse(null, 0)).toBeFalsy();
    // eslint-disable-next-line no-undef
    expect(processData.errors.length).toBe(1);
  });

  test("null value invokes error correctly", async () => {
    await parser.parse(null, 0);

    expect(sbcErrorSpy).toHaveBeenCalledWith(1, "Parse", "The input null is not of the supported type string", 0);
  });

  test("correct input data returns true", async () => {
    parser = new SimpleParser(["name", "prototypeToken.name"], "string");

    expect(await parser.parse("Goblin", 0)).toBeTruthy();
  });

  test("correct input data invokes parseValueToDocPath with correct values", async () => {
    parser = new SimpleParser(["name", "prototypeToken.name"], "string");
    await parser.parse("Goblin", 0);

    // One call for each element in array argument
    expect(parseValueToDocPathSpy).toHaveBeenCalledTimes(2);
    // Checking all func calls are correctly structured
    expect(parseValueToDocPathSpy.mock.calls[0]).toEqual([sbcData.characterData.actorData, "name", "Goblin"]);
    expect(parseValueToDocPathSpy.mock.calls[1]).toEqual([
      sbcData.characterData.actorData,
      "prototypeToken.name",
      "Goblin",
    ]);
  });

  test("error during parsing returns false", async () => {
    parseValueToDocPathSpy.mockImplementationOnce(() => {
      throw Error("Dummy forced error.");
    });
    // Suppress terminal output for readability
    jest.spyOn(console, "error").mockImplementationOnce(() => {});

    parser = new SimpleParser(["name"], "string");

    expect(await parser.parse("Goblin", 0)).toBeFalsy();
  });

  test("error during parsing calls sbcError and adds error to array of errors", async () => {
    parseValueToDocPathSpy.mockImplementationOnce(() => {
      throw Error("Dummy forced error.");
    });
    // Suppress terminal output for readability
    jest.spyOn(console, "error").mockImplementationOnce(() => {});

    parser = new SimpleParser(["name"], "string");
    let [name, line] = ["Goblin", 0];
    await parser.parse(name, line);

    expect(sbcErrorSpy).toHaveBeenCalledTimes(1);
    expect(sbcErrorSpy).toHaveBeenCalledWith(0, "Parse", `Failed to parse ${name} into name`, line);
  });
});
