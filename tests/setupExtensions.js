// Add all jest-extended matchers
// import * as matchers from "jest-extended";
// expect.extend(matchers);

// Add specific matchers
import { toBeNumber, toHaveBeenCalledExactlyOnceWith, toHaveBeenCalledOnce, toInclude } from "jest-extended";
expect.extend({
  toBeNumber,
  toHaveBeenCalledExactlyOnceWith,
  toHaveBeenCalledOnce,
  toInclude,
});
