// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";
import { sbcUtils } from "./sbcUtils.js";

/* ------------------------------------ */
/* Compendiums and Entities             */

/* ------------------------------------ */
export class CompendiumOps {
  constructor() {
    this.packLookup = {};
    this.packData = {};
    this.packProcessing = false;
  }

  get defaultCompendia() {
    return [...game.packs.keys()].filter(
      (o) =>
        o.startsWith("pf1.") ||
        o.startsWith("pf1e-archetypes") ||
        o.startsWith("pf-content") ||
        o.startsWith("pf1-statblock-converter")
    );
  }

  async registerDefaultCompendia() {
    sbcUtils.log("Registering default compendia...");
    await this.processCompendiums(this.defaultCompendia, { bypassLock: true });
  }

  /**
   * Process the compendiums to cache their contents for later use.
   * This will create a lookup object for each type of entity in the compendiums.
   *
   * @param {string[]|string} packs           Array of pack names to process
   * @param {object} [options]                Additional options
   * @param {boolean} [options.reset]         Whether to reset the cache
   * @param {boolean} [options.bypassLock]    Whether to bypass the lock and process anyway
   * @returns {Promise<void>}                 A Promise that resolves once the compendiums have been processed
   */
  async processCompendiums(packs = [], { reset = false, bypassLock = false } = {}) {
    if (this.packProcessing && !bypassLock) return;
    if (!Array.isArray(packs)) packs = [packs];
    if (packs.length === 0) return;

    this.packProcessing = true;
    if (this.packData && reset) {
      this.packData = {};
      this.packLookup = {};
      await this.registerDefaultCompendia();
    }

    for (const pack of packs) {
      let compendium = await game.packs.get(pack);

      if (compendium) {
        await compendium.getIndex();
        compendium.fuzzyIndex = sbcUtils.sortArrayByName([...compendium.index]);

        // Cache the compendium for later use
        this.packData[pack] = compendium;

        // Create a lookup for the compendium's contents by item (and subtype if available)
        // Each entry will be the item's name as the key and the UUID as the value
        for (const entity of compendium.fuzzyIndex) {
          let type = entity.type;
          let subtype = entity.system?.subType ?? null;

          // Special exception, as the fuzzyIndex seems to not add the subtype for feat feats.
          // I assume it's because that's the default in template.json.
          if (type === "feat" && !subtype) subtype = "feat";

          const packEntity = {
            name: entity.name,
            uuid: entity.uuid,
            classes: entity.system?.associations?.classes ?? [],
            tags: entity.system?.tags ?? [],
            subType: subtype,
          };

          if (!this.packLookup[pack]) this.packLookup[pack] = {};
          if (!this.packLookup[pack][type]) this.packLookup[pack][type] = { items: [] };
          this.packLookup[pack][type]["items"].push(packEntity);

          if (subtype) {
            if (!(subtype in this.packLookup[pack][type])) this.packLookup[pack][type][subtype] = [];
            this.packLookup[pack][type][subtype].push(packEntity);
          }
        }
      }
    }

    this.packProcessing = false;
  }

  isKnownCompendium(packId) {
    return !!this.packLookup[packId] || false;
  }

  /**
   * Generate permutations of an array. Complexity is O(n!).
   * Should be safe up to 7, though you should probably consider something else if you're reaching that high often.
   *
   * @template T
   * @param {T[]} perm - The Array to be generated upon
   * @returns {Array.<T[]>|false} An Array containing all Array permutations or false if failed.
   */
  uniquePermutations(perm) {
    const total = new Set();
    if (perm.length > 7) {
      console.warn("Array too large. Not attempting.", perm);
      return false;
    }

    for (let i = 0; i < perm.length; i = i + 1) {
      const rest = this.uniquePermutations(perm.slice(0, i).concat(perm.slice(i + 1)));

      if (!rest.length) {
        total.add([perm[i]]);
      } else {
        for (let j = 0; j < rest.length; j = j + 1) {
          total.add([perm[i]].concat(rest[j]));
        }
      }
    }
    return [...total];
  }

  /**
   * Searches through compendia quickly using the system generated index caches.
   * Exact matches excluding punctuation and case are prioritized before searching word order permutations.
   *
   * @param {string} searchTerm                       The name of the Document being searched for
   *
   * @param {object} searchOptions                    Provides a filter to limit search to specific packs or Document types
   * @param {Set} [searchOptions.packs]               A Set of packs to search in
   * @param {string} [searchOptions.type]             A Document type to limit which packs are searched in
   * @param {string[]} [searchOptions.itemTypes]      An Item type to limit which form is acceptable
   * @param {string[]} [searchOptions.itemSubTypes]   An Item subtype to limit which form is acceptable
   * @param {string[]} [searchOptions.classes]        The classes the entity belongs to
   * @param {string} [searchOptions.race]             The race the entity has, if any
   *
   * @returns {Promise<string|undefined>}             The found entity or null if not found
   * @private
   */
  async findInCompendia(searchTerm, searchOptions) {
    if (!searchTerm) {
      return undefined;
    }

    let packs;
    if (searchOptions?.packs && searchOptions.packs.length)
      packs = searchOptions.packs.flatMap((o) => game.packs.get(o) ?? []);
    else packs = game.packs.filter((o) => !searchOptions?.type || o.metadata.type == searchOptions.type);
    searchTerm = searchTerm.toLocaleLowerCase();
    let found, uuid;

    let itemTypes = searchOptions.itemTypes;
    let searchArray = [];

    for (const pack of packs) {
      const packId = pack.metadata.id;
      let packData = this.packData[packId];

      if (!packData) {
        sbcUtils.log(`Pack ${packId} not found in cache. Processing...`);
        await this.processCompendiums(packId);
        packData = this.packData[packId];
      }

      for (const type of itemTypes) {
        if (!this.packLookup[packId]?.[type]) continue;

        const subTypes = searchOptions.itemSubTypes ?? Object.keys(this.packLookup[packId][type]);
        for (const subType of subTypes) {
          let arrayData = this.packLookup[packId][type][subType] ?? [];
          arrayData = arrayData.filter((o) => {
            // Filter out any items that don't have the right class, if set
            const classMatch =
              o.classes?.length > 0
                ? searchOptions.classes?.length !== undefined
                  ? o.classes.some((c) => searchOptions.classes.includes(pf1.utils.createTag(c)))
                  : true
                : true;

            // Filter out any items that don't have the right race, if set
            const raceMatch =
              o.tags?.length > 0 && type === "feat" && o.subType === "racial"
                ? searchOptions?.race?.length > 0
                  ? o.tags.some((t) => pf1.utils.createTag(t) === searchOptions.race)
                  : false
                : true;

            return classMatch && raceMatch;
          });
          searchArray = searchArray.concat(arrayData);
        }
      }
    }

    searchArray = sbcUtils.sortArrayByName(searchArray);
    if (searchArray.length > 0) {
      found = pf1.utils.binarySearch(searchArray, searchTerm, (sp, it) => {
        return sp.localeCompare(it.name, undefined, {
          ignorePunctuation: true,
        });
      });
      if (found > -1) {
        uuid = searchArray[found].uuid;
      }
    }

    if (uuid) return uuid;

    let searchMutations = this.uniquePermutations(searchTerm.split(/[ _-]/));
    if (searchMutations) searchMutations = searchMutations.map((o) => o.join(" "));
    else {
      // If array is too long, search for just a reversed version and one that pivots around commas/ semicolons
      searchMutations = [null];
      searchMutations.push(searchTerm.split(/[ _-]/).reverse().join(" "));
      searchMutations.push(
        searchTerm
          .split(/[,;] ?/)
          .reverse()
          .flatMap((o) => o.split(" "))
          .join(" ")
      );
    }

    for (let mut = 1; mut < searchMutations.length; mut++) {
      found = pf1.utils.binarySearch(searchArray, searchMutations[mut], (sp, it) =>
        sp.localeCompare(it.name, undefined, {
          ignorePunctuation: true,
        })
      );
      if (found > -1) {
        uuid = searchArray[found].uuid;
      }
    }

    if (uuid) return uuid;
    return null;
  }

  /**
   * @private
   *
   * @param {object} searchTerm                       The name of the Document being searched for
   * @param {string} [searchTerm.item]                The name being searched for
   * @param {Set|string[]|string} [searchTerm.search] A list of search terms to be searched for
   * @param {string} [searchTerm.name]                The name of the Document being searched for
   * @param {string} [searchTerm.shortName]           The short name of the Document being searched for
   * @param {string} [searchTerm.altName]             The alternative name of the Document being searched for
   * @param {string} [searchTerm.altName2]            The second alternative name of the Document being searched for
   *
   * @param {object} searchOptions                    Provides a filter to limit search to specific packs or Document types
   * @param {Set} [searchOptions.packs]               A Set of packs to search in
   * @param {string} [searchOptions.type]             A Document type to limit which packs are searched in
   * @param {string[]} [searchOptions.itemTypes]      An Item type to limit which form is acceptable
   * @param {string[]} [searchOptions.itemSubTypes]   An Item subtype to limit which form is acceptable
   * @param {string[]} [searchOptions.classes]        The classes the entity belongs to
   * @param {string} [searchOptions.race]             The race the entity has, if any
   *
   * @returns {Promise<Item|null>}                    The found entity or null if not found
   */
  async searchCompendia(searchTerm, searchOptions = {}) {
    if (!searchTerm) {
      return null;
    }

    /**
     * @type {string|undefined}
     */
    let searchResult;
    let foundEntity = {};

    searchOptions = Object.assign(
      {
        packs: [],
        type: "Item",
        itemTypes: [],
        itemSubTypes: null,
        classes: undefined,
      },
      searchOptions
    );

    // Filter invalid and duplicate search terms to avoid unnecessary lookups
    if (!searchTerm.search) {
      searchTerm.search = new Set();
    } else if (typeof searchTerm.search === "string") {
      searchTerm.search = new Set([searchTerm.search]);
    } else if (Array.isArray(searchTerm.search)) {
      searchTerm.search = new Set(searchTerm.search);
    }

    let searchTerms = searchTerm.search;
    searchTerms.add(searchTerm.name).add(searchTerm.shortName).add(searchTerm.altName).add(searchTerm.altName2);
    searchTerms = [...searchTerms].filter((o) => !!o);

    if (searchTerm.item === "Scribe Scroll") console.log(searchOptions);
    for (const searchName of searchTerms) {
      sbcUtils.log(
        `Searching compendium for "${searchName}" (item: ${searchTerm.item}) in packs:`,
        searchOptions.packs
      );
      searchResult = await this.findInCompendia(searchName, searchOptions);
      if (searchResult) break;
    }

    sbcUtils.log(`Search Result for "${searchTerm.item}": `, searchResult || "<none>");

    // Return the searchResult, which either is a clone of the found entity or null
    if (!searchResult) {
      return null;
    }

    foundEntity = await fromUuid(searchResult);

    // Turns out that this doesn't add the core sourceId flag, so we do so manually.
    const itemData = foundEntity.toObject();
    itemData.flags.core = { sourceId: searchResult };
    const item = new Item.implementation(itemData);
    return item;
  }

  /**
   * @param {object} searchTerm                       The name of the Document being searched for
   * @param {Set|string[]|string} [searchTerm.search] A list of search terms to be searched for
   * @param {string} [searchTerm.name]                The name of the Document being searched for
   * @param {string} [searchTerm.shortName]           The short name of the Document being searched for
   * @param {string} [searchTerm.altName]             The alternative name of the Document being searched for
   * @param {string} [searchTerm.altName2]            The second alternative name of the Document being searched for
   *
   * @param {object} searchOptions                    Provides a filter to limit search to specific packs or Document types
   * @param {string} [searchOptions.type]             A Document type to limit which packs are searched in
   * @param {string} [searchOptions.itemType]         An Item type to limit which form is acceptable
   * @param {string[]} [searchOptions.itemTypes]      An Item type to limit which form is acceptable
   * @param {string} [searchOptions.itemSubType]       An Item subtype to limit which form is acceptable
   * @param {string[]} [searchOptions.itemSubTypes]   A list of Item subtypes to limit which form is acceptable
   * @param {string[]} [searchOptions.classes]        The classes the entity belongs to
   * @param {string} [searchOptions.race]             The race the entity has, if any
   *
   * @returns {Promise<Item|null>}                    The found entity or null if not found
   */
  async findEntityInCompendia(searchTerm, searchOptions = {}) {
    if (searchOptions.itemType) {
      searchOptions.itemTypes = Array.isArray(searchOptions.itemType)
        ? searchOptions.itemType
        : [searchOptions.itemType];
      delete searchOptions.itemType;
    }

    if (searchOptions.itemSubType) {
      searchOptions.itemSubTypes = Array.isArray(searchOptions.itemSubType)
        ? searchOptions.itemSubType
        : [searchOptions.itemSubType];
      delete searchOptions.itemSubType;
    }

    const packValues = Object.values(this.packData);

    let systemCompendia = new Set();
    let defaultCompendia = new Set();
    let customCompendia = new Set();

    packValues.forEach((o) => {
      switch (o.metadata.package) {
        case "pf1":
          systemCompendia.add(o.metadata.id);
          break;

        case "pf1e-archetypes":
        case "pf-content":
        case "pf1-statblock-converter":
          defaultCompendia.add(o.metadata.id);
          break;

        default:
          customCompendia.add(o.metadata.id);
          break;
      }
    });

    /** @type {string[]} customCompendiumSettings */
    let customCompendiumSettings =
      game.settings
        .get(window.SBC.config.modData.mod, "customCompendiums")
        ?.split(/[,;]/g)
        .filter((o) => !this.packData[o]) ?? [];

    await this.processCompendiums(customCompendiumSettings);

    const results = await Promise.all([
      await this.searchCompendia(searchTerm, foundry.utils.mergeObject(searchOptions, { packs: customCompendia })),
      await this.searchCompendia(searchTerm, foundry.utils.mergeObject(searchOptions, { packs: defaultCompendia })),
      await this.searchCompendia(searchTerm, foundry.utils.mergeObject(searchOptions, { packs: systemCompendia })),
    ]);

    return results.reduce((acc, val) => acc || val, null);
  }
}
