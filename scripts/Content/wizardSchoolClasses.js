const wizardSchoolClasses = [
  "Abjurer",
  "Conjurer",
  "Diviner",
  "Enchanter",
  "Evoker",
  "Illusionist",
  "Necromancer",
  "Transmuter",
  "Universalist",
];
export default wizardSchoolClasses;
