const sillyGear = {
  "sandals of elvenkind": "Boots of Elvenkind",
  "sawtooth sabre": "Sawtoothed Sabre",
  "resist (?:fire|cold|acid|electric(?:ity)?|sonic)": "Resist Energy",
  "^backpack$": "Backpack, common",
  "pale lavender ioun stone": "pale lavender ellipsoid ioun stone",
  "robes of the archmagi": "robe of the archmagi",
  "^ambrosia$": "Ambrosia (Vial)",
  "^Rations \\((\\d+) days\\)$": "Trail Rations",
  "Winter Blanket": "Blanket",
  "spherewalker staff": "Spherewalker's Staff",
  "coat of resistance": "Cloak of Resistance",
};

export default sillyGear;
