const descriptions = {
  senses: {
    blindsight: "senses.blindsight",
    blindsense: "senses.blindsense",
    darkvision: "senses.darkvision",
    "dragon senses": "senses.dragonSenses",
    greensight: "senses.greensight",
    lifesense: "senses.lifesense",
    tremorsense: "senses.tremorsense",
    "true seeing": "senses.trueSeeing",
    truesight: "senses.truesight",
    thoughtsense: "senses.thoughtsense",
    "low-light": "senses.lowLight",
    mistsight: "senses.mistsight",
    "carrion sense": "senses.carrionSense",
    deathwatch: "senses.deathwatch",
    deepsight: "senses.deepsight",
    minesight: "senses.minesight",
    "water sense": "senses.waterSense",
    scent: "senses.scent",
    "see in darkness": "senses.seeInDarkness",
    "all-around vision": "senses.allAroundVision",
  },
};
export default descriptions;
