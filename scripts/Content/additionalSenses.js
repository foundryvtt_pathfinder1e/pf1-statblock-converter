const additionalSenses = [
  "all-around vision",
  "carrion sense",
  "deepsight",
  "deathwatch",
  "dragon senses",
  "greensight",
  "lifesense",
  "minesight",
  "water sense",
];
export default additionalSenses;
