import { sbcParser } from "./sbcParser.js";
import { sbcUtils } from "./sbcUtils.js";
import { sbcData } from "./sbcData.js";
import { sbcError } from "./sbcError.js";
// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

/* ------------------------------------ */
/* sbcInputDialog                       */
/* Create a modal dialog with           */
/* an input, preview and error area.    */
/* Input is saved in a raw format and   */
/* sent to sbcParser.js to convert to   */
/* workable data.                       */
/* ------------------------------------ */

export class InputDialog extends Application {
  /**
   * @param {sbcTypes.AppData} appData  The data for the actor
   * @param {object} options            Options for the dialog
   */
  constructor(appData, options) {
    super(options);

    /** @type {sbcTypes.ProcessData} */
    this.processData = foundry.utils.mergeObject(sbcData.defaultData, { appData });
    if (appData.statblock) {
      this.processData.input.text = appData.statblock;
      const app = this;
      setTimeout(async () => {
        await app.checkInput(document.getElementById("sbcInput"));
      }, window.SBC.settings.getSetting("inputDelay"));
    }
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "sbcModal",
      template: "modules/pf1-statblock-converter/templates/sbcModal.hbs",
      width: 1024,
      height: 768,
      resizable: true,
      classes: ["sbcModal"],
      popOut: true,
      title: "sbc | Statblock Converter by Fair Strides",
      scrollY: [".sbcInput", ".sbcHighlights", ".sbcPreview"],
    });
  }

  async close(options) {
    if (!this.processData.imported && window.SBC.actor?.id) {
      const actor = game.actors.get(window.SBC.actor.id);
      await actor?.delete();
    }
    window.SBC.app = null;

    super.close(options);
  }

  async getData(options = {}) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;

    /**
     * @typedef {object} data
     * @property {string} input                                         The raw input text
     * @property {string} actorType                                     The type of the actor (PC or NPC)
     * @property {Array<sbcTypes.ErrorData>} errors                     The errors that occured during parsing
     * @property {Array<sbcTypes.ErrorHighlightData>} errorHighlights   The highlights for the errors in the input text
     * @property {string} preview                                       The pre-rendered preview of the parsed data
     * @property {object} progressBar                                   The progress bar data
     * @property {boolean} progressBar.ready                            Whether the actor is ready to be imported
     * @property {string} progressBar.text                              The text to display on the progress bar
     * @property {number} progressBar.width                             The width of the progress bar, as a decimal percentage
     */
    let data = super.getData(options);

    data = foundry.utils.mergeObject(data, {
      input: this.processData.input.text,
      actorType: sbcInstance.actorType,
      choices: {
        0: "NPC",
        1: "PC",
        // 2: "Trap",
        // 3: "Haunt",
        // 4: "Vehicle",
      },
      errors: [],
      errorHighlights: [],
      preview: "",
      progressBar: {
        class: this.processData.progressBar.class,
        ready: this.processData.input?.prepared?.success || false,
        text: this.processData.progressBar.text,
        width: this.processData.progressBar.width,
      },
    });

    // Error Highlights
    // By default, the entire input is shown without any markings
    // The marks are added when errors are processed
    this.processData.input.prepared.data.forEach((line, index) => {
      data.errorHighlights.push({
        value: line,
        messageBefore: "",
        messageAfter: "",
        line: index,
        marked: false,
      });
    });

    // Errors
    let lastId = 0;
    let lastMessage = "";
    let duplicates = 0;
    this.processData.errors.forEach((error, index) => {
      let message = error.errorLabel;
      if (error.level === sbcError.ERRORLEVELS.ERROR || error.level === sbcError.ERRORLEVELS.FATAL) {
        message += ` >> ${error.keyword} failed `;
      }

      message += ` >> ${error.message}`;
      if (message !== lastMessage) {
        lastId = index;
        lastMessage = message;

        // TODO: This will currently only highlight the last error in each line. Figure something smarter out for this
        if (error.line !== -1 && error.value) {
          const lineContent = this.processData.input.prepared.data[error.line];
          const errorValue = error.value.trim();
          const [messageBefore, messageAfter] = lineContent.split(errorValue);

          // Set the error highlight
          data.errorHighlights[error.line] = {
            value: errorValue,
            messageBefore: messageBefore,
            messageAfter: messageAfter,
            line: error.line,
            marked: true,
          };
        }

        data.errors.push({
          message: message,
          duplicates: duplicates,
        });
        // TODO: Figure out how to get this to actually work
        // duplicates = 2;
      } else {
        data.errors[lastId].duplicates = duplicates;
        duplicates++;
      }
    });

    // sbcUtils.log("> There were " + data.errors.length + " issue(s) parsing the provided statblock:");

    // Preview
    if (data.progressBar.ready) {
      data.preview = await renderTemplate("modules/pf1-statblock-converter/templates/sbcPreview.hbs", {
        actor: window.SBC.actor,
        notes: this.processData.notes,
        flags: this.processData.flags,
      });
    }

    // Error Highlighting
    // TBD

    return data;
  }

  /* ------------------------------------ */
  /* eventListeners                       */
  /* ------------------------------------ */
  /**
   * Activate event listeners
   *
   * @param {JQuery<HTMLElement>} html  The html object
   */
  activateListeners(html) {
    super.activateListeners(html);

    let inputArea = document.getElementById("sbcInput");
    let actorTypeToggles = document.getElementsByClassName("actorTypeToggle");
    let app = this;

    /* ------------------------------------ */
    /* ActorType Toggle                     */
    /* ------------------------------------ */
    for (let i = 0; i < actorTypeToggles.length; i++) {
      actorTypeToggles[i].addEventListener("click", this.toggleActorType.bind(this, inputArea));
    }

    /* ------------------------------------ */
    /* Input Area (with delay)              */
    /* ------------------------------------ */
    // Get the input from the textArea
    // delayed by inputDelay so not every keystroke results in a previewGeneration
    inputArea.addEventListener(
      "keyup",
      _debounce(async () => {
        await app.checkInput(inputArea);
      }, window.SBC.settings.getSetting("inputDelay"))
    );

    // Link the scroll event to the backdrop of the highlights
    inputArea.addEventListener("scroll", this.syncScrolls.bind(this));

    /* ------------------------------------ */
    /* Import Button                        */
    /* ------------------------------------ */
    // Get the input from the textArea when sbcImportButton is clicked
    let sbcImportButton = document.getElementById("sbcImportButton");
    sbcImportButton.addEventListener("click", this.import.bind(this));

    // Reset everything
    let sbcResetButton = document.getElementById("sbcResetButton");
    sbcResetButton.addEventListener("click", this.resetSBC.bind(this));
  }

  /**
   * Sync the scroll position of the input area with the highlights area
   *
   * @param {Event} _ev  The event that triggered the function
   * @returns {void}
   */
  syncScrolls(_ev) {
    let inputArea = document.getElementById("sbcInput");
    let backdrop = document.getElementById("sbcBackdrop");

    backdrop.scrollTop = inputArea.scrollTop;
  }

  /**
   * Switch between actor types
   *
   * @param {HTMLElement} inputArea   The input area element
   * @param {Event} ev                The event that triggered the function
   */
  async toggleActorType(inputArea, ev) {
    /** @type {sbcTypes.ConfigData} */
    let config = window.SBC.config;
    // When the actorTypeToggle is clicked, check if there i input and try to generate an updated preview
    sbcUtils.log(
      `Switching between ${config.const.actorType[window.SBC.actorType]} and ${
        config.const.actorType[+ev.target.value]
      } actor type.`
    );

    // Toggle the actor type and recreate the actor
    window.SBC.actorType = ev.target.value;
    window.SBC.configInitialized = false;
    window.SBC.actor = await sbcUtils.reinitActor(window.SBC.actor);

    // Cache the input and reset our data, then reassign the input
    let text = this.processData.input.text;
    const appData = this.processData.appData;
    this.processData = foundry.utils.mergeObject(sbcData.defaultData, { appData });
    this.processData.input.text = text;

    // Now tell the input element to reparse the input
    inputArea.value = text;
    window.SBC.configInitialized = true;
    inputArea.dispatchEvent(new Event("keyup"));
  }

  /**
   * Check the input and try to parse it
   *
   * @param {HTMLElement} inputArea  The input area element
   * @returns {Promise<void>}
   */
  async checkInput(inputArea) {
    if (!window.SBC.configInitialized) return;
    if (window.SBC.isImporting) return;

    // Reset the data and reassign the input
    const appData = this.processData.appData;
    this.processData = foundry.utils.mergeObject(sbcData.defaultData, { appData });
    this.processData.input.text = inputArea.value.trim();

    // Recreate a fresh actor
    window.SBC.actor = await sbcUtils.reinitActor(window.SBC.actor);

    // Check, if there is an input and try to parse that
    if (this.processData.input.text) {
      this.processData.actorReady = false;
      // Prepare and parse the input
      try {
        await sbcParser.prepareInput(this.processData);
      } catch (error) {
        console.error(error);
      }

      try {
        await sbcParser.parseInput(this.processData);
      } catch (error) {
        inputArea.value = this.processData.input.prepared.data.join("\n");
        window.SBC.isImporting = false;
        await this.render();
      }
      inputArea.value = this.processData.input.prepared.data.join("\n");
      // if the input could successly be parsed, generate a new preview
      if (this.processData.input.prepared.success) {
        window.SBC.isImporting = false;
        this.processData.actorReady = true;
        Hooks.callAll("sbc.inputParsed");
        await this.render();
      } else {
        window.SBC.isImporting = false;
        // The input could not be parsed, show the errors
        console.error("Could not parse the input");
        let error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Parse", "Could not parse the input");
        this.processData.errors.push(error);
        await this.render();
      }
    } else {
      // There is no input to generate a preview from, reset sbc
      await this.render();
    }
  }

  /**
   * Import the parsed data
   *
   * @returns {Promise<void>}
   */
  async import() {
    if (window.SBC.isImporting) return;

    console.log("Importing...");
    sbcUtils.log("Finished parsing. Importing...");

    if (!this.processData.input.text) {
      // If there is no Input
      const error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Input", "No Input found");
      this.processData.errors.push(error);
    } else if (!this.processData.actorReady) {
      // If the input could not be parsed correctly
      const error = new sbcError(sbcError.ERRORLEVELS.FATAL, "Import", "Could not create an Actor");
      this.processData.errors.push(error);
    } else {
      // Create a permanent actor using the data from the temporary one
      const actorData = window.SBC.actor.toObject();
      let actorUpdates = pf1.migrations.migrateActorData(actorData, null, { actor: window.SBC.actor });

      // Make sure the actor will be in the correct folder
      actorUpdates.folder = window.SBC.folderId;

      // Fix health if it's off from max after all that.
      // Needs to be done here with real actor since the temporary misbehaves with health.
      const hp = window.SBC.actor.system.attributes.hp;
      if (hp.value !== hp.max) {
        actorUpdates = foundry.utils.mergeObject(actorUpdates, {
          attributes: {
            hp: {
              value: hp.max,
            },
          },
        });
      }

      // Alpha-sort each category of items on the actor
      let itemUpdates = [];
      for (const items of Object.values(window.SBC.actor.itemTypes)) {
        itemUpdates = itemUpdates.concat(
          items.sort((a, b) => a.name.localeCompare(b.name)).map((i, x) => ({ _id: i.id, sort: 112500 + x * 15 }))
        );
      }
      await window.SBC.actor.updateEmbeddedDocuments("Item", itemUpdates);

      // Update the actor
      await window.SBC.actor.update(actorUpdates);

      // Make the actor rest so we can recharge things
      await window.SBC.actor.performRest({ restoreHealth: false });

      this.processData.imported = true;
      await this.close();
    }

    this.render(true);
  }

  /**
   * @returns {Promise<void>}
   */
  async resetSBC() {
    const appData = this.processData.appData;
    this.processData = foundry.utils.mergeObject(sbcData.defaultData, { appData });

    window.SBC.actor = await sbcUtils.reinitActor(window.SBC.actor);
    this.render(true);
  }
}

/* ------------------------------------ */
/* Input Delay                          */
/* ------------------------------------ */
/**
 * Debounce function to delay the input
 *
 * @param {Function} callback   The function to call after the delay
 * @param {number} wait         The delay in ms
 * @returns {Function}          The debounced function
 */
function _debounce(callback, wait) {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      callback.apply(this, args);
    }, wait);
  };
}
