import { InputDialog } from "./sbcInput.js";
import { parserMapping } from "./Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import { escapeForRegex, goldRegex } from "./sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

/**
 * Utility functions for the Statblock Converter.
 */
export class sbcUtils {
  /**
   * Creates a temporary actor for the conversion process.
   * This actor is stored in a hidden WIP folder until they are either imported or the window is closed.
   *
   * @returns {Promise<StoredDocument<Actor>|undefined>}  The temporary actor
   */
  static async createActor() {
    let tempActor = await Actor.create({
      name: "sbc | Actor Template",
      type: window.SBC.config.const.actorType[window.SBC.actorType],
      folder: window.SBC.wipFolderId,
    });

    await this.setTokenData(tempActor);
    return tempActor;
  }

  /* ------------------------------------ */
  /* Resetting and Updating               */

  /* ------------------------------------ */
  /**
   * Re-initializes the actor for the conversion process.
   *
   * @param {Actor} actor      The actor to reinitialize
   * @returns {Promise<Actor>} The reinitialized actor
   */
  static async reinitActor(actor) {
    if (actor) {
      const actorCheck = game.actors.get(actor.id);
      if (actorCheck) {
        sbcUtils.log(`Deleting actor ${actor.name} with ID ${actor.id}.`, undefined, "info");
        await actor.delete();
      }
    }
    // while (game.actors.getName("sbc | Actor Template")) {
    //     await game.actors.getName("sbc | Actor Template").delete();
    // }

    const newActor = await sbcUtils.createActor();
    return newActor;
  }

  /**
   *  Sets up the prototype token with the SBC settings.
   *
   * @param {Actor} actor                 The actor to set the token data for
   * @returns {Promise<Actor|undefined>}  The updated actor or undefined if the actor is not found
   */
  static async setTokenData(actor) {
    let label = actor.type === "character" ? "pc" : "npc";
    return await actor.update({
      prototypeToken: {
        displayName: window.SBC.settings.getSetting("displayName"),
        sight: {
          enabled: window.SBC.settings.getSetting(`${label}sight`),
        },
        disposition: window.SBC.settings.getSetting("disposition"),
        displayBars: window.SBC.settings.getSetting("displayBars"),
        bar1: window.SBC.settings.getSetting("attributeBar1"),
        bar2: window.SBC.settings.getSetting("attributeBar2"),
        brightSight: 0,
      },
    });
  }

  /**
   * Log to the console and errorArea
   *
   * @param {any} message Output to the console.
   * @param {any} [data=null] See {@link https://developer.mozilla.org/en-US/docs/Web/API/console/debug_static#parameters console.debug}
   * @param {"debug"|"error"|"info"|"log"|"warn"} [outputFunc="debug"] The log level by log func call (default "debug").
   *
   * @todo The params should be an object in order to be able to destructure and avoid 'undefined' placeholder.
   */
  static log(message, data = null, outputFunc = "debug") {
    const logFunc = console[outputFunc];

    // Skipping debug log when debug is off
    if (logFunc === "debug" && !window.SBC.settings.getSetting("debug")) {
      return;
    }

    // Attach triggering line to the log
    const stack = "\n" + new Error().stack.split("\n").slice(2, 3).join("\n");
    if (data) logFunc("sbc-pf1 | " + message, data, stack);
    else logFunc("sbc-pf1 | " + message, stack);
  }

  /**
   * Generate a placeholder entity for the given input.
   *
   * @param {sbcTypes.PlaceholderEntityData} input    The input to generate a placeholder entity for
   * @param {number} [_line=-1]                       The line in the statblock the entity was found
   * @returns {Promise<Item>}                         The generated placeholder entity
   */
  static async generatePlaceholderEntity(input, _line = -1) {
    // If the input is NOT found in any of the given compendiums, create a placeholder

    let entityData = {
      name: input.name ? input.name : input.item ? input.item : "undefined",
      type: input.type ? input.type : null,
      changes: input.changes || [],

      // Creature-related
      creatureType: input.creatureType ? input.creatureType : null,
      subTypes: input.subTypes ? input.subTypes : null,
      img: input.img ? input.img : "systems/pf1/icons/skills/yellow_36.jpg",
      size: input.size ? input.size : "med",

      // Gear-related
      subtext: input.subtext ? input.subtext : null,
      currency: input.currency ? input.currency : null,
      price: input.price || 0,
      enhancement: input.enhancement ? input.enhancement : null,
      mwk: input.mwk ? input.mwk : null,
      quantity: input.quantity ? input.quantity : null,

      // Class-related
      wizardClass: input.wizardClass ? input.wizardClass : null,
      suffix: input.suffix ? input.suffix : null,
      archetype: input.archetype ? input.archetype : null,
      level: input.level ? input.level : null,

      // Ability-related
      specialAbilityType: input.specialAbilityType ? input.specialAbilityType : "na",
      desc: input.desc ? input.desc : "sbc | Placeholder",

      // Spell-related
      // WIP
    };

    /**
     * @type {Item}
     */
    let entity = null;

    switch (input.type) {
      case "container":
        entity = new Item.implementation({
          name: entityData.name,
          type: "container",
          system: {
            description: {
              value: "sbc | All currency carried was put into this container.",
            },
            currency: {
              pp: entityData.currency.pp,
              gp: entityData.currency.gp,
              sp: entityData.currency.sp,
              cp: entityData.currency.cp,
            },
            weightReduction: 100,
          },
          img: "systems/pf1/icons/items/inventory/pouch-sealed.jpg",
        });
        break;

      case "feat":
      case "feats":
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: "feat",
          system: {
            description: {
              value:
                "sbc | As " + entityData.name + " could not be found in any compendium, a placeholder was generated.",
            },
          },
          img: entityData.img,
        });
        break;

      case "race":
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: "race",
          system: {
            creatureType: entityData.creatureType,
            description: {
              value: entityData.desc || "sbc | As no playable race was found a placeholder was generated.",
            },
            size: entityData.size,
            subTypes: entityData.subTypes,
          },
          img: entityData.img,
        });
        break;

      case "misc":
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: "feat",
          system: {
            abilityType: entityData.specialAbilityType,
            description: {
              value: entityData.desc,
            },
            changes: entityData.changes,
            subType: "misc",
          },
          img: entityData.img,
        });
        break;

      case "attack":
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: "attack",
          system: {
            description: {
              value: entityData.desc,
            },
            subType: "misc",
          },
          img: entityData.img,
        });
        break;

      case "classFeat":
      case "class-abilities":
        if (entityData.specialAbilityType !== null) {
          entity = new Item.implementation({
            name: entityData.name.capitalize(),
            type: "feat",
            system: {
              abilityType: entityData.specialAbilityType,
              description: {
                value: entityData.desc,
              },
              subType: "classFeat",
            },
            img: entityData.img,
          });
        } else {
          entity = new Item.implementation({
            name: entityData.name.capitalize(),
            type: "feat",
            system: {
              abilityType: "",
              description: {
                value: entityData.desc,
              },
              subType: "classFeat",
            },
            img: entityData.img,
          });
        }
        break;

      case "domains":
      case "mysteries":
      case "oppositions":
      case "spirits":
      case "bloodlines":
      case "patrons":
      case "thassilonianSpecialist":
      case "inquisition":
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: "feat",
          system: {
            description: {
              value: "sbc | As there is no dedicated field for " + entityData.type + ", this placeholder was created.",
            },
            subType: "classFeat",
          },
          img: entityData.img,
        });
        break;

      case "loot": {
        entity = new Item.implementation({
          name: entityData.quantity > 1 ? this.singularize(entityData.name) : entityData.name,
          type: entityData.type,
          system: {
            quantity: entityData.quantity,
            value: entityData.value,
            price: entityData.price,
            description: {
              value:
                "sbc | As " + entityData.name + " could not be found in any compendium, a placeholder was generated.",
            },
          },
          img: entityData.img,
        });
        break;
      }

      default:
        entity = new Item.implementation({
          name: entityData.name.capitalize(),
          type: entityData.type,
          system: {
            description: {
              value:
                "sbc | As " + entityData.name + " could not be found in any compendium, a placeholder was generated.",
            },
          },
          img: entityData.img,
        });
        break;
    }

    return entity;
  }

  /* ------------------------------------ */
  /* Workers                              */

  /* ------------------------------------ */

  /**
   * Removes the `content` from the `line` and returns the cleaned line.
   *
   * @param {string} content  The content to remove from the line
   * @param {string} line     The line to clean
   * @returns {string}        The cleaned line
   */
  static cleanLine(content, line) {
    return line.replace(content, "").trim();
  }

  /**
   * Find first needle occurrence in haystack. Needles ordered longest to shortest
   *
   * @param {Array<string>} needles  The array to search in
   * @param {string} haystack        The string to search for
   * @returns {string|null}          The found element, or null if none found
   */
  static haystackSearch(needles, haystack) {
    if (!haystack) {
      return null;
    }

    needles = needles.sort((a, b) => b.length - a.length);
    haystack = haystack.toLowerCase();
    for (const needle of needles) {
      if (haystack.includes(needle.toLowerCase())) {
        return needle;
      }
    }
    return null;
  }

  /**
   * Find first needle occurrence in haystack. Needles ordered longest to shortest
   *
   * @param {Array<string>} needles  The array to search in
   * @param {string} haystack        The string to search for
   * @returns {string|null}          The found element, or null if none found
   */
  static haystackRegexSearch(needles, haystack) {
    if (!haystack) {
      return null;
    }

    needles = needles.sort((a, b) => b.length - a.length);
    haystack = haystack.toLowerCase();
    for (const needle of needles) {
      if (haystack.match(new RegExp(needle, "i")) !== null) {
        console.log(needle);
        return needle;
      }
    }
    return null;
  }

  /**
   * Find all needle matches in haystack
   *
   * @param {Array<string>} needles   The array of strings to search for
   * @param {string} haystack         The string to search in
   * @returns {Array<string>}         The found elements
   */
  static haystackSearchAll(needles, haystack) {
    if (!haystack) {
      return null;
    }

    const matches = [];
    haystack = haystack.toLowerCase();
    for (const needle of needles) {
      if (haystack.includes(needle.toLowerCase())) {
        matches.push(needle);
      }
    }

    return matches;
  }

  /**
   * Parses a string for subtext in parentheses and returns the base value and the subtext.
   *
   * @param {string} value    The value to parse
   * @returns {Array<string>} The base value and the subtext
   */
  static parseSubtext(value) {
    // Remove punctuation at the end of the input
    let input = value.replace(/(^[,;.: ]*|[,;.: ]+$)/g, "");

    let startSubtextIndex = input.indexOf("(");
    let endSubtextIndex = input.indexOf(")");

    if (endSubtextIndex === -1) {
      input += ")";
      endSubtextIndex = input.indexOf(")");
    }

    if (startSubtextIndex > -1 && endSubtextIndex > startSubtextIndex) {
      let baseValue = input.substring(0, startSubtextIndex).trim();
      let subValue = input.substring(startSubtextIndex + 1, endSubtextIndex).trim();
      let restValues = [];

      // Check, if there is something left and parse that again
      if (endSubtextIndex + 1 < input.length) {
        let rest = input
          .substring(endSubtextIndex + 1)
          .replace(/(^[,;.: ]*|[,;.: ]+$)/g, "")
          .trim();
        restValues = this.parseSubtext(rest);
      }

      if (!Array.isArray(restValues) || !restValues.length) {
        return [baseValue, subValue];
      } else {
        return [baseValue, subValue, restValues];
      }
    } else {
      return [value];
    }
  }

  /**
   * Returns the key of the given value in the object.
   *
   * @param {object} object The object to search in
   * @param {string} value  The value to search for
   * @returns {string}      The key of the value
   */
  static getKeyByValue(object, value) {
    return Object.keys(object).find((key) => object[key].toLowerCase() === value.toLowerCase());
  }

  /**
   * Returns the modifier for a given attribute value
   *
   * @param {number} attribute  The attribute value
   * @returns {number}          The modifier value
   */
  static getModifier(attribute) {
    return Math.floor((attribute - 10) / 2);
  }

  /**
   * Returns the average value of a die roll.
   *
   * @param {number} diceSize The size of the dice
   * @param {number} rate     The rate to use for the average calculation
   * @returns {number}        The average value
   */
  static getDiceAverage(diceSize, rate = 0.5) {
    return (diceSize + 1) * rate;
  }

  /**
   * Returns the total modifier value from the changes on an actor
   *
   * @param {Actor} actor         The actor to look for changes
   * @param {string} target       The change target to look for
   * @param {string|null} type    The type to look for, if any
   * @returns {Promise<number>}   The total modifier value, 0 if none found
   */
  static async getTotalFromChanges(actor, target, type = null) {
    let currentItems = actor.items.contents;

    let changeTotal = 0;
    const actorRollData = actor.getRollData();

    await Promise.all(
      currentItems.map(async (currentItem) => {
        // Check if the item has Changes
        if (currentItem.system.changes) {
          const changes = currentItem.system.changes;
          const itemRollData = currentItem.getRollData();

          let targetChange = changes.find(function (element) {
            if (element.target.toLowerCase() === target.toLowerCase() && (!type || element.type === type)) {
              return element;
            }
          });

          if (targetChange) {
            let processedFormula = null;
            if (/@class\.level/.test(targetChange.formula)) {
              processedFormula = await Roll.fromTerms(Roll.parse(targetChange.formula, itemRollData)).evaluate();
            } else {
              processedFormula = await Roll.fromTerms(Roll.parse(targetChange.formula, actorRollData)).evaluate();
            }

            changeTotal += processedFormula?.total ?? +targetChange.formula;
          }
        }
      })
    );

    return Promise.resolve(changeTotal);
  }

  /**
   * Formats the given string to change formulas to inline rolls.
   *
   * @param {string} string The string to format
   * @returns {string}      The formatted string
   */
  static makeValueRollable(string) {
    return string.replace(/(\d+d\d+)/g, "[[$1]]");
  }

  /**
   * Formats the given string to be in camelCase.
   *
   * @param {string} text The text to camelize
   * @returns {string}    The camelized text
   */
  static camelize(text) {
    if (!text) {
      return text;
    }

    return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function (_match, p1, p2, _offset) {
      if (p2) return p2.toUpperCase();
      return p1.toLowerCase();
    });
  }

  /**
   * Translates a string
   *
   * @param {string} key  The key to translate
   * @param {object} data The data to pass to the translation
   * @returns {string}    The translated string
   */
  static translate(key, data = {}) {
    return game.i18n.format(`SBCPF1.${key}`, data);
  }

  /**
   * Runs the given operation on each item in the list in parallel.
   *
   * @param {Array|object} items         The items to run the operation on
   * @param {Function} operation  The operation to run on each item
   * @param {Array} args          The arguments to pass to the operation (each time)
   * @returns {Promise<Array>}    The results of the operations
   */
  static async runParallelOps(items, operation, args = []) {
    if (Array.isArray(items)) {
      const promises = items.map(async (item, index) => await operation(item, index, ...args));
      return await Promise.all(promises);
    }

    const promises = Object.keys(items).map(async (key) => await operation(items[key], key, ...args));
    return await Promise.all(promises);
  }

  /**
   * Takes a given string and converts all distances and weights in it to the system-defined unit.
   *
   * @param {string} string         The string to be converted
   * @returns {string}              The converted string
   */
  static convertStringUnits(string) {
    // Distance conversions
    const dashFeet = string.matchAll(/(\d+)-foot/g);
    for (const distance of dashFeet) {
      const [length, unit] = pf1.utils.convertDistance(+distance[1]);
      string = string.replace(distance[0], length + "-" + (unit === "m" ? "meter" : "foot"));
    }

    const dashMiles = string.matchAll(/(\d+)-mile/g);
    for (const distance of dashMiles) {
      const [length, unit] = pf1.utils.convertDistance(+distance[1], "mi");
      string = string.replace(distance[0], length + "-" + (unit === "km" ? "kilometer" : "mile"));
    }

    const longFeet = string.matchAll(/(\d+) feet/g);
    for (const distance of longFeet) {
      const [length, unit] = pf1.utils.convertDistance(+distance[1]);
      string = string.replace(distance[0], length + " " + (unit === "m" ? "meters" : "feet"));
    }

    const longMiles = string.matchAll(/(\d+) miles/g);
    for (const distance of longMiles) {
      const [length, unit] = pf1.utils.convertDistance(+distance[1], "mi");
      string = string.replace(distance[0], length + " " + (unit === "km" ? "kilometers" : "miles"));
    }

    const shortFeet = string.matchAll(/(\d+) ft\./g);
    for (const distance of shortFeet) {
      string = string.replace(distance[0], pf1.utils.convertDistance(+distance[1]).join(" "));
    }

    const shortMiles = string.matchAll(/(\d+) mi\b/g);
    for (const distance of shortMiles) {
      string = string.replace(distance[0], pf1.utils.convertDistance(+distance[1], "mi").join(" "));
    }

    // Weight Conversions
    const weightSystem = pf1.utils.getWeightSystem();

    const shortPounds = string.matchAll(/(\d+) lbs?\./g);
    for (const weight of shortPounds) {
      const newWeight = pf1.utils.convertWeight(+weight[1]);
      string = string.replace(
        weight[0],
        newWeight + " " + (weightSystem === "metric" ? "kg" : newWeight !== 1 ? "lbs." : "lb.")
      );
    }

    const longPounds = string.matchAll(/(\d+) pounds?/g);
    for (const weight of longPounds) {
      const newWeight = pf1.utils.convertWeight(+weight[1]);
      string = string.replace(
        weight[0],
        pf1.utils.convertWeight(newWeight) +
          " " +
          (weightSystem === "metric" ? "kilogram" : "pound") +
          (newWeight !== 1 ? "s" : "")
      );
    }

    return string;
  }

  /**
   * Takes a given string and attempts to convert it from plural to singular
   *
   * @param {string} string       The plural string
   * @returns {string}            The singularized string
   */
  static singularize(string) {
    return string
      .replace(/\(\d+\)/g, "")
      .trim()
      .replace(/ies$/, "y")
      .replaceAll("doses of", "dose of")
      .replace(/s$/, "");
  }

  /**
   * Takes a given string and capitalizes all words minus certain exceptions like "of", "and", etc.
   *
   * @param {string} string       The string to capitalize
   * @returns {string}            The capitalized string
   *
   * @todo Cases where the world starts with "(" are not handled correctly
   */
  static capitalize(string) {
    return (string || "")
      .split(" ")
      .map((word) => {
        if (["of", "and", "or", "the", "a", "an"].includes(word)) return word;
        if (word.match(/^[^a-z]/i)) return word.charAt(0) + word.charAt(1).toUpperCase() + word.slice(2);
        return word.charAt(0).toUpperCase() + word.slice(1);
      })
      .join(" ");
  }

  /**
   * Extract a money object from a string
   *
   * @param {string} string                                            The string to extract the money from
   * @returns {{pp: number, gp: number, sp: number, cp: number}|null}  The extracted money object
   */
  static getMoneyFromString(string) {
    const gold = goldRegex().exec(string);
    if (!gold[0]) {
      return null;
    }
    const goldGroups = gold.groups;
    return {
      pp: +(goldGroups.pp || 0),
      gp: +(goldGroups.gp || 0),
      sp: +(goldGroups.sp || 0),
      cp: +(goldGroups.cp || 0),
    };
  }

  /**
   * Convert a money object to gold
   *
   * @param {object} money                          The money object
   * @param {number|null} money.pp                  The platinum pieces
   * @param {number|null} money.gp                  The gold pieces
   * @param {number|null} money.sp                  The silver pieces
   * @param {number|null} money.cp                  The copper pieces
   * @returns {number}                              The total gold value
   */
  static convertMoneyToGold(money) {
    return +(money.pp || 0) * 10 + +(money.gp || 0) + +(money.sp || 0) / 10 + +(money.cp || 0) / 100;
  }

  /**
   * Search a magic ability based on its name and a potential prefix
   *
   * @param {string} abilityName                        The name of the magic ability
   * @param {string} [prefix]                           The prefix to search for
   * @returns {MagicAbility|null}                       The found magic ability or null
   */
  static getMagicAbility(abilityName, prefix) {
    let searchName = prefix ? `${prefix} ${abilityName}` : abilityName;
    searchName = searchName.toLowerCase().replace(/[- ]/g, "");

    for (let key in window.SBC.config.magicalAbilities.Abilities) {
      if (key.toLowerCase() === searchName) {
        return {
          name: key,
          ...window.SBC.config.magicalAbilities.Abilities[key],
        };
      }
    }
    return null;
  }

  /**
   * Process a given formula using either the actor or item data.
   *
   * @param {string} itemFormula  The formula to process
   * @param {import ("../pf1/module/documents/actor/actor-base.mjs").ActorBasePF} actor The actor to process the formula with
   * @param {import ("../pf1/module/documents/item/item-base.mjs").ItemBasePF} item The item to process the formula with
   * @returns {Promise<sbcTypes.ProcessedFormulaResult>}            The processed formula
   */
  static async processFormula(itemFormula, actor, item = null) {
    const actorRollData = actor.getRollData();
    const itemRollData = item?.getRollData();

    let processedFormula = null;
    if (/@class\.level/.test(itemFormula) && itemRollData) {
      processedFormula = await Roll.fromTerms(Roll.parse(itemFormula, itemRollData)).evaluate();
      sbcUtils.log(`Processing ${itemFormula} and got results: `, [
        processedFormula,
        itemRollData,
        Roll.parse(itemFormula, itemRollData),
      ]);
    } else {
      processedFormula = await Roll.fromTerms(Roll.parse(itemFormula, actorRollData)).evaluate();
      sbcUtils.log(`Processing ${itemFormula} and got results: `, [
        processedFormula,
        actorRollData,
        Roll.parse(itemFormula, actorRollData),
      ]);
    }

    return { formula: processedFormula?.formula ?? +itemFormula, total: processedFormula?.total ?? +itemFormula };
  }

  /**
   * Update an item with the associated class, if applicable
   *
   * @param {import("../pf1/module/documents/item/item-feat.mjs").ItemFeatPF} entity The item to update
   * @param {Array<import("../pf1/module/documents/item/item-class.mjs").ItemClassPF>} actorClasses The classes of the actor
   * @returns {Promise<void>}     The updated item
   */
  static async addAssociatedClass(entity, actorClasses) {
    if (entity.system.subType === "classFeat" && entity.system.associations.classes?.length > 0) {
      const itemClasses = entity.system.associations.classes.map((c) => c.toLowerCase());
      actorClasses = actorClasses.map((c) => c.system.tag);
      const classMatch = itemClasses.find((c) => actorClasses.includes(c));

      if (classMatch) await entity.updateSource({ "system.class": classMatch });
    }
  }

  /**
   * Calculate the percentual similarity of two strings based on their Levenshtein Distance
   *
   * @param {string} first     The first string
   * @param {string} second    The second string
   * @returns {number}         The levenshtein distance
   */
  static stringSimilarity(first, second) {
    return 1 - this.levenshteinDistance(first, second) / Math.max(first.length, second.length);
  }

  /**
   * Calculate the levenshtein distance for two strings
   * https://en.wikipedia.org/wiki/Levenshtein_distance
   *
   * @param {string} first     The first string
   * @param {string} second    The second string
   * @returns {number}         The levenshtein distance
   */
  static levenshteinDistance(first, second) {
    if (first === second) return 0;

    if (first.length > second.length) [first, second] = [second, first];

    const _min = (d0, d1, d2, bx, ay) => {
      return d0 < d1 || d2 < d1 ? (d0 > d2 ? d2 + 1 : d0 + 1) : bx === ay ? d1 : d1 + 1;
    };

    let [lengthFirst, lengthSecond] = [first.length, second.length];

    while (lengthFirst > 0 && first.charCodeAt(lengthFirst - 1) === second.charCodeAt(lengthSecond - 1)) {
      lengthFirst--;
      lengthSecond--;
    }

    let offset = 0;
    while (offset < lengthFirst && first.charCodeAt(offset) === second.charCodeAt(offset)) {
      offset++;
    }

    lengthFirst -= offset;
    lengthSecond -= offset;

    if (lengthFirst === 0 || lengthSecond < 3) {
      return lengthSecond;
    }

    let x = 0;
    let y;
    let d0, d1, d2, d3;
    let dd, dy, ay;
    let bx0, bx1, bx2, bx3;
    let vector = [];

    for (y = 0; y < lengthFirst; y++) {
      vector.push(y + 1);
      vector.push(first.charCodeAt(offset + y));
    }

    let len = vector.length - 1;

    for (; x < lengthSecond - 3; ) {
      bx0 = second.charCodeAt(offset + (d0 = x));
      bx1 = second.charCodeAt(offset + (d1 = x + 1));
      bx2 = second.charCodeAt(offset + (d2 = x + 2));
      bx3 = second.charCodeAt(offset + (d3 = x + 3));
      dd = x += 4;
      for (y = 0; y < len; y += 2) {
        dy = vector[y];
        ay = vector[y + 1];
        d0 = _min(dy, d0, d1, bx0, ay);
        d1 = _min(d0, d1, d2, bx1, ay);
        d2 = _min(d1, d2, d3, bx2, ay);
        dd = _min(d2, d3, dd, bx3, ay);
        vector[y] = dd;
        d3 = d2;
        d2 = d1;
        d1 = d0;
        d0 = dy;
      }
    }

    for (; x < lengthSecond; ) {
      bx0 = second.charCodeAt(offset + (d0 = x));
      dd = ++x;
      for (y = 0; y < len; y += 2) {
        dy = vector[y];
        vector[y] = dd = _min(dy, d0, dd, bx0, vector[y + 1]);
        d0 = dy;
      }
    }

    return dd;
  }

  /**
   * Check if the given item is a duplicate of an existing item on the actor
   *
   * @param {import ("../pf1/module/documents/actor/actor-pf.mjs").ActorPF} actor The actor to check for duplicates
   * @param {string} itemType                                                     The type of the item to check for duplicates
   * @param {string} itemSubType                                                  The subType of the item to check for duplicates
   * @param {string} itemName                                                     The name of the item to check for duplicates
   * @param {import ("../pf1/module/documents/item/item-pf.mjs").ItemPF} entity   The entity to check for duplicates
   * @returns {import ("../pf1/module/documents/item/item-pf.mjs").ItemPF|null}   The found duplicate item or null
   */
  static checkForDuplicateItem(actor, itemType, itemSubType, itemName, entity) {
    let currentItems = [];
    if (!itemType || itemType === "all") currentItems = actor.allItems;
    else currentItems = actor.itemTypes[itemType];

    if (itemSubType) currentItems = currentItems.filter((item) => item.system.subType === itemSubType);

    let duplicateTest = new RegExp(`(^${escapeForRegex(itemName.toLowerCase())})`, "i");
    return currentItems.find(
      (i) =>
        duplicateTest.test(i.name.toLowerCase()) ||
        (i.system.tag && entity.system.tag && i.system.tag === entity.system.tag) ||
        pf1.utils.createTag(i.name) === pf1.utils.createTag(entity.name)
    ); // Check first by name and then by tag, if it exists
  }

  /**
   * Returns the number with a sign in front of it.
   *
   * @param {number|string} number  The number to prefix
   * @returns {string}              The prefixed number
   */
  static prefixNumber(number) {
    return ("+" + number).replace(/\+([-+])/g, "$1");
  }

  /**
   * Opens the SBC dialog
   *
   * @param {string} statblock The statblock to open the dialog with
   * @returns {Promise<void>}
   * @async
   */
  static async openSBCDialog(statblock = null) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;

    if (!sbcInstance.configInitialized) return;

    await sbcUtils.initializeSBC();

    if (!sbcInstance.app) {
      sbcUtils.log(sbcUtils.translate("log.inputDialogDoesNotYetExist"));
      sbcInstance.actor = await sbcUtils.createActor();

      sbcInstance.app = new InputDialog({
        folderId: sbcInstance.folderId,
        wipFolderId: sbcInstance.wipFolderId,
        statblock: statblock,
      });

      parserMapping.initMapping(sbcInstance.app);
    }

    sbcUtils.log(sbcUtils.translate("log.openingInputDialog"));
    sbcInstance.app.render(true);

    Hooks.callAll("sbc.started");
  }

  /**
   * Initializes the SBC
   *
   * @returns {Promise<void>}
   * @private
   * @async
   */
  static async initializeSBC() {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    sbcUtils.log("Initializing sbc v" + sbcInstance.config.modData.version);

    const customFolderName = window.SBC.settings.getSetting("importFolder");
    const customWIPFolderName = "SBC_WIP";
    let searchForExistingFolder = await game.folders.find(
      (entry) => entry.name === customFolderName && entry.type === "Actor"
    );
    let searchForExistingWIPFolder = await game.folders.find(
      (entry) => entry.name === customWIPFolderName && entry.type === "Actor"
    );

    // Check, if a custom input folder still exists, as it could have been deleted after changing the module settings
    if (!searchForExistingFolder) {
      const newFolder = await Folder.create({
        name: customFolderName,
        type: "Actor",
        color: "#e76f51",
        parent: null,
      });
      const info = sbcUtils.translate("interfaceMessages.folderCreated");
      ui.notifications.info(info);
      sbcUtils.log(info);
      searchForExistingFolder = newFolder;
    }
    sbcInstance.folderId = searchForExistingFolder.id;

    if (!searchForExistingWIPFolder) {
      const newWIPFolder = await Folder.create({
        name: customWIPFolderName,
        type: "Actor",
        color: "#e76f51",
        parent: searchForExistingFolder,
      });
      sbcInstance.wipFolderId = newWIPFolder.id;
    } else {
      sbcInstance.wipFolderId = searchForExistingWIPFolder.id;
      game.actors
        .filter((actor) => actor.folder?.id === searchForExistingWIPFolder.id)
        .map(async (actor) => await actor.delete());
    }

    // If the default actor is PC, change the value down the line
    sbcInstance.actorType = window.SBC.settings.getSetting("defaultActorType");
    Hooks.callAll("sbc.reset");
  }

  /**
   * Creates a search set from a list of options and a list of classes to mix them with
   *
   * @param {Array<string>} options   The options to create the search set from
   * @param {boolean} includeClasses  Whether to include classes in the search set
   * @param {Array<string>} subClasses The classes array to use instead to include in the search set
   * @returns {Set<string>}           The created search set
   */
  static createSearchSet(options, includeClasses = false, subClasses = []) {
    let searchSet = new Set();
    for (let option of options) {
      if (!option) continue;

      searchSet.add(option);
      if (includeClasses) {
        let classes = subClasses.length
          ? subClasses
          : window.SBC.actor.itemTypes.class.filter((c) => c.system.subType !== "racial");
        if (!subClasses.length) {
          classes = classes
            .map((i) => i.system.tag.substring(0, 3).toUpperCase())
            .concat(classes.map((i) => `${i.system.tag[0]}${i.system.tag[2]}${i.system.tag[3]}`.toUpperCase()))
            .concat(window.SBC.app.processData.characterData.archetypes.map((x) => sbcUtils.capitalize(x)));
        }
        let fighterIndex = classes.indexOf("FIG");
        if (fighterIndex > -1) {
          classes.splice(fighterIndex, 1);
          classes.push("FGT");
        }
        for (let c of classes) {
          searchSet.add(`${option} (${c})`);
        }
      }
    }
    return searchSet;
  }

  /**
   * A locale-safe insertion sort of an Array of Objects, not in place. Ignores punctuation and capitalization.
   * `name` properties in objects will be lowercased.
   *
   * @template T
   * @param {Array.<T & {name: string}>} inputArr - Array to be sorted. Each element must have a name property set
   * @returns {T[]} - New sorted Array
   */
  static sortArrayByName(inputArr) {
    inputArr = foundry.utils.deepClone(inputArr);
    for (const elem of inputArr) {
      elem.name = elem.name.toLocaleLowerCase();
    }
    return pf1.utils.naturalSort(inputArr, "name", { numeric: true, ignorePunctuation: true });
  }
}
