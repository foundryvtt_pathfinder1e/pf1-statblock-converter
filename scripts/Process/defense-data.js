import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";

/* ------------------------------------ */
/* Parser for defense data              */
/* ------------------------------------ */
/**
 * Parses the defense data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise.<boolean>}               `false` if any error is caught.
 */
export async function parseDefense(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING DEFENSE DATA"
    );

  let parsedSubCategories = [];
  processData.notes["defense"] = {};

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      let lineContent = data[line];

      // Parse Normal AC
      if (!parsedSubCategories["acNormal"] && lineContent.toLowerCase().startsWith("ac")) {
        parsedSubCategories["acNormal"] = await parseAC("acNormal", "AC", processData, lineContent, line + startLine);
      }

      // Parse Touch AC
      if (!parsedSubCategories["acTouch"] && lineContent.match(/Touch/i)) {
        parsedSubCategories["acTouch"] = await parseAC("acTouch", "Touch", processData, lineContent, line + startLine);
      }

      // Parse Flat-footed AC
      if (!parsedSubCategories["acFlatFooted"] && lineContent.match(/flat-footed/i)) {
        parsedSubCategories["acFlatFooted"] = await parseAC(
          "acFlatFooted",
          "flat-footed",
          processData,
          lineContent,
          line + startLine
        );
      }

      // Parse AC Types
      if (!parsedSubCategories["acTypes"] && /AC.*?\((.*?)\)/i.test(lineContent)) {
        // if (/^(?:AC[^(]*[(])([^)]*)/i.test(lineContent)) {
        parsedSubCategories["acTypes"] = await parseACTypes(processData, lineContent, line + startLine);
        // }
      }

      // Parse HP and HD
      if (!parsedSubCategories["hp"] && lineContent.toLowerCase().startsWith("hp")) {
        parsedSubCategories["hp"] = await parseHP(lineContent, line + startLine);
      }

      // Parse Saves
      if (!parsedSubCategories["saves"] && lineContent.toLowerCase().startsWith("fort ")) {
        parsedSubCategories["saves"] = await parserSaves(lineContent, line + startLine);
      }

      // Parse Damage Reduction
      if (!parsedSubCategories["dr"] && lineContent.match(/\bDR\b/i)) {
        parsedSubCategories["dr"] = await parseDR(lineContent, line + startLine);
      }

      // Parse Immunities
      if (!parsedSubCategories["immune"] && lineContent.match(/Immune\b/i)) {
        parsedSubCategories["immune"] = await parseImmunities(lineContent, line + startLine);
      }

      // Parse Resistances
      if (!parsedSubCategories["resist"] && lineContent.match(/(?<!abilities\s*)\bResist\b/i)) {
        parsedSubCategories["resist"] = await parseResistances(lineContent, line + startLine);
      }

      // Parse Weaknesses / Vulnerabilities
      if (!parsedSubCategories["weakness"] && lineContent.match(/\bWeaknesses\b/i)) {
        parsedSubCategories["weakness"] = await parseWeaknesses(lineContent, line + startLine);
      }

      // Parse Spell Resistance
      if (!parsedSubCategories["sr"] && lineContent.match(/\bSR\b/i)) {
        parsedSubCategories["sr"] = await parseSR(lineContent, line + startLine);
      }

      // Parse Defensive Abilities
      if (!parsedSubCategories["defensiveAbilities"] && lineContent.match(/Defensive Abilities\b/i)) {
        parsedSubCategories["defensiveAbilities"] = await parserDefensiveAbilities(
          processData,
          lineContent,
          line + startLine
        );
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      const errorMessage = `Parsing the defense data failed at line ${line + startLine}`;
      const error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Defense", errorMessage, line + startLine);
      processData.errors.push(error);

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  sbcUtils.log("RESULT OF PARSING DEFENSE DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories, window.SBC.actor.system);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parse the various AC forms (Normal, Touch, and Flat-footed)
 *
 * @param {string} type                         The type of AC to parse ("acNormal", "acTouch", or "acFlatFooted")
 * @param {string} typeRegex                    The type term to use in regex for this section
 * @param {sbcTypes.ProcessData} processData    Process Data object
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  The result of the parsing (`false` if an error occurred)
 */
async function parseAC(type, typeRegex, processData, lineContent, line) {
  const parserAC = parserMapping.map.defense[type];
  const acRegex = new RegExp(`${typeRegex}\\s*(-?\\d+)`, "i");
  const ac = lineContent.match(acRegex)[1].trim();

  processData.notes.defense[type] = ac;
  processData.characterData.conversionValidation.attributes[type] = +ac;

  return await parserAC.parse(+ac, line);
}

/**
 * Parse the AC Types that appear after the AC totals for the categories.
 * For example, "(+2 natural armor, +1 deflection, +1 dodge)"
 *
 * @param {sbcTypes.ProcessData} processData    Process Data object
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  The result of the parsing (`false` if an error occurred)
 */
async function parseACTypes(processData, lineContent, line) {
  const parserAcTypes = parserMapping.map.defense.acTypes;
  // const acTypes = lineContent.match(/^(?:AC[^(]*[(])([^)]*)/i)[1].trim()
  const acTypes = lineContent.match(/AC.*?\((.*?)\)/i)[1].trim();

  processData.characterData.conversionValidation.attributes["acTypes"] = acTypes;

  return await parserAcTypes.parse(acTypes, line);
}

/**
 * Parse the HP and HD of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseHP(lineContent, line) {
  const parserHp = parserMapping.map.defense.hp;
  const hp = lineContent.match(/^(?:HP)(.*)/i)[1].trim();

  return await parserHp.parse(hp, line);
}

/**
 * Parse the Saves of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parserSaves(lineContent, line) {
  const parserSaves = parserMapping.map.defense.saves;
  const saves = lineContent.match(/^(Fort.*)/i)[1].trim();
  return await parserSaves.parse(saves, line);
}

/**
 * Parse the Damage Reduction of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseDR(lineContent, line) {
  const parserDr = parserMapping.map.defense.dr;
  const dr = lineContent.match(/\bDR\b(.*?)(?=$|Immune|Resist|SR|Weakness)/i)[1].trim();
  return await parserDr.parse(dr, line);
}

/**
 * Parse the Immunities of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseImmunities(lineContent, line) {
  const parserImmune = parserMapping.map.defense.immune;
  const immunities = lineContent.match(/\bImmune\b(.*?)(?=$|Resist|SR|Weakness|DR)/i)[1].trim();
  return await parserImmune.parse(immunities, line);
}

/**
 * Parse the Weaknesses of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseWeaknesses(lineContent, line) {
  const parserWeakness = parserMapping.map.defense.weakness;
  const weaknesses = lineContent.match(/\bWeakness(?:es)?\b(.*?)(?=$|\b(?:Resist|Immune|SR|Weakness|DR)\b)/i)[1].trim();
  return await parserWeakness.parse(weaknesses, line);
}

/**
 * Parse the Resistances of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseResistances(lineContent, line) {
  const parserResist = parserMapping.map.defense.resist;
  const resistances = lineContent.match(/\bResist\b(.*?)(?=$|\b(?:Immune|SR|Weakness|DR)\b)/i)[1].trim();
  return await parserResist.parse(resistances, line);
}

/**
 * Parse the Spell Resistance of the creature.
 *
 * @param {string} lineContent  Content of the line.
 * @param {number} line         Line number
 * @returns {Promise<boolean>}  The result of the parsing (`false` if an error occurred)
 */
async function parseSR(lineContent, line) {
  const parserSr = parserMapping.map.defense.sr;
  const sr = lineContent.match(/\bSR\b(.*?)(?=$|\b(?:Immune|Resist|Weakness|DR)\b)/i)[1].trim();
  return await parserSr.parse(sr, line);
}

/**
 * Parse the Defensive Abilities of the creature.
 *
 * @param {sbcTypes.ProcessData} processData    Process Data object
 * @param {string} lineContent                  Content of the line.
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  The result of the parsing (`false` if an error occurred)
 */
async function parserDefensiveAbilities(processData, lineContent, line) {
  const parserDefensiveAbilities = parserMapping.map.defense.defensiveAbilities;
  const defensiveAbilities = lineContent
    .match(/Defensive Abilities\b\s*(.*?)(?=$|\b(?:Immune|Resist|Weakness|DR|SR)\b)/i)[1]
    .replace(/\s*[,;]+/g, ",")
    .replace(/(,\s*$)/, "")
    .trim();

  const [result, entities] = await parserDefensiveAbilities.parse(defensiveAbilities, line, "class-abilities", [
    "buff",
    "consumable",
    "equipment",
    "feat",
    "loot",
    "weapon",
  ]);
  processData.notes.defense.defensiveAbilities = entities;

  return result;
}
