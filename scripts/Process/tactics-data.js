import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";

/* ------------------------------------ */
/* Parser for tactics data              */
/* ------------------------------------ */
/**
 * Parses the ecology data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
export async function parseTactics(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING TACTICS DATA"
    );

  const parsedSubCategories = [];
  processData.notes["tactics"] = {
    hasTactics: true,
  };

  const parserTactics = parserMapping.map.tactics;

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      const lineContent = data[line];

      // Parse Before Combat
      if (!parsedSubCategories["beforeCombat"] && lineContent.match(/Before Combat/i) !== null) {
        parsedSubCategories["beforeCombat"] = await parseBeforeCombatSection(
          parserTactics,
          processData,
          lineContent,
          startLine + line
        );
      }

      // Parse During Combat
      if (!parsedSubCategories["duringCombat"] && lineContent.match(/During Combat/i) !== null) {
        parsedSubCategories["duringCombat"] = await parseDuringCombatSection(
          parserTactics,
          processData,
          lineContent,
          startLine + line
        );
      }

      // Parse Morale
      if (!parsedSubCategories["morale"] && lineContent.match(/^Morale/i) !== null) {
        parsedSubCategories["morale"] = await parseMoraleSection(
          parserTactics,
          processData,
          lineContent,
          startLine + line
        );
      }

      // Parse Base Statistics
      if (!parsedSubCategories["baseStatistics"] && lineContent.match(/^Base Statistics/i) !== null) {
        parsedSubCategories["baseStatistics"] = await parseBaseStatisticsSection(
          parserTactics,
          processData,
          lineContent,
          startLine + line
        );
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      const errorMessage = `Parsing the tactics data failed at line ${line + startLine} (non-critical)`;
      const error = new sbcError(sbcError.ERRORLEVELS.WARNING, "Parse/Tactics", errorMessage, line + startLine);
      processData.errors.push(error);

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  sbcUtils.log("RESULT OF PARSING TACTICS DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parses the Before Combat section of the statblock.
 *
 * @param {TacticsParser} parserTactics         Tactics parser
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  `true` if successful, `false` otherwise.
 */
async function parseBeforeCombatSection(parserTactics, processData, lineContent, line) {
  const beforeCombat = {
    name: "Before Combat",
    entry: lineContent.match(/^(?:Before Combat)([\s\S]*?)(?=$|During Combat|Morale|Base Statistics)/i)[1],
  };

  processData.notes.tactics.beforeCombat = beforeCombat.entry;
  return await parserTactics.parse(beforeCombat, line);
}

/**
 * Parses the During Combat section of the statblock.
 *
 * @param {import("../Parsers/Misc/tactics-parser.js").default} parserTactics         Tactics parser
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  `true` if successful, `false` otherwise.
 */
async function parseDuringCombatSection(parserTactics, processData, lineContent, line) {
  const duringCombat = {
    name: "During Combat",
    entry: lineContent.match(/^(?:During Combat)([\s\S]*?)(?=$|Morale|Base Statistics)/i)[1],
  };

  processData.notes.tactics.duringCombat = duringCombat.entry;
  return await parserTactics.parse(duringCombat, line);
}

/**
 * Parses the Morale section of the statblock.
 *
 * @param {import("../Parsers/Misc/tactics-parser.js").default} parserTactics         Tactics parser
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  `true` if successful, `false` otherwise.
 */
async function parseMoraleSection(parserTactics, processData, lineContent, line) {
  const morale = {
    name: "Morale",
    entry: lineContent.match(/^(?:Morale)([\s\S]*?)(?=$|Base Statistics)/i)[1],
  };

  processData.notes.tactics.morale = morale.entry;
  return await parserTactics.parse(morale, line);
}

/**
 * Parses the Base Statistics section of the statblock.
 *
 * @param {import("../Parsers/Misc/tactics-parser.js").default} parserTactics         Tactics parser
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content
 * @param {number} line                         Line number
 * @returns {Promise<boolean>}                  `true` if successful, `false` otherwise.
 */
async function parseBaseStatisticsSection(parserTactics, processData, lineContent, line) {
  const baseStatistics = {
    name: "Base Statistics",
    entry: lineContent.match(/^(?:Base Statistics)([\s\S]*?)$/i)[1],
  };

  processData.notes.tactics.baseStatistics = baseStatistics.entry;
  return await parserTactics.parse(baseStatistics, line);
}
