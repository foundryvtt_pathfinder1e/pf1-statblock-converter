import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";

/* ------------------------------------ */
/* Parser for special ability data      */
/* ------------------------------------ */
/**
 * Parses the ecology data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
export async function parseSpecialAbilities(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " +
        processData.parsedCategories +
        "/" +
        processData.foundCategories +
        " >> PARSING SPECIAL ABILITY DATA"
    );

  let parsedSubCategories = [];
  let parsedSubCategoriesCounter = 0;
  processData.notes.specialAbilities = [];
  const parserSpecialAbility = parserMapping.map.specialAbilities;

  // Loop through the lines, and parse out each special ability.
  // Each special ability is its own line in the array, so we send each line to the parser.
  // If we parsed one, we add it to our array of parsed special abilities.
  // Skip the first line, as it contains the section header "Special Abilities"
  for (let line = 1; line < data.length; line++) {
    try {
      const lineContent = data[line];
      const specialAbilityLabel = `specialAbility-${parsedSubCategoriesCounter}`;

      // Parse Special Ability
      if (!parsedSubCategories[specialAbilityLabel]) {
        parsedSubCategories[specialAbilityLabel] = await parserSpecialAbility.parse(lineContent, startLine + line);
        parsedSubCategoriesCounter += +!!parsedSubCategories[specialAbilityLabel];
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      let errorMessage = `Parsing the special abilities failed at line ${line + startLine} (non-critical)`;
      let error = new sbcError(sbcError.ERRORLEVELS.WARNING, "Parse/Special Abilities", errorMessage, line + startLine);
      processData.errors.push(error);

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      // This is non-critical, so parse the rest
      return false;
    }
  }

  // Now that we're done, let's combine all the special abilities for our notes later on.
  //     processData.notes["specialAbilities"].parsedSpecialAbilities = processData.notes["specialAbilities"].parsedSpecialAbilities.join(`

  // `)

  sbcUtils.log("RESULT OF PARSING SPECIAL ABILITY DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}
