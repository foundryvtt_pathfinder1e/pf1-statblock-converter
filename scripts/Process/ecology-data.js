import { sbcUtils } from "../sbcUtils.js";
import { sbcError } from "../sbcError.js";
import { parserMapping } from "../Parsers/parser-mapping.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";

/* ------------------------------------ */
/* Parser for ecology data              */
/* ------------------------------------ */
/**
 * Parses the ecology data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
export async function parseEcology(processData, data, startLine) {
  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING ECOLOGY DATA"
    );

  const parsedSubCategories = [];
  processData.notes["ecology"] = {
    hasEcology: true,
  };

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      const lineContent = data[line];

      const parserEcology = parserMapping.map.ecology;

      // Parse Environment
      if (!parsedSubCategories["environment"] && lineContent.match(/Environment/i)) {
        parsedSubCategories["environment"] = await parseEnvironment(
          parserEcology,
          processData,
          lineContent,
          startLine + line
        );
      }

      // Parse Organization
      if (!parsedSubCategories["organization"] && lineContent.match(/Organization/i)) {
        parsedSubCategories["organization"] = await parseOrganization(
          parserEcology,
          processData,
          lineContent,
          startLine + line
        );
      }

      // Parse Treasure
      if (!parsedSubCategories["treasure"] && lineContent.match(/Treasure/i)) {
        parsedSubCategories["treasure"] = await parseTreasure(
          parserEcology,
          processData,
          lineContent,
          startLine + line
        );
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      const errorMessage = `Parsing the ecology data failed at line ${line + startLine} (non-critical)`;
      const error = new sbcError(sbcError.ERRORLEVELS.WARNING, "Parse/Ecology", errorMessage, line + startLine);
      processData.errors.push(error);

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      // This is non-critical, so parse the rest
      throw err;
    }
  }
  sbcUtils.log("RESULT OF PARSING ECOLOGY DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parses the environment data of the statblock.
 *
 * @param {EcologyParser} parserEcology         Parser for ecology data.
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content to parse.
 * @param {number} line                         Line number.
 * @returns {Promise.<boolean>}                 `true` if parsing was successful.
 */
async function parseEnvironment(parserEcology, processData, lineContent, line) {
  const environment = {
    name: "Environment",
    entry: lineContent.match(/^(?:Environment)([\s\S]*?)(?=$|Organization|Treasure)/i)[1],
  };

  processData.notes.ecology.environment = environment.entry;
  return await parserEcology.parse(environment, line);
}

/**
 * Parses the organization data of the statblock.
 *
 * @param {EcologyParser} parserEcology         Parser for ecology data.
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content to parse.
 * @param {number} line                         Line number.
 * @returns {Promise.<boolean>}                 `true` if parsing was successful.
 */
async function parseOrganization(parserEcology, processData, lineContent, line) {
  const organization = {
    name: "Organization",
    entry: lineContent.match(/(?:Organization)([\s\S]*?)(?=$|Treasure)/i)[1],
  };

  processData.notes.ecology.organization = organization.entry;
  return await parserEcology.parse(organization, line);
}

/**
 * Parses the treasure data of the statblock.
 *
 * @param {EcologyParser} parserEcology         Parser for ecology data.
 * @param {sbcTypes.ProcessData} processData    Process data object.
 * @param {string} lineContent                  Line content to parse.
 * @param {number} line                         Line number.
 * @returns {Promise.<boolean>}                 `true` if parsing was successful.
 */
async function parseTreasure(parserEcology, processData, lineContent, line) {
  const treasure = {
    name: "Treasure",
    entry: lineContent.match(/(?:Treasure)([\s\S]*?)$/i)[1],
  };

  // Check for npc gear
  const hasNPCgear = lineContent.match(/(NPC Gear)/i);

  if (hasNPCgear) {
    const npcGear = lineContent.match(/(?:NPC Gear\s*\()([^)]*)/gi)[0].replace(/NPC Gear\s*\(/i, "");
    processData.treasureParsing.treasureToParse = npcGear;
    processData.treasureParsing.lineToRemove = line;

    const errorMessage = `
        This is treasure and will not be included as items in the actor. If you want to parse these as real items, press here:<br/>
        <input type="button" id="parseTreasureAsGearButton" value="Parse Treasure as Gear"></input>`;

    const error = new sbcError(sbcError.ERRORLEVELS.WARNING, "Parse/Ecology", errorMessage, line);
    processData.errors.push(error);
  }

  processData.notes.ecology.treasure = treasure.entry;
  return await parserEcology.parse(treasure, line);
}
