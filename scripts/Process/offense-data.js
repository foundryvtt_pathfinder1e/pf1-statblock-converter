import { parserMapping } from "../Parsers/parser-mapping.js";
import { sbcError } from "../sbcError.js";
import { sbcUtils } from "../sbcUtils.js";
import SpellBooksParser from "../Parsers/Offense/spellbook-parser.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../sbcTypes.js";
import { classesRegex } from "../sbcRegex.js";

/* ------------------------------------ */
/* Parser for offense data              */
/* ------------------------------------ */

// Setup indices, booleans and arrays for spell-parsing
/**
 * Raw data for spellbooks.
 *
 * @type {{number: sbcTypes.RawSpellBookData}}
 */
let rawSpellBooks = {};
let spellBooksFound = 0;
let currentSpellBook = 0;
const currentSpellBookType = {
  0: "primary",
  1: "secondary",
  2: "tertiary",
  3: "spelllike",
  4: "quinary",
  5: "senary",
  6: "septenary",
  7: "octonary",
};

let startIndexOfSpellBooks = [];
let existingSpellbooks = [];

/**
 * Parses the offense data of the statblock.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {Array.<string>} data               Array of separated data.
 * @param {number} startLine                  Index from where base part starts.
 * @returns {Promise<boolean>}               `false` if any error is caught.
 */
export async function parseOffense(processData, data, startLine) {
  rawSpellBooks = {};
  spellBooksFound = 0;
  currentSpellBook = 0;
  startIndexOfSpellBooks = [];
  existingSpellbooks = [];

  window.SBC.settings.getSetting("debug") &&
    console.groupCollapsed(
      "sbc-pf1 | " + processData.parsedCategories + "/" + processData.foundCategories + " >> PARSING OFFENSE DATA"
    );

  let parsedSubCategories = [];
  processData.notes.offense = {
    speed: [],
    spellBooks: [],
  };

  let meleeData = { text: "", line: 0 };
  let rangedData = { text: "", line: 0 };
  const specialAttacksExist = data.some((line) => /^Special\s+Attacks\b\s*/i.test(line));
  let specialAttackData = { text: "", line: 0 };

  // Loop through the lines
  for (let line = 0; line < data.length; line++) {
    try {
      const lineContent = data[line];

      if (/^Spe{0,2}d\s*/i.test(lineContent)) {
        if (!parsedSubCategories["landSpeed"] && /^Spe{0,2}d\s*\d+/i.test(lineContent)) {
          parsedSubCategories["landSpeed"] = await parseSpeed(lineContent, "land", "^Spe{0,2}d", line + startLine);
        } else {
          parsedSubCategories["landSpeed"] = await parseSpeed("Speed 0 ft.", "land", "^Spe{0,2}d", line + startLine);
        }

        // Parse Swim Speed
        if (!parsedSubCategories["swimSpeed"] && lineContent.match(/\bSwim\s*/i)) {
          parsedSubCategories["swimSpeed"] = await parseSpeed(lineContent, "swim", "\\bSwim", line + startLine);
        }

        // Parse Climb Speed
        if (!parsedSubCategories["climbSpeed"] && lineContent.match(/\bClimb\s*/i)) {
          parsedSubCategories["climbSpeed"] = await parseSpeed(lineContent, "climb", "\\bClimb", line + startLine);
        }

        // Parse Burrow Speed
        if (!parsedSubCategories["burrowSpeed"] && lineContent.match(/\bBurrow\s*/i)) {
          parsedSubCategories["burrowSpeed"] = await parseSpeed(lineContent, "burrow", "\\bBurrow", line + startLine);
        }

        // Parse Fly Speed
        if (!parsedSubCategories["flySpeed"] && lineContent.match(/\bFly\s*/i)) {
          parsedSubCategories["flySpeed"] = await parseSpeed(lineContent, "fly", "\\bFly", line + startLine);
        }
      }

      // Parse Speed Abilities
      // WIP

      // Parse Melee Attacks
      if (!parsedSubCategories["melee"] && lineContent.match(/^Melee\s*/i)) {
        meleeData = await parseAttackLine(lineContent, "Melee", data[line + 1], line + startLine);

        // Record for our notes
        processData.notes.offense.melee = meleeData.text;

        // Process later after Special Attacks
        if (!specialAttacksExist) {
          // Process Weapon Groups before attacks
          await processWeaponGroups(processData);

          parsedSubCategories["melee"] = await parseAttacks(meleeData, "mwak");
        }
      }

      // Parse Ranged Attacks
      if (!parsedSubCategories["ranged"] && lineContent.match(/^Ranged\s*/i)) {
        // Find and cache the range data for later use
        rangedData = await parseAttackLine(lineContent, "Ranged", data[line + 1], line + startLine);

        // Record for our notes
        processData.notes.offense.ranged = rangedData.text;

        // Process later after Special Attacks
        if (!specialAttacksExist) {
          // Process Weapon Groups before attacks
          await processWeaponGroups(processData);

          parsedSubCategories["ranged"] = await parseAttacks(rangedData, "rwak");
        }
      }

      // Parse Special Attacks
      if (!parsedSubCategories["specialAttacks"] && lineContent.match(/^Special\s+Attacks\b\s*/i)) {
        specialAttackData.text = lineContent;
        specialAttackData.line = line + startLine;
      }

      // Parse Space, Reach and StatureparserStature
      if (!parsedSubCategories["spaceStature"]) {
        if (/^Space\b.*\bReach\b/i.test(lineContent)) {
          // This may overwrite space and reach that was automatically derived by creature size,
          // which in theory should be fine, i guess

          const parserStature = parserMapping.map.offense.stature;
          const [spaceResult, space] = await parseSpace(lineContent, line + startLine);
          let [reach, reachContext] = await parseReach(lineContent);

          processData.notes.offense.space = pf1.utils.convertDistance(+space).join(" ");
          processData.notes.offense.reach = pf1.utils.convertDistance(+reach).join(" ");
          processData.notes.offense.reachContext = sbcUtils.convertStringUnits(reachContext);

          // Foundry PF1 actor has no field for "reach", so try to derive the "stature" from reach
          // In 90% of all cases this should be "tall"
          const stature = +reach < +space ? "long" : "tall";

          parsedSubCategories["spaceReach"] = {
            space: spaceResult,
            stature: await parserStature.parse(stature, line + startLine),
          };
        }
      }

      /* Parse Spell-Like Abilities
       *
       * Collate all lines that are part of the Spell-Like Abilities,
       * by putting all lines found after the keyword ...
       * ... up until the end of the section or
       * ... up until the next keyword
       * into an array
       */
      if (!parsedSubCategories["spellLikeAbilities"] && lineContent.match(/Spell-Like\s+Abilities\b\s*/i)) {
        startSpellbookProcessing(processData, lineContent, line, true, false);
        sbcUtils.log(`Spell-Like Abilities found: ${currentSpellBook}, ${startIndexOfSpellBooks[currentSpellBook]}`);
      }

      // Parse Psychic Magic
      // Psychic Magic contains a header and a single line after about its pool
      if (!parsedSubCategories["psychicMagic"] && lineContent.match(/^Psychic\s+Magic\b\s*/i)) {
        startSpellbookProcessing(processData, lineContent, line, false, true);

        rawSpellBooks[spellBooksFound].spells.push(data[line + 1]);
        line++;
      }

      // Parse Spells/Extracts Prepared/Known
      /* Collate all lines that are part of the spellbook's/formula book's spells,
       * by putting all lines found after the keyword ...
       * ... up until the end of the section or
       * ... up until the next keyword
       * into an array
       */
      if (!parsedSubCategories["spellBooks"] && /(?:Spells|Extracts) (?:Prepared|Known)\b\s*/i.test(lineContent)) {
        startSpellbookProcessing(processData, lineContent, line, false, false);
        sbcUtils.log(`Spellbook ${currentSpellBook}, ${startIndexOfSpellBooks[currentSpellBook]}`);
      }

      /* If there are Spell-Like Abilities and the current line comes after the start of this section
       * or we are in spellbook processing and the current line comes after the start of this section
       * then check the line for the keywords that denote a new spell section and determine
       * if we should push the line into the current section's spells array.
       */
      if (
        spellBooksFound !== 0 &&
        currentSpellBook <= spellBooksFound &&
        +line > +startIndexOfSpellBooks[currentSpellBook]
      ) {
        /* If the line contains any of the keyswords that denote a new spell section
         * like "spells prepared" or "spells known"
         * then don't push the line to the spellbook's spells array.
         */
        if (/Spell-Like|Spells|Extracts (?:Prepared|Known)|Psychic Magic/gi.test(lineContent) === false) {
          // Push the line into the array holding the raw data for Spell-Like Abilities
          rawSpellBooks[currentSpellBook].spells.push(lineContent);
        }
      }

      /* If this is the last line of the offense block
       * send spellBooks (if available) to the SpellBooksParser
       * and the notes section
       */
      if (line == data.length - 1) {
        /** @type {sbcTypes.SBC} */
        const sbcInstance = window.SBC;
        let spellBooksInUse = 0;
        let spellBookMapping = {};
        await sbcUtils.runParallelOps(Object.values(rawSpellBooks), async (rawSpellBook, i) => {
          let parser = new SpellBooksParser(sbcInstance.app);

          sbcUtils.log(`Spellbook ${i} Processing: ${rawSpellBook.spellBookType}`, rawSpellBook);
          const [result, type, inUse] = await parser.parse(rawSpellBook, startIndexOfSpellBooks[i]);
          if (inUse) {
            spellBookMapping[type] = currentSpellBookType[spellBooksInUse];
            spellBooksInUse++;
          }
          sbcUtils.log(`Spellbook ${i} Results: ${result} - ${type} - ${inUse}`);
        });

        if (spellBooksInUse > 0) {
          const spellBookData = {};
          const spellbooks = window.SBC.actor.system.attributes.spells.spellbooks;
          const books = new Set(Object.keys(spellbooks));

          const itemUpdates = [];
          const allSpells = window.SBC.actor.itemTypes.spell;

          for (const [type, book] of Object.entries(spellBookMapping)) {
            if (book === type) {
              books.delete(book);
              continue;
            }

            spellBookData[book] = pf1.utils.deepClone(spellbooks[type]);

            allSpells
              .filter((spell) => spell.system.spellbook === type)
              .forEach((spell) => {
                itemUpdates.push({ _id: spell._id, "system.spellbook": book });
              });

            processData.characterData.conversionValidation.spellBooks[book] = pf1.utils.deepClone(
              processData.characterData.conversionValidation.spellBooks[type]
            );
            delete processData.characterData.conversionValidation.spellBooks[type];
            books.delete(book);
          }
          books.forEach((book) => {
            spellBookData[book] = SpellBooksParser.getDefaultSpellbookData();
          });

          await window.SBC.actor.update({ "system.attributes.spells.spellbooks": spellBookData });
          await window.SBC.actor.updateEmbeddedDocuments("Item", itemUpdates);
        }

        if (!specialAttacksExist) {
          // Process Weapon Groups before attacks
          await processWeaponGroups(processData);
        } else {
          parsedSubCategories["specialAttacks"] = await parserSpecialAttacks(
            processData,
            specialAttackData.text,
            specialAttackData.line
          );

          // Process Weapon Groups before attacks
          await processWeaponGroups(processData);

          // Process Melee and Ranged Attacks
          parsedSubCategories["melee"] = await parseAttacks(meleeData, "mwak");
          parsedSubCategories["ranged"] = await parseAttacks(rangedData, "rwak");
        }
      }
    } catch (err) {
      window.SBC.settings.getSetting("debug") && console.error(err);
      const errorMessage = `Parsing the offense data failed at line ${line + startLine}`;
      const error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Parse/Offense", errorMessage, line + startLine);
      processData.errors.push(error);

      window.SBC.settings.getSetting("debug") && console.groupEnd();
      return false;
    }
  }

  sbcUtils.log("RESULT OF PARSING OFFENSE DATA (TRUE = PARSED SUCCESSFULLY)");
  sbcUtils.log(parsedSubCategories);
  window.SBC.settings.getSetting("debug") && console.groupEnd();

  return true;
}

/**
 * Parses the speed of the creature.
 *
 * @param {string} lineContent  The speed string.
 * @param {string} type         The type of speed.
 * @param {string} regexTerm    The regex term to match the speed.
 * @param {number} line         The line number.
 * @returns {Promise<boolean>}  `false` if any error is caught.
 */
async function parseSpeed(lineContent, type, regexTerm, line) {
  const parserSpeed = parserMapping.map.offense.speed;
  const speedRegex = new RegExp(`${regexTerm}\\s*($|[^,]*)`, "i");
  const speed = lineContent.match(speedRegex)[1].trim();

  return await parserSpeed.parse(speed, line, type);
}

/**
 * Parses the attack line.
 *
 * @param {string} lineContent  The line content.
 * @param {string} type         The type of attack.
 * @param {string} nextLine     The next line.
 * @param {number} line         The line number.
 * @returns {object}            The parsed attack line.
 */
function parseAttackLine(lineContent, type, nextLine, line) {
  const attackRegex = new RegExp(`^${type}\\s*(.*)`, "i");
  let attackText = lineContent.match(attackRegex)[1].trim();
  if (attackText.match(/(or|and)$/i)) attackText += " " + nextLine.trim();

  return { text: attackText, line: line };
}

/**
 * Parses the attacks.
 *
 * @param {object} attackData   The attack data.
 * @param {string} type         The type of attack.
 * @returns {Promise<boolean>}  `false` if any error is caught.
 */
async function parseAttacks(attackData, type) {
  const parserAttacks = parserMapping.map.offense.attacks;
  return await parserAttacks.parse(attackData.text, attackData.line, type);
}

/**
 * Parses the special attacks.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                The line content.
 * @param {number} line                       The line number.
 * @returns {Promise<boolean>}                `false` if any error is caught.
 */
async function parserSpecialAttacks(processData, lineContent, line) {
  const parserSpecialAttacks = parserMapping.map.offense.specialAttacks;
  const specialAttacks = lineContent.match(/^Special\s+Attacks?\s*(.*)/i)[1].trim();

  // parsedSubCategories["specialAttacks"] = await parserSpecialAttacks.parse(specialAttacks, line+startLine)
  const [result, entities] = await parserSpecialAttacks.parse(
    specialAttacks,
    line,
    ["special-attacks", "class-abilities"],
    ["feat"],
    ["classFeat"],
    false
  );

  processData.notes.offense.specialAttacks = entities;

  return result;
}

/**
 * Parses the space.
 *
 * @param {string} lineContent              The line content.
 * @param {number} line                     The line number.
 * @returns {Promise<[boolean, string]>}    `false` if any error is caught. The space string.
 */
async function parseSpace(lineContent, line) {
  // 10-28-23 Change:
  // Check for space without checking for number.
  // This used to check for a number, but failed to catch creatures that took up less space.
  // For example, an Imp has a space of "2-1/2 ft."
  const parserSpace = parserMapping.map.offense.space;
  let space = lineContent.match(/^Space\s*(\S+)\s+/i)[1];
  space = space.replace(/(.)-(.)/g, "$1.$2");

  const spaceMatch = space.match(/(\d*)\.+(\d*)\/+(\d*)/);
  if (spaceMatch?.length > 0) space = +spaceMatch[1] + spaceMatch[2] / spaceMatch[3];

  const spaceInSquares = (Math.round(space / 0.5) * 0.5) / 5;
  const result = await parserSpace.parse(spaceInSquares, line);

  return Promise.resolve([result, space]);
}

/**
 * Parses the reach.
 *
 * @param {string} lineContent  The line content.
 * @returns {Promise<[string, string]>}  The reach and reach context.
 */
async function parseReach(lineContent) {
  const reachInput = sbcUtils.parseSubtext(lineContent.match(/Reach(.*)/i)[1]);
  let reach = "";
  let reachContext = "";

  if (reachInput[0]) {
    reach = reachInput[0];
    // reach = reachInput[0].replace(/(\d+)(.*)/g, "$1").trim();
    reach = reach.replace(/(.)-(.)/g, "$1.$2");
    const reachMatch = reach.match(/(\d*)\.+(\d*)\/+(\d*)/);
    if (reachMatch?.length > 1) reach = +reachMatch[1] + reachMatch[2] / reachMatch[3];

    reach = reach.replace(/(\d+\.?\d*)(.*)/g, "$1");
  }

  if (reachInput[1]) reachContext = reachInput[1].replace(/[()]/g, "").trim();
  return Promise.resolve([reach, reachContext]);
}

// Spellcasting support functions
/**
 * Get the caster level from a line.
 *
 * @param {string} line The line to check.
 * @returns {number}    The caster level, or 0 if not found.
 */
function getCasterLevel(line) {
  return line.match(/\bCL\b\s*(?<cl>\d+)/i)?.groups.cl ?? 0;
}

/**
 * Get the concentration bonus from a line.
 *
 * @param {string} line The line to check.
 * @returns {number|null}    The concentration bonus, or null if not found.
 */
function getConcentrationBonus(line) {
  return line.match(/\b(Concentration\b|Conc\.)\s*\+(?<bonus>\d+)/i)?.groups?.bonus;
}

/**
 * Start the processing of a spellbook.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @param {string} lineContent                The line content.
 * @param {number} line                       The line number.
 * @param {boolean} isSpellLike               Whether this is a spell-like ability.
 * @param {boolean} isPsychicMagic            Whether this is psychic magic.
 */
function startSpellbookProcessing(processData, lineContent, line, isSpellLike, isPsychicMagic) {
  let spellCastingClass = "_hd";
  let spellCastingType = "prepared";
  let spellBookType = "spelllike";
  let spellBookName = null;
  let isAlchemist = false;

  if (!isSpellLike && !isPsychicMagic) {
    // Check for the spellCastingType (Spontaneous is default)
    // Set casterLevel and concentrationBonus
    // Set spellCastingClass (hd is default)
    spellCastingType = lineContent.match(/Prepared/i) ? "prepared" : "spontaneous";
    spellCastingClass = "hd";
    isAlchemist = !!lineContent.match(/Extracts/i);
    spellBookType = null;

    const classMatch = classesRegex(false).exec(lineContent)?.groups?.class;

    if (classMatch) {
      spellCastingClass = classMatch;
      const classItem = window.SBC.actor.itemTypes.class.find((c) => c.name.match(new RegExp(classMatch, "i")));
      if (!classItem) {
        spellCastingClass = "hd";
        spellBookName = `${classMatch} ${sbcUtils.translate("spellbookName.spells" + (spellCastingType === "prepared" ? "Prepared" : "Known"))}`;
      }
    } else {
      const classItem = window.SBC.actor.itemTypes.class.find(
        (c) => c.system.casting && c.system.casting.type === spellCastingType
      );
      spellCastingClass = classItem ? classItem.name : "hd";
    }

    // If it's a normal casting class of some sort
    currentSpellBook = spellBooksFound;
    spellBooksFound++;
    sbcUtils.log(`Current Spellbook: ${currentSpellBook}, Spellbooks found: ${spellBooksFound}`);
    spellBookType = currentSpellBookType[currentSpellBook];
  } else if (isPsychicMagic) {
    // If it's psychic magic
    currentSpellBook = spellBooksFound;
    spellBooksFound++;

    spellCastingType = "points";
    spellBookType = currentSpellBookType[currentSpellBook];
  } else {
    // Otherwise, it's a spell-like ability list
    currentSpellBook = spellBooksFound;
    spellBooksFound++;
    spellBookType = currentSpellBookType[currentSpellBook];
  }

  // Set casterLevel and concentrationBonus
  const casterLevel = getCasterLevel(lineContent);
  const concentrationBonus = getConcentrationBonus(lineContent) ?? casterLevel;

  // Before we push new data, check if the spellbook type already exists
  // If so, we need to move that data to the next slot.
  if (existingSpellbooks.includes(spellBookType)) {
    // Get the next unused spellbook type
    let newBook = getNextSpellbook();

    // Shuffle data around to swap.
    rawSpellBooks[newBook] = duplicate(rawSpellBooks[currentSpellBook]);
    rawSpellBooks[newBook].spellBookType = currentSpellBookType[newBook];
    startIndexOfSpellBooks[newBook] = startIndexOfSpellBooks[currentSpellBook];
  }

  // Push the line into the array holding the raw data for Spell-Like Abilities
  rawSpellBooks[currentSpellBook] = {
    firstLine: lineContent,
    spells: [],
    spellCastingType: spellCastingType,
    spellCastingClass: spellCastingClass,
    casterLevel: casterLevel,
    concentrationBonus: concentrationBonus,
    spellBookType: spellBookType,
    isSpellLike: isSpellLike,
    isAlchemist: isAlchemist,
    spellBookName: spellBookName,
  };
  startIndexOfSpellBooks[currentSpellBook] = line;

  // Keep a list of used spellbooks
  existingSpellbooks.push(spellBookType);
  console.log(`Current Spellbook: ${currentSpellBook}, Spellbooks found: ${spellBooksFound}`);
}

/**
 * Process the Weapon Groups.
 *
 * This function will parse the class feats and find any weapon groups
 * that the character has access to. It will then add these to the
 * character's weapon groups data for later processing of attack
 * bonuses when SBC processes the attacks.
 *
 * @param {sbcTypes.ProcessData} processData  Process data object.
 * @returns {Promise<void>}   A promise that resolves when the weapon groups have been processed.
 */
async function processWeaponGroups(processData) {
  const features = window.SBC.actor.itemTypes.feat.filter((feat) => feat.system.subType === "classFeat");
  const weaponGroups = Object.values(window.SBC.config.weaponGroups);

  for (const feature of features) {
    let subText = sbcUtils.parseSubtext(feature.name.toLowerCase());
    subText.shift();
    for (const group of subText) {
      const foundGroup = weaponGroups.find((wg) => group.includes(wg));
      if (foundGroup) {
        const foundBonus = parseInt(group.match(/\+(\d+)/)?.[1] ?? 0);
        sbcUtils.log(`Found Group ${foundGroup}, Found Bonus ${foundBonus}`);
        const groupKey = Object.keys(window.SBC.config.weaponGroups)[weaponGroups.indexOf(foundGroup)];

        if (foundGroup && foundBonus > 0 && !Object.keys(processData.characterData.weaponGroups).includes(groupKey))
          processData.characterData.weaponGroups[groupKey] = foundBonus;
      }
    }
  }

  sbcUtils.log("Weapon Groups", processData.characterData.weaponGroups);
}

/**
 * Get the next available spellbook type.
 *
 * This function will return the next available spellbook type that is not already in use.
 *
 * @returns {number}  The next spellbook type.
 */
function getNextSpellbook() {
  return parseInt(
    Object.keys(currentSpellBookType).find((type) => !existingSpellbooks.includes(currentSpellBookType[type]))
  );
}
