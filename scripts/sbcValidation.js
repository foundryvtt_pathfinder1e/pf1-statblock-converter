import { sbcError } from "./sbcError.js";
import { sbcUtils } from "./sbcUtils.js";
// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

/* ------------------------------------ */
/* Conversion Validation                */
/* ------------------------------------ */
export class sbcValidation {
  /**
   * Build changes for the current process data
   *
   * @param {sbcTypes.ProcessData} processData  The current process data
   * @param {string} itemID                     The item ID to build changes for
   * @returns {Promise<void>}                   A Promise that resolves once the changes have been built
   */
  static async buildChanges(processData, itemID = null) {
    let actor = window.SBC.actor;
    /** @type {Item} */
    let item = null;
    /** @type {Array<Item>} */
    let items = [];
    if (itemID !== null) {
      item = actor.items.get(itemID);
      items = [item];
    }
    if (item === null) items = actor.items;
    const rollData = actor.getRollData();
    let changes = {};

    items.forEach((currentItem) => {
      let itemChanges = currentItem.changes;

      // Check, if the currentItem has changes to be considered
      // eslint-disable-next-line no-undef
      if (!foundry.utils.isEmpty(itemChanges)) {
        itemChanges.map(async (element) => {
          const target = element.target;
          const formula = element.formula;
          const type = element.type;

          if (changes[target] == null) changes[target] = {};
          let processedFormula = await Roll.fromTerms(Roll.parse(formula, rollData)).evaluate();

          processedFormula = processedFormula?.total ?? +formula;

          if (changes[target][type] == null) {
            changes[target][type] = processedFormula;
          } else {
            changes[target][type] += processedFormula;
          }
        });
      }
    });

    processData.changes = changes;
  }

  /**
   * Get the total value of a specific target and type from the changes
   *
   * @param {sbcTypes.ProcessData} processData  The current process data
   * @param {string} target                     The target to get the value from
   * @param {string} type                       The type to get the value from
   * @returns {Promise<number>}                 The total value of the target and type
   */
  static async getValueFromChanges(processData, target = null, type = null) {
    let total = 0;
    if (target == null && type == null) return total;
    else if (target !== "all") {
      if (processData.changes[target] == null) return 0;

      for (const key in Object.keys(processData.changes[target])) {
        if ((type && key === type) || type == null) total += processData.changes[target][key];
      }

      return total;
    } else {
      Object.keys(processData.changes).map((subtarget) => {
        sbcUtils.log(`Checking subtarget ${subtarget}:`);
        Object.keys(processData.changes[subtarget]).map((k) => {
          sbcUtils.log(`type k is ${k} = ${processData.changes[subtarget][k]}`);
          if ((type && k === type) || type == null) total += +processData.changes[subtarget][k];
        });
      });
      return total;
    }
  }

  /**
   * Validate the conversion and create a conversion buff.
   *
   * This function will create a conversion buff with all the changes that need to be made to the actor.
   * It will also add context notes to the buff.
   *
   * The conversion buff will be created in two stages:
   * 1. Ability Scores
   * 2. All other attributes
   *
   * The reason for this is that ability scores can have cascading effects on other attributes
   * and we want to avoid that cascading effects interfere with other corrections.
   * The conversion buff will be created as a perm buff and will be hidden from the token.
   *
   * @param {sbcTypes.ProcessData} processData  The current process data
   * @returns {Promise<void>}
   */
  static async conversionValidation(processData) {
    if (!window.SBC.settings.getSetting("createBuff")) return;
    window.SBC.settings.getSetting("debug").debug && console.groupCollapsed("sbc-pf1 | Conversion Validation");
    console.log(processData);
    const actor = window.SBC.actor;
    const conversionValidation = processData.characterData.conversionValidation;

    try {
      await this.buildChanges(processData);

      const stage1changes = [],
        stage2changes = [];
      let contextNotes = [];

      let valueInAcTypes = 0;

      // Validate the spellBooks
      let spellBooksToValidate = Object.keys(conversionValidation.spellBooks);
      const bookUpdates = {};
      for (let i = 0; i < spellBooksToValidate.length; i++) {
        let spellBookToValidate = spellBooksToValidate[i];
        let casterLevelToValidate = conversionValidation.spellBooks[spellBookToValidate].casterLevel;
        let concentrationBonusToValidate = conversionValidation.spellBooks[spellBookToValidate].concentrationBonus;
        let casterLevelInActor = actor.system.attributes.spells.spellbooks[spellBookToValidate].cl.total;

        console.log(spellBookToValidate, casterLevelToValidate, casterLevelInActor);
        // let spellCastingAbility = actor.system.attributes.spells.spellbooks[spellBookToValidate].ability
        // let spellCastingAbilityModifier = actor.system.abilities[spellCastingAbility].mod

        const concentrationBonusOnActor =
          actor.system.attributes.spells.spellbooks[spellBookToValidate].concentration.total;

        let differenceInCasterLevel = +casterLevelToValidate - +casterLevelInActor;
        let differenceInConcentrationBonus =
          +concentrationBonusToValidate - concentrationBonusOnActor - differenceInCasterLevel;

        processData.miscNotes.spellbookNotes[i] = { key: spellBookToValidate };
        if (differenceInCasterLevel !== 0) {
          processData.miscNotes.spellbookNotes[i].casterLevel =
            `sbc | Total in statblock was CL ${casterLevelToValidate}, adjust as needed.`;
          bookUpdates[`data.attributes.spells.spellbooks.${spellBookToValidate}`] = {
            cl: {
              formula: differenceInCasterLevel.toString(),
            },
            //clNotes: "sbc | Total in statblock was CL " + casterLevelToValidate + ", adjust as needed.",
          };
        }
        if (differenceInConcentrationBonus !== 0) {
          processData.miscNotes.spellbookNotes[i].concentrationBonus =
            `sbc | Total in statblock was +${concentrationBonusToValidate}, adjust as needed.`;
          bookUpdates[`data.attributes.spells.spellbooks.${spellBookToValidate}.concentrationFormula`] =
            differenceInConcentrationBonus.toString();
          // bookUpdates[`data.attributes.spells.spellbooks.${spellBookToValidate}.concentrationNotes`] =
          //   "sbc | Total in statblock was +" + concentrationBonusToValidate + ", adjust as needed.";
        }
      }

      // eslint-disable-next-line no-undef
      if (!foundry.utils.isEmpty(bookUpdates)) {
        await actor.update(bookUpdates);
        await updateMiscNotesItem(actor, processData.miscNotes.spellbookNotes);
      }

      // Validate ability scores first as they can have cascading effects
      const abilityScoreKeys = ["str", "dex", "con", "int", "wis", "cha"];
      for (let abl of abilityScoreKeys) {
        const totalInActor = actor.system.abilities[abl].total;
        let totalInStatblock = conversionValidation.attributes[abl.capitalize()] ?? totalInActor;
        const difference = +totalInStatblock - +totalInActor;
        if (difference === 0) continue;

        console.log(totalInStatblock, totalInActor);
        let attributeChange = Object.assign({}, pf1.components.ItemChange.defaultData, {
          formula: difference.toString(),
          type: "untypedPerm",
          target: abl,
          value: +difference,
        });

        stage1changes.push(attributeChange);

        delete conversionValidation.attributes[abl.capitalize()];
      }

      // Stage 1 conversion buff to allow ability score cascading effects to not interfere with other corrections
      // This might be better off as direct modification to the actor, however.
      if (stage1changes.length) {
        let conversionBuffItem1 = {
          name: "sbc | Conversion Buff (Ability Scores)",
          type: "buff",
          system: {
            description: {
              value: `<h2>sbc | Conversion Buff (Ability Scores)</h2>
                            This Buff was created by <strong>sbc</strong> to compensate for differences between the statblock input and FoundryVTTs automatically calculated values.
                            <br><br>
                            As differences in ability scores can have cascading effects, these get handled first and in a separate conversion buff.`,
            },
            active: true,
            subType: "perm",
            changes: stage1changes,
            hideFromToken: true,
            level: 0,
            tag: "sbcConversionBuff1",
            useCustomTag: true,
          },
          img: "systems/pf1/icons/skills/yellow_36.jpg",
        };

        await actor.createEmbeddedDocuments("Item", [conversionBuffItem1]);
      }

      // Get an array of all attributes that need to be validated
      let attributesToValidate = Object.keys(conversionValidation.attributes);
      // And push "acNormal", "acTouch" and "acFlatFooted" to the end of that array so it gets validated after the acTypes
      attributesToValidate.splice(attributesToValidate.indexOf("acNormal"), 1);
      attributesToValidate.splice(attributesToValidate.indexOf("acTouch"), 1);
      attributesToValidate.splice(attributesToValidate.indexOf("acFlatFooted"), 1);
      attributesToValidate.push("acNormal", "acTouch", "acFlatFooted");

      // Loop through the attributes ...
      for (let i = 0; i < attributesToValidate.length; i++) {
        let attribute = attributesToValidate[i];
        let type = "";
        let target = "";
        let totalInStatblock = conversionValidation.attributes[attribute];
        let totalInActor = 0;
        let valueInItems = 0;
        let difference = 0;

        // Skip if it's not a number (such as CMB being a dash)
        if (isNaN(totalInStatblock)) continue;

        // Generate Changes for the conversionBuff
        switch (attribute.toLowerCase()) {
          case "cmd":
          case "cmb":
          case "init":
            totalInActor = actor.system.attributes[attribute].total;
            type = "untypedPerm";
            target = attribute;
            sbcUtils.log(
              `Attribute: ${attribute} | Total in Actor: ${totalInActor} | Total in Statblock: ${totalInStatblock}`
            );
            if (totalInActor !== totalInStatblock) {
              difference = +totalInStatblock - +totalInActor;
            }
            break;
          case "hpbonus":
            type = "untypedPerm";
            target = "mhp";
            difference = +totalInStatblock;
            break;
          case "hptotal":
            totalInActor = actor.system.attributes.hp.max;
            type = "untypedPerm";
            target = "mhp";
            difference = +totalInStatblock - +totalInActor;
            break;
          case "acnormal":
            totalInActor = actor.system.attributes.ac.normal.total;
            type = "untypedPerm";
            target = "aac";
            difference = +totalInStatblock - +totalInActor - +valueInAcTypes;
            break;
          case "base":
          case "enhancement":
          case "dodge":
          case "inherent":
          case "deflection":
          case "morale":
          case "luck":
          case "sacred":
          case "insight":
          case "resistance":
          case "profane":
          case "trait":
          case "racial":
          case "competence":
          case "circumstance":
          case "alchemical":
          case "penalty":
            valueInItems = await this.getValueFromChanges(processData, "all", attribute.toLowerCase());

            type = attribute;
            target = "aac";
            difference = +totalInStatblock - +valueInItems;
            valueInAcTypes += +difference;
            break;
          case "rage":
            type = "untypedPerm";
            target = "ac";
            difference = +totalInStatblock;
            break;
          case "fort":
          case "ref":
          case "will":
            type = "untypedPerm";
            target = attribute;
            totalInActor = actor.system.attributes.savingThrows[attribute].total ?? 0;
            difference = +totalInStatblock - +totalInActor;
            break;
          default:
            break;
        }

        // If the total in the statblock differs from the total in foundry, add a change to the conversion buff
        if (difference !== 0) {
          let attributeChange = await new pf1.components.ItemChange({
            formula: difference.toString(),
            type: type,
            target: target,
            value: +difference,
          });

          stage2changes.push(attributeChange.toObject());
        }
      }

      // Add context notes to the buff
      let contextNotesToAdd = Object.keys(conversionValidation.context);

      for (let i = 0; i < contextNotesToAdd.length; i++) {
        let contextNoteType = contextNotesToAdd[i];
        let contextNoteToAdd = conversionValidation.context[contextNoteType];

        if (contextNoteToAdd !== "") {
          let contextNote = {
            subTarget: contextNoteType,
            target: "misc",
            text: contextNoteToAdd,
          };
          contextNotes.push(contextNote);
        }
      }

      // Handle Skill Information in the conversionValidation
      // (1) Create skillContext Objects to add to the Buff
      // (2) Adjust for differences between calculated skillTotals and statblockTotals
      let skillKeys = Object.keys(conversionValidation.skills);

      for (let i = 0; i < skillKeys.length; i++) {
        let skillKey = skillKeys[i];
        let parentSkillKey = skillKey.replace(/(\d+)/, "");
        let skillToValidate = conversionValidation.skills[skillKey];
        let skillModInActor = 0;

        let skillSubKeys = Object.keys(actor.system.skills[parentSkillKey]);

        // For Skills with subskill --> target: "skill.prf.subSkills.prf1"
        let target = "";

        if (!skillSubKeys.includes("subSkills")) {
          const skInfo = actor.getSkillInfo(skillKey);
          skillModInActor = skInfo.mod ?? 0;
          target = "skill." + skillKey;
        } else {
          const subSkillInfo = actor.getSkillInfo(`${parentSkillKey}.subSkills.${skillKey}`);
          skillModInActor = subSkillInfo.mod ?? 0;
          target = "skill." + parentSkillKey + ".subSkills." + skillKey;
        }

        // (1) Create skillContext Objects to add to the Buff
        if (skillToValidate.context !== "") {
          let contextNote = skillToValidate.context;
          let skillContext = {
            target: target,
            text: contextNote,
          };
          contextNotes.push(skillContext);
        }

        // (2) Adjust for differences between calculated skillTotals and statblockTotals
        if (+skillToValidate.total !== +skillModInActor) {
          let difference = +skillToValidate.total - +skillModInActor;

          if (difference !== 0) {
            let skillChange = await new pf1.components.ItemChange({
              formula: difference.toString(),
              type: "untypedPerm",
              target: target,
              value: +difference,
            });

            stage2changes.push(skillChange.toObject());
          }
        }
      }

      let conversionBuffItem2 = {
        name: "sbc | Conversion Buff",
        type: "buff",
        system: {
          description: {
            value: `<h2>sbc | Conversion Buff</h2>
                        This Buff was created by <strong>sbc</strong> to compensate for differences between the input and the values FoundryVTT calculates automatically.
                        <br><br>
                        Especially when the pathfinder system gets upgraded, entries in compendiums get updated or foundry changes in some way, this buff may become outdated.
                        <br><br>
                        For most mooks the conversion should more or less work, but for important NPCs or creatures it is adviced to double check the conversion manually.`,
          },
          active: true,
          subType: "perm",
          changes: stage2changes,
          contextNotes: contextNotes,
          hideFromToken: true,
          level: 0,
          tag: "sbcConversionBuff2",
          useCustomTag: true,
        },
        img: "systems/pf1/icons/skills/yellow_36.jpg",
      };

      // Only create the conversion buff if there are changes to be made
      if (stage2changes.length > 0 || contextNotes.length > 0) {
        await actor.createEmbeddedDocuments("Item", [conversionBuffItem2]);
      }

      Hooks.callAll("sbc.validated", actor);
    } catch (err) {
      window.SBC.settings.getSetting("debug").debug && console.error(err);
      let errorMessage = "Failed to validate the conversion and create a conversion buff";
      let error = new sbcError(sbcError.ERRORLEVELS.ERROR, "Validation", errorMessage);
      processData.errors.push(error);
      throw err;
    }
    window.SBC.settings.getSetting("debug").debug && console.groupEnd();
  }
}

/**
 * Update the Misc Notes Item with the new spellbook data notes
 *
 * @param {import ("../pf1/module/documents/actor/actor-base.mjs").ActorBasePF} actor The actor to update the misc notes item for
 * @param {sbcTypes.SpellbookNotesData[]} miscNotes The misc notes to update the misc notes item with
 * @returns {Promise<void>}
 */
async function updateMiscNotesItem(actor, miscNotes) {
  let contextNotes = [];
  let descData = "";

  for (let note of miscNotes) {
    const spellbook = actor.system.attributes.spells.spellbooks[note.key];
    if (note.casterLevel || note.concentration) {
      descData += `<p><strong>${spellbook.name || spellbook.label}</strong>:</p>`;

      if (note.casterLevel) {
        contextNotes.push({
          target: `cl.book.${note.key}`,
          text: note.casterLevel,
        });

        descData += `<p>Caster Level: ${note.casterLevel}</p>`;
      }

      if (note.concentration) {
        contextNotes.push({
          target: `concn.${note.key}`,
          text: note.concentration,
        });

        descData += `<p>Concentration Bonus: ${note.concentration}</p>`;
      }
    }
  }

  let miscNotesItem = actor.itemTypes.feat.find((item) => item.name === "Miscellaneous Notes");
  if (miscNotesItem) {
    descData = miscNotesItem.system.description.value + descData;
    await miscNotesItem.update({
      "system.contextNotes": miscNotesItem.system.contextNotes.concat(contextNotes),
      "system.description.value": descData,
    });
  } else {
    descData = `<p>This feature was created by <strong>SBC</strong> to collect any notes that don't have a source on the actor,
                such as saving throw details or a bonus to CMD from extra legs. It may also be used to record a natural armor bonus,
                spell resistance bonus, or spellcasting notes.</p>${descData}`;
    await actor.createEmbeddedDocuments("Item", [
      {
        name: "Miscellaneous Notes",
        type: "feat",
        system: {
          contextNotes: contextNotes,
          description: { value: descData },
          subType: "misc",
          tag: "miscellaneousNotes",
        },
        img: "systems/pf1/icons/skills/yellow_36.jpg",
      },
    ]);
  }
}
