/*
 * Statblock Converter (SBC) for Pathfinder 1st Edition on FoundryVTT
 *
 * Current Authors: Fair Strides and KitCat
 * Contributions by: Noon, mkahvi
 *
 * Original Author: Lavaeolous
 */

import { registerSettings, sbcSettings } from "./sbcSettings.js";
import { sbcUtils } from "./sbcUtils.js";
import { sbcConfig } from "./sbcConfig.js";
import { CompendiumOps } from "./sbcCompendium.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

/**
 * The global SBC object
 *
 * @type {sbcTypes.SBC}
 */
window.SBC = {
  app: null,
  actor: null,
  actorType: null,
  folderId: "",
  wipFolderId: "",
  config: sbcConfig,
  configInitialized: false,
  compendiumSearch: new CompendiumOps(),
  isImporting: false,
  settings: sbcSettings,
};

/* ------------------------------------ */
/* Hooks                                */
/* ------------------------------------ */
// Do anything after initialization but before ready
Hooks.once("setup", async () => {
  registerSettings();
});

// Do anything once the module is ready
Hooks.once("ready", async () => {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;
  await sbcSettings.updateCustomCompendiums(true);
  await sbcUtils.initializeSBC();
  hideWIPFolder();

  Hooks.callAll("sbc.loadCustomCompendiums");

  await sbcInstance.config.initializeConfig();

  console.log(sbcUtils.translate("log.configInitialized"));
  sbcInstance.configInitialized = true;

  game.modules.get(sbcInstance.config.modData.mod).api = { openSBCDialog: sbcUtils.openSBCDialog };
});

// Render the sbcButton when the actorDirectory is visible
Hooks.on(
  "renderActorDirectory",
  /**
   * @param {*} _app The app that called the hook
   * @param {JQuery} jq The jQuery object of the app
   * @param {*} _data The data passed to the app
   * @returns {void}
   */
  (_app, jq, _data) => {
    hideWIPFolder();
    if (!game.user.hasPermission("ACTOR_CREATE")) return;

    // Handle rendering the SBC window button
    sbcUtils.log(sbcUtils.translate("log.renderingButton"));
    const sbcStartButton = document.createElement("button");
    sbcStartButton.id = "startSBCButton";
    sbcStartButton.classList.add("create-entity", "sbcButton");

    const icon = document.createElement("i");
    icon.classList.add("fas", "fa-file-import");
    sbcStartButton.appendChild(icon);

    const text = document.createTextNode(sbcUtils.translate("sbcButton"));
    sbcStartButton.appendChild(text);

    // Append the button to the directory footer
    jq[0].querySelector(".directory-footer").append(sbcStartButton);

    // Handle the click event
    sbcStartButton.addEventListener("click", async () => await sbcUtils.openSBCDialog());
  }
);

// When SBC resets, re-setup the fuzzy-index for searching compendiums
Hooks.on("sbc.reset", async function () {
  let customCompendiums = [];
  let customCompendiumSettings = window.SBC.settings.getSetting("customCompendiums");

  if (customCompendiumSettings !== "") {
    customCompendiumSettings = customCompendiumSettings.replace(/\s/g, "");
    customCompendiums.push(...customCompendiumSettings.split(/[,;]/g));
    console.log(customCompendiums);
  }

  await fuzzyIndexPacks(customCompendiums);
});

// Allow other modules to add custom compendiums
Hooks.on(
  "sbc.loadCustomCompendiums",
  /**
   *
   * @param {Array<string>} compendia The compendiums to load
   * @returns {Promise<void>}
   */
  async function (compendia) {
    if (!compendia) return;
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;

    let customCompendiums = new Set();
    let customCompendiumSettings = window.SBC.settings.getSetting("customCompendiums");

    if (customCompendiumSettings !== "") {
      customCompendiumSettings = customCompendiumSettings.replace(/\s/g, "");
      customCompendiums = new Set(customCompendiumSettings.split(/[,;]/g));
    }
    compendia.forEach((element) => {
      customCompendiums.add(element);
    });

    await fuzzyIndexPacks(Array.from(customCompendiums));
    sbcInstance.config.initializeConfig();
    game.settings.set(sbcInstance.config.modData.mod, "customCompendiums", Array.from(customCompendiums).join(","));
  }
);

Hooks.on(
  "pf1RegisterMaterialTypes",
  /**
   * @param {object} registry The system's materials registry
   */
  (registry) => {
    // Register Silver Material, as it is used in stat blocks
    const data = registry.get("alchemicalSilver");
    data.addon = false;
    data.name = "PF1.Materials.Types.Silver";
    registry.register("pf1-statblock-converter", "silver", data);
  }
);

/* ------------------------------------ */
/* Helpers                              */
/* ------------------------------------ */
/**
 * Indexes the provided packs for fuzzy searching
 *
 * @param {Array<string>} packs The packs to index
 * @returns {Promise<void>}
 * @private
 * @async
 */
async function fuzzyIndexPacks(packs) {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;
  await sbcInstance.compendiumSearch.processCompendiums(packs, { reset: true });
}

/**
 * Hides the WIP folder
 *
 * @private
 */
function hideWIPFolder() {
  /** @type {sbcTypes.SBC} */
  const sbcInstance = window.SBC;

  // Hide the WIP sub-folder, if it exists
  document.querySelector(`.folder[data-folder-id="${sbcInstance.wipFolderId}"]`)?.remove();
}
