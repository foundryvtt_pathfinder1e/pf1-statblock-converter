import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";

// Parse Speed
export default class SpeedParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Offense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "speed";
  }

  /**
   * Parses a speed string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, type) {
    if (value.length <= 0) return true;

    const rawInput = value.replace(/(^[,;\s]*|[,;\s]*$)/g, "");
    const input = sbcUtils.parseSubtext(rawInput);

    let speed = input[0].match(/(\d+)\sft\./)?.[1];
    let speedContext = "";

    if (speed == null) {
      this.throwParseError(undefined, value, line, type);
      return false;
    }

    const speedDistance = pf1.utils.convertDistance(+speed);
    let speedNote =
      game.i18n.localize(`PF1.Movement.Mode.${type.toLowerCase()}`) + ` ${speedDistance[0]} ${speedDistance[1]}`;

    // Factor in speed changes from items/features/races. This needs to account for both
    // the specific type of movement, and any "all speed" changes.
    speed -= await sbcUtils.getTotalFromChanges(window.SBC.actor, `${type}Speed`);
    speed -= await sbcUtils.getTotalFromChanges(window.SBC.actor, "allSpeeds");
    this.app.processData.characterData.conversionValidation.attributes[type] = +speed;
    await window.SBC.actor.update({
      [`system.attributes.speed.${type}.base`]: +speed,
    });

    switch (type) {
      case "fly": {
        // TODO: Allow abbreviations
        if (input.length > 1) {
          const flyManeuverability = sbcUtils.haystackSearch(Object.keys(pf1.config.flyManeuverabilities), input[1]);

          if (flyManeuverability) {
            await window.SBC.actor.update({
              ["system.attributes.speed.fly.maneuverability"]: flyManeuverability,
            });
            speedNote += ` (${game.i18n.localize(`PF1.Movement.FlyManeuverability.Quality.${flyManeuverability.toLowerCase()}`)})`;
          }
          if (input[2]) {
            speedContext = input[2];
          }
        }
        break;
      }

      default:
        if (input.length > 1) {
          speedContext = input[1];
        }
        break;
    }

    if (speedContext !== "") {
      /*
       * TODO: DO STUFF WITH SPEED CONTEXT NOTES
       * CURRENTLY THE SHEET DOES NOT SUPPORT THEM
       */
    }

    this.app.processData.notes.offense.speed.push(speedNote);
    return true;
  }
}
