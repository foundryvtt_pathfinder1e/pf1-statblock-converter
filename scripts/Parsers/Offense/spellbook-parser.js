import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";
import { escapeForRegex, wizardClassesRegex } from "../../sbcRegex.js";
import { Metamagic, PrestigeClasses } from "../../Content/index.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

const SpellFrequencyTypes = Object.freeze({
  Constant: "constant",
  Weekly: "weekly",
  Monthly: "monthly",
  Yearly: "yearly",
  AtWill: "atWill",
  Daily: "daily",
});

const spellRowCounts = {};

// Parse Spell Books and Spell-Like Abilities
export default class SpellBooksParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Offense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "spellBook";
  }

  /**
   * Parses a spell book
   *
   * @param {sbcTypes.RawSpellBookData} value                    The value to parse
   * @param {number} line                     The line number
   * @param {string} _type                    The type of value
   * @returns {Promise<(boolean|string)[]>}   A boolean indicating success, and the spellbook type
   */
  async _parse(value, line, _type = undefined) {
    sbcUtils.log("Trying to parse the following Spell Book.", value);

    let spellCastingType = value.spellCastingType;
    let spellRows = value.spells;
    let spellBookType = value.spellBookType;
    let altNameSuffix =
      spellCastingType === "prepared" ? "Prepared" : spellCastingType === "points" ? "Psychic" : "Known";

    // Set up the spellbook
    spellBookType = await this.setupSpellbook(
      spellCastingType,
      spellBookType,
      value.spellCastingClass,
      value.isAlchemist ? "extracts" : "spells",
      altNameSuffix,
      +value.casterLevel,
      +value.concentrationBonus,
      value.isSpellLike,
      value.firstLine,
      value.spellBookName
    );

    // Initialize the spell row counts
    spellRowCounts[spellBookType] = {};
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].forEach((key) => {
      spellRowCounts[spellBookType][key] = {};
      spellRowCounts[spellBookType][key].base = 0;
      spellRowCounts[spellBookType][key].max = 0;
    });

    const spellbook = window.SBC.actor.system.attributes.spells.spellbooks[spellBookType];
    let spellRowsNotes = [];
    let wizardSchool = null;
    let wizardSchoolShort = null;
    let spellBookName = spellbook.name;
    let nameOptions = SpellBooksParser.getClassPostfixFromSpellbookName(spellBookName);
    let castingClasses = window.SBC.actor.itemTypes.class.filter((i) => i.system.casting);
    castingClasses = castingClasses
      .map((i) => i.system.tag.substring(0, 3).toUpperCase())
      .concat(castingClasses.map((i) => `${i.system.tag[0]}${i.system.tag[2]}${i.system.tag[3]}`.toUpperCase()));

    // If we're a wizard class, we want to know which school of spells is our focus
    const wizardClass = value.spellCastingClass.match(wizardClassesRegex());
    if (wizardClass) {
      switch (wizardClass[0]) {
        case "Abjurer":
          wizardSchoolShort = "abj";
          wizardSchool = "Abjuration";
          break;
        case "Conjurer":
          wizardSchoolShort = "con";
          wizardSchool = "Conjuration";
          break;
        case "Diviner":
          wizardSchoolShort = "div";
          wizardSchool = "Divination";
          break;
        case "Enchanter":
          wizardSchoolShort = "enc";
          wizardSchool = "Enchantment";
          break;
        case "Evoker":
          wizardSchoolShort = "evo";
          wizardSchool = "Evocation";
          break;
        case "Illusionist":
          wizardSchoolShort = "ill";
          wizardSchool = "Illusion";
          break;
        case "Necromancer":
          wizardSchoolShort = "nec";
          wizardSchool = "Necromancy";
          break;
        case "Transmuter":
          wizardSchoolShort = "trs";
          wizardSchool = "Transmuation";
          break;
        case "Universalist":
        default:
          wizardSchoolShort = "";
          wizardSchool = "Universalist";
          break;
      }
    } else if (spellbook.class === "wizard" || (spellBookName && spellBookName.toLowerCase().includes("wizard"))) {
      wizardSchoolShort = "";
      wizardSchool = "Universalist";
    }

    if (wizardSchool === "Universalist") {
      await window.SBC.actor.update({
        [`system.attributes.spells.spellbooks.${spellBookType}.domainSlotValue`]: 0,
      });
    }

    /* Parse the spell rows
     * line by line
     */
    let spellTotal = 0;
    for (let i = 0; i < spellRows.length; i++) {
      spellTotal += this.splitText(spellRows[i].replace(/(^[^-]*-)/, ""), false).length;
    }

    let specialRow = "";
    for (let i = 0; i < spellRows.length; i++) {
      if (spellRows[i] === "") continue;
      let spellRow = spellRows[i];

      /** @type {sbcTypes.SpellRowData} */
      const spellRowData = {
        spellLevel: -1,
        spellsPerX: "",
        spellsPerXTotal: -1,
        isAtWill: false,
        frequency: null,
        isSpellRow: false,
        spellRowIsInitialized: false,
        wizardSchool: wizardSchoolShort,
        isSpellLike: value.isSpellLike,
      };

      // Get spellLevel, spellsPerX and spellsPerXTotal
      this.processSpellRow(spellRow, spellBookType, spellRowData);

      // Check for use frequencies
      this.processFrequencies(spellRow, spellRowData);

      if (spellRowData.isSpellRow) {
        let spellRowNotes = {
          frequency:
            spellRowData.spellLevel === -1
              ? {
                  type: sbcUtils.translate(`spellFrequencyNoun.${spellRowData.frequency}`),
                  amount: spellRowData.spellsPerXTotal !== -1 ? +spellRowData.spellsPerXTotal : 0,
                }
              : null,
          level: +spellRowData.spellLevel,
          levelEnum: sbcUtils.translate(`spellLevelEnum.${spellRowData.spellLevel}`),
          spells: [],
        };

        let spells = this.splitText(spellRow.replace(/(^[^-]*-)/, ""), false);

        // Check if the spellbook has just class features, and prioritize.
        let featuresOnly = false;
        if (value.isSpellLike) {
          for (let spell of spells) {
            spell = spell.replaceAll(/\*/g, "").trim();
            let [spellName, altName] = SpellBooksParser.processSpellName(spell);
            if (spellName === "") continue;

            let searchEntity = {
              search: new Set([spellName, altName]),
              type: "spell",
              item: spellName,
            };

            searchEntity.search = sbcUtils.createSearchSet(
              [spellName, altName],
              true,
              nameOptions.concat(castingClasses)
            );

            // nameOptions.forEach((nameOption) => {
            //   searchEntity.search.add(`${spellName} (${nameOption})`);
            //   searchEntity.search.add(`${altName} (${nameOption})`);
            // });

            // castingClasses.forEach((castingClass) => {
            //   searchEntity.search.add(`${spellName} (${castingClass})`);
            //   searchEntity.search.add(`${altName} (${castingClass})`);
            // });

            let [spellEntity, featureEntity] = await this.findSpellEntities(searchEntity);

            let entity = await sbcUtils.generatePlaceholderEntity(searchEntity, line);
            let foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, "feat", "misc", spellName, entity);

            if (!spellEntity && featureEntity) {
              featuresOnly = true;
              break;
            } else if (featureEntity && spellTotal === 1) {
              featuresOnly = true;
              break;
            } else if (!featureEntity && foundItem) {
              featuresOnly = true;
              break;
            }
          }
        }

        // Loop through the spells
        await sbcUtils.runParallelOps(spells, async (spell) => {
          if (featuresOnly) spellTotal--;

          await this.processSpell(
            spell.replaceAll(/\*/g, "").trim(),
            spellRowData,
            spellBookType,
            spellCastingType,
            nameOptions,
            castingClasses,
            line,
            spellRowNotes,
            featuresOnly
          );
        });

        spellRowsNotes.push(spellRowNotes);
      }
      // Search for Domains, Mysteries, etc
      else {
        specialRow = await this.parseSpellbookSpecials(spellRow, line, spellBookType);
      }

      let countUpdates = {};
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].forEach((key) => {
        countUpdates[`system.attributes.spells.spellbooks.${spellBookType}.spells.spell${key}.base`] =
          spellRowCounts[spellBookType][key].base;
        countUpdates[`system.attributes.spells.spellbooks.${spellBookType}.spells.spell${key}.max`] =
          spellRowCounts[spellBookType][key].max;
      });

      await window.SBC.actor.update(countUpdates);
    }

    // Handle the Wizard School, if applicable
    if (wizardSchool) {
      /** @type {sbcTypes.SBC} */
      const sbcInstance = window.SBC;
      const compendiumOps = sbcInstance.compendiumSearch;
      const searchEntity = {
        search: new Set([`${wizardSchool} School`, wizardSchool]),
        type: "feat",
        item: wizardSchool,
      };
      const searchEntityOptions = {
        itemType: "feat",
        itemSubType: "classFeat",
        classes: this.app.processData.characterData.classes,
        race: this.app.processData.characterData.race,
      };

      const actor = window.SBC.actor;
      const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
      const existingEntity = actor.itemTypes.feat
        .filter((feat) => feat.system.subType === "classFeat")
        .find((feat) => (feat.name === feat.name) === entity?.name);
      searchEntity.search.clear();

      if (entity) {
        if (
          !existingEntity ||
          (existingEntity && entity.flags.core?.sourceId !== existingEntity.flags.core?.sourceId)
        ) {
          await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
          await createItem(entity);
        }
      }
    }

    const spellBook = window.SBC.actor.system.attributes.spells.spellbooks[spellBookType];
    sbcUtils.log(spellBook);
    if (!spellTotal) {
      // Reset the spellbook to factory defaults.
      await window.SBC.actor.update({
        [`system.attributes.spells.spellbooks.${spellBookType}`]: SpellBooksParser.getDefaultSpellbookData(),
      });
    }

    this.app.processData.notes.offense.spellBooks.push({
      name: value.firstLine.split("(")[0].trim(),
      cl: +value.casterLevel,
      asf: spellBook.arcaneSpellFailure ? window.SBC.actor.spellFailure : 0,
      concentration: (+value.concentrationBonus >= 0 ? "+" : "") + +value.concentrationBonus,
      spellRows: spellRowsNotes,
      specialRow: specialRow,
    });
    sbcUtils.log(spellRowsNotes);

    // Return true for the try, and the spellBookType for further processing.
    return [true, spellBookType, spellTotal !== 0];
  }

  /**
   * Set up the spellbook
   *
   * @param {string} spellCastingType     The spellcasting type
   * @param {string} spellBookType        The spellbook type
   * @param {string} spellCastingClass    The spellcasting class
   * @param {string} spellsOrExtracts     Whether the spellbook contains spells or extracts
   * @param {string} altNameSuffix        The suffix for the spellbook name
   * @param {number} casterLevel          The caster level
   * @param {number} concentrationBonus   The concentration bonus
   * @param {boolean} isSpellLike         Whether the spellbook is spell-like
   * @param {string} firstLine            The first line of the spellbook
   * @param {string} spellBookName        The spellbook name
   * @returns {Promise<string>}           The spellbook type
   */
  async setupSpellbook(
    spellCastingType,
    spellBookType,
    spellCastingClass,
    spellsOrExtracts,
    altNameSuffix,
    casterLevel,
    concentrationBonus,
    isSpellLike,
    firstLine,
    spellBookName
  ) {
    let domainSlots = 0;
    let isPsychic = false;
    let hasCantrips = true;
    // General spellbook update defaults
    let spellBookUpdates = {
      domainSlotValue: domainSlots,
      psychic: isPsychic,
      spellPreparationMode: spellCastingType,
      inUse: true,
    };
    const actor = window.SBC.actor;

    // If the actor doesn't have this spellbook, set up default data for it
    if (!actor.system.attributes.spells.spellbooks[spellBookType]) {
      spellBookUpdates = foundry.utils.mergeObject(SpellBooksParser.getDefaultSpellbookData(), spellBookUpdates);
    }

    const finalSpellCastingClass = spellCastingClass.match(wizardClassesRegex()) ? "wizard" : spellCastingClass;

    // Now customize the spellbook updates based on type of casting
    if (isSpellLike) {
      // Spell-Like Abilities
      spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
        ability: "cha",
        autoSpellLevelCalculation: false,
        class: "_hd",
        arcaneSpellFailure: false,
        name: sbcUtils.capitalize(firstLine.match(/^(.*?)\(/)[1].trim()),
        kind: "arcane",
        hasCantrips: hasCantrips,
      });
    } else if (spellCastingType === "points") {
      const actorAbilities = actor.system.abilities;
      // Psychic Magic
      spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
        arcaneSpellFailure: false,
        autoSpellLevelCalculation: false,
        class: "_hd",
        name: sbcUtils.translate("spellbookName.psychicMagic"),
        kind: "psychic",
        hasCantrips: true,
        isPsychic: true,
        spellPreparationMode: "spontaneous",
        spellPoints: {
          useSystem: true,
          maxFormula: "0",
          max: 0,
        },
        ability: actorAbilities.int.total >= actorAbilities.cha.total ? "int" : "cha",
      });
    } else {
      // Spell Books
      spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
        class: finalSpellCastingClass.toLowerCase(),
        name:
          spellCastingClass.capitalize() +
          " " +
          sbcUtils.translate(`spellbookName.${spellsOrExtracts}`) +
          " " +
          altNameSuffix,
      });
    }

    // If the class isn't based on hit dice, try to use the class
    if (["hd", "_hd"].includes(spellCastingClass) === false) {
      // Try to find the spell-casting class
      const classItem =
        actor.itemTypes.class.filter(
          (i) =>
            i.name.toLowerCase() === spellCastingClass.toLowerCase() || i.system.tag === spellCastingClass.toLowerCase()
        )[0] ?? null;

      // If the class is found, update the spellbook updates
      if (classItem) {
        casterLevel =
          classItem.system.level + (isNaN(classItem.system.casting.offset) ? 0 : classItem.system.casting.offset);
        spellCastingClass = classItem.system.tag;
        const spellsKnownOffsets = SpellBooksParser.getHighestSpellLevelFreeSlots(
          spellCastingClass,
          classItem.system.level
        );

        delete spellBookUpdates["name"];
        spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
          class: spellCastingClass,
          kind: classItem.system.casting.spells,
          ability: classItem.system.casting.ability,
          arcaneSpellFailure: classItem.system.casting.spells === "arcane",
          domainSlotValue: classItem.system.casting.domain,
          casterType: classItem.system.casting.progression,
          hasCantrips: classItem.system.casting.cantrips,
          autoSpellLevelCalculation: true,
          "cl.-=autoSpellLevelCalculationFormula": "",
          psychic: classItem.system.casting.spells === "psychic",
          spellPreparationMode: classItem.system.casting.type,
          spells: spellsKnownOffsets,
          inUse: true,
        });

        // Check for prestige classes
        if (!isSpellLike) {
          let prestigeClasses = actor.itemTypes.class.filter(
            (c) =>
              c.system.subType === "prestige" &&
              c.system.tag !== classItem.system.tag &&
              !this.app.processData.characterData.prestigeClassCasting.has(c.system.tag)
          );

          if (prestigeClasses.length > 0) {
            for (const prestigeClass of prestigeClasses) {
              const prestigeKey = Object.keys(PrestigeClasses).find(
                (k) => k.toLowerCase() === prestigeClass.name.toLowerCase()
              );

              const prestigeData = PrestigeClasses[`${prestigeKey}`];
              if (prestigeData?.offset) {
                if (prestigeData.castingType) {
                  if (prestigeData.castingType.includes(spellBookUpdates["kind"]) === false) {
                    continue;
                  }
                }

                if (prestigeData.castingClass) {
                  if (prestigeData.castingClass.includes(spellBookUpdates["class"]) === false) {
                    continue;
                  }
                }

                // Update the spellbook with the prestige class offset
                delete spellBookUpdates["cl.-=autoSpellLevelCalculationFormula"];
                spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
                  "cl.autoSpellLevelCalculationFormula": prestigeData.offset,
                });

                // Update the caster level with the prestige class offset
                const offsetValue = await sbcUtils.processFormula(prestigeData.offset, actor);
                casterLevel += offsetValue.total;

                this.app.processData.characterData.prestigeClassCasting.set(prestigeClass.system.tag, spellBookType);
              }
            }
          }
        }
      }
    } else if (spellCastingClass === "hd") {
      spellBookUpdates = foundry.utils.mergeObject(spellBookUpdates, {
        class: "_hd",
        ability: "cha",
      });
    }

    // Check for and add the spellbook name if necessary
    if (spellBookName) {
      spellBookUpdates["name"] = spellBookName;
    }

    // Save Data needed for validation
    // and put it into the notes sections as well
    this.app.processData.characterData.conversionValidation.spellBooks[spellBookType] = {
      casterLevel: +casterLevel,
      concentrationBonus: +concentrationBonus,
    };

    // Run all the updates at once
    await actor.update({
      [`system.attributes.spells.spellbooks.${spellBookType}`]: spellBookUpdates,
    });

    return spellBookType;
  }

  /**
   * Process the spell row for spell level, spells per X and spells per X total
   *
   * @param {string} spellRow                     The spell row to process
   * @param {string} spellBookType                The spellbook type
   * @param {sbcTypes.SpellRowData} spellRowData  The spell row data object
   * @returns {Array}                             An array with the spell level, spells per X and spells per X total
   */
  async processSpellRow(spellRow, spellBookType, spellRowData) {
    // Check for Psychic Energy points
    let match;

    if ((match = spellRow.match(/^(\d+)\s*\bPE\b/)) !== null) {
      let PE = match[1];

      // Update psychic spell points
      await window.SBC.actor.update({
        [`system.attributes.spells.spellbooks.${spellBookType}.spellPoints`]: {
          maxFormula: PE,
          value: +PE,
        },
      });
      spellRowData.isSpellRow = true;
    }
    // Check for spell frequency. If one is given, this is the amount per interval
    else if ((match = spellRow.match(/(\d+)\s*(?:rounds)?\/(?:day|week|month|year)/i)) !== null) {
      spellRowData.spellsPerXTotal = match[1];
      spellRowData.isSpellRow = true;
    }
    // General spell level section
    else if ((match = spellRow.match(/(^\d)/)) !== null) {
      spellRowData.spellLevel = match[1];
      spellRowData.isSpellRow = true;
    }
    // Is this even needed?
    else if ((match = spellRow.match(/\d+\/([a-zA-Z]*)\)*-/)) !== null) {
      spellRowData.spellsPerX = match[1];
    }
  }

  /**
   * Process the frequencies of spell usage.
   *
   * Mainly, checking for whether a spell section is
   * At Will, Constant, Weekly, Monthly or Yearly
   *
   * @param {string} spellRow                     The spell row to process
   * @param {sbcTypes.SpellRowData} spellRowData  The spell row data object
   */
  processFrequencies(spellRow, spellRowData) {
    switch (true) {
      case /Constant/i.test(spellRow):
        spellRowData.isSpellRow = true;
        spellRowData.frequency = SpellFrequencyTypes.Constant;
        spellRowData.isAtWill = true;
        break;

      case /\d+\s*(?:rounds)?\/day/i.test(spellRow):
        spellRowData.frequency = SpellFrequencyTypes.Daily;
        break;

      case /\d+\s*(?:rounds)?\/week/i.test(spellRow):
        spellRowData.frequency = SpellFrequencyTypes.Weekly;
        break;

      case /\d+\s*(?:rounds)?\/month/i.test(spellRow):
        spellRowData.frequency = SpellFrequencyTypes.Monthly;
        break;

      case /\d+\s*(?:rounds)?\/year/i.test(spellRow):
        spellRowData.frequency = SpellFrequencyTypes.Yearly;
        break;

      case /At[- ]will/i.test(spellRow):
        spellRowData.isSpellRow = true;
        spellRowData.frequency = SpellFrequencyTypes.AtWill;
        spellRowData.isAtWill = true;
        break;

      default:
      // Does this need any handling?
    }
  }

  /**
   * Process the spell array element
   *
   * @param {string} spell                        The spell to process
   * @param {sbcTypes.SpellRowData} spellRowData  The spell row data object
   * @param {string} spellBookType                The spellbook type
   * @param {string} spellCastingType             The spell-casting type
   * @param {Array<string>} nameOptions           The name options
   * @param {Array<string>} castingClasses        The spell-casting class
   * @param {number} line                         The line number
   * @param {object} spellRowNotes                The spell row notes object
   * @param {boolean} featuresOnly                Whether to only process features
   */
  async processSpell(
    spell,
    spellRowData,
    spellBookType,
    spellCastingType,
    nameOptions,
    castingClasses,
    line,
    spellRowNotes,
    featuresOnly
  ) {
    // Process the spell name and get the related spell data and search terms
    let [spellName, altName, spellContext, isDomainSpell, isMythicSpell] = SpellBooksParser.processSpellName(spell);
    if (spellName === "") return;
    let [_result, entityItem] = [null, null];

    let searchEntity = {
      search: new Set([spellName, altName]),
      type: "spell",
      item: spellName,
    };

    searchEntity.search = sbcUtils.createSearchSet([spellName, altName], true, nameOptions.concat(castingClasses));

    // nameOptions.forEach((nameOption) => {
    //   searchEntity.search.add(`${spellName} (${nameOption})`);
    //   searchEntity.search.add(`${altName} (${nameOption})`);
    // });

    // castingClasses.forEach((castingClass) => {
    //   searchEntity.search.add(`${spellName} (${castingClass})`);
    //   searchEntity.search.add(`${altName} (${castingClass})`);
    // });

    // Find the spell entity, if it exists. Could be Spell or Feature.
    let [spellEntity, featureEntity] = await this.findSpellEntities(searchEntity, featuresOnly ? "classFeat" : "spell");
    let entity = featuresOnly ? featureEntity : spellEntity;

    // otherwise overwrite "entity" with a placeholder
    if (entity === null) {
      if (!featuresOnly) {
        entity = await sbcUtils.generatePlaceholderEntity(searchEntity, line);
      } else {
        searchEntity.type = "classFeat";
        entity = await sbcUtils.generatePlaceholderEntity(searchEntity, line);
      }
    }

    let foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, "feat", "classFeat", spellName, entity);

    if (!foundItem) {
      foundItem = sbcUtils.checkForDuplicateItem(window.SBC.actor, "feat", "misc", spellName, entity);
    }

    // Check for spell DC
    let spellDC = -1;
    const spellDCMatch = spellContext.match(/\bDC\b\s*(\d+)(?:,\s)?/);
    if (spellDCMatch) {
      spellDC = spellDCMatch[1];
      spellContext = spellContext.replace(spellDCMatch[0], "").trim();
    }

    // Check for multiple uses of a spell
    const multipleMatch = spellContext.match(
      /(?<![+-]\s*\d*\s*|level\s*)\b(\d+)\b(?!\s*d\s*\d*\s*[+-]|\s*lbs|\s*kg|\s*%)[,)]?/i
    );
    let spellMultiple = 1;
    if (multipleMatch) {
      spellMultiple = parseInt(multipleMatch[1]);
      spellContext = spellContext
        .replace(multipleMatch[1], "")
        .replace(/[,;]\s*$/, "")
        .trim();
    }

    if (entity.type === "feat" || foundItem) {
      if (!foundItem) {
        await sbcUtils.addAssociatedClass(entity, window.SBC.actor.itemTypes.class);
        [_result, entityItem] = await createItem(entity);
      } else entityItem = foundItem;

      await SpellBooksParser.setFeatureUses(entityItem, spellMultiple, spellRowData.frequency);

      spellRowNotes.spells.push({
        name: entityItem.name,
        prepCount: 1,
        dc: -1,
        domain: false,
      });

      return;
    }

    // Now check the school to match with the wizard school, if it exists
    if (spellRowData.wizardSchool && spellRowData.wizardSchool === entity.system.school) {
      isDomainSpell = true;
    }

    if (spellContext !== "") spell = sbcUtils.capitalize(spellName) + " (" + spellContext + ")";
    else spell = sbcUtils.capitalize(spellName);

    // Setup spell update data, starting with assigning it to the right spellbook
    let spellUpdates = {
      name: spell,
      "system.spellbook": spellBookType,
    };

    // Adjust the name with Mythic
    if (isMythicSpell) {
      spellUpdates["name"] += " (Mythic)";
    }

    // Set the spellLevel
    if (spellRowData.spellLevel !== -1) {
      spellUpdates["system.level"] = +spellRowData.spellLevel;
    }

    // Set the spell's PE cost, if any
    const peCostMatch = spell.match(/(\d+)\s*PE/);
    if (peCostMatch !== null) {
      spellUpdates["spellPoints.cost"] = `${peCostMatch[1]}`;
    }

    // Adjust the spell slots for the spellbook's given spell level
    await this.processSpellSlots(
      spellRowData,
      entity,
      spellUpdates,
      spellBookType,
      spellCastingType,
      spellMultiple,
      isDomainSpell
    );

    // Set At Will for spells marked as "at will"
    spellUpdates["system.atWill"] = spellRowData.isAtWill && (entity.system.level > 0 || spellRowData.isSpellLike);

    // Change SpellName to reflect frequency
    switch (spellRowData.frequency) {
      case SpellFrequencyTypes.Constant:
      case SpellFrequencyTypes.Weekly:
      case SpellFrequencyTypes.Monthly:
      case SpellFrequencyTypes.Yearly:
        spellUpdates["name"] = sbcUtils.translate(`spellFrequency.${spellRowData.frequency}`) + ": " + entity.name;
        break;

      default:
        // Nothing to do here.
        break;
    }

    // Set data for domain spells
    if (isDomainSpell) {
      Object.assign(spellUpdates, {
        "system.domain": true,
        "system.slotCost": 1,
      });
    }

    // Update the spell entity
    await entity.updateSource(spellUpdates);

    sbcUtils.log("Spell Updates", spellUpdates);
    // Create the spell entity
    try {
      [_result, entityItem] = await createItem(entity);
    } catch (err) {
      console.error(err);
    }

    await entityItem.update(spellUpdates);

    // Set the spellDC
    // This is the offset for the dc, not the total!
    await this.processSpellDC(entityItem, spellDC, spellBookType);

    spellRowNotes.spells.push({
      name: entityItem.name,
      prepCount: spellMultiple,
      dc: spellDC,
      domain: !entity.system?.school ? isDomainSpell : false,
    });
  }

  /**
   * Process the spell slots for the spellbook's given spell level
   *
   * @param {sbcTypes.SpellRowData} spellRowData  The spell row data object
   * @param {ItemSpellPF} entity                  The entity item
   * @param {object} spellUpdates                 The spell update object
   * @param {string} spellBookType                The spellbook type
   * @param {string} spellCastingType             The spell-casting type
   * @param {number} spellMultiple                The number of times a spell can be cast
   * @param {boolean} isDomainSpell               Whether the spell is a domain spell
   * @returns {Promise<void>}
   */
  async processSpellSlots(
    spellRowData,
    entity,
    spellUpdates,
    spellBookType,
    spellCastingType,
    spellMultiple,
    isDomainSpell
  ) {
    // Set the spells uses / preparation
    // where SLAs can be cast a number of times per X per sla
    // and spontaneous spells of a given spellLevel can be cast a total of X times per day
    //
    const actor = window.SBC.actor;
    // Initialize some values for the row
    if (!spellRowData.spellRowIsInitialized) {
      await actor.update({
        [`system.attributes.spells.spellbooks.${spellBookType}.spells.spell${entity.system.level}.base`]: 0,
      });
      spellRowData.spellRowIsInitialized = true;
    }

    // Do not count Constant and At Will spells towards spell slot totals
    if (!spellRowData.isAtWill && !spellRowData.isConstant) {
      // Spell-Like Abilities can be cast a number of times per day each
      if (spellRowData.spellsPerXTotal !== -1 && spellRowData.isSpellLike) {
        Object.assign(spellUpdates, {
          "system.uses.max": +spellRowData.spellsPerXTotal,
          "system.uses.value": +spellRowData.spellsPerXTotal,
          "system.uses.per": spellRowData.spellsPerX,
          "system.preparation.max": +spellRowData.spellsPerXTotal,
          "system.preparation.value": +spellRowData.spellsPerXTotal,
        });

        if (entity.system.level === 0) {
          Object.assign(spellUpdates, {
            "system.uses.autoDeductChargesCost": "1",
          });
        }

        spellRowCounts[spellBookType][`${entity.system.level}`].base += +spellRowData.spellsPerXTotal;
        spellRowCounts[spellBookType][`${entity.system.level}`].max += +spellRowData.spellsPerXTotal;
      }

      // Spells Known can be cast a number of times per day in total for the given spellRow
      else if (spellRowData.spellsPerXTotal !== -1 && spellCastingType === "spontaneous" && !spellRowData.isSpellLike) {
        spellRowCounts[spellBookType][`${entity.system.level}`].base = +spellRowData.spellsPerXTotal;
        spellRowCounts[spellBookType][`${entity.system.level}`].max = +spellRowData.spellsPerXTotal;
      }

      // Spells Prepared can be cast according to how often they are prepared
      else if (spellCastingType === "prepared" && spellRowData.spellsPerXTotal === -1) {
        Object.assign(spellUpdates, {
          "system.preparation.max": spellMultiple,
          "system.preparation.value": spellMultiple,
        });

        spellRowCounts[spellBookType][`${entity.system.level}`].base += spellMultiple;
        spellRowCounts[spellBookType][`${entity.system.level}`].max += spellMultiple;
      }

      // Subtract Domain Spells from the count
      if (isDomainSpell) {
        spellRowCounts[spellBookType][`${entity.system.level}`].base -= 1;
        spellRowCounts[spellBookType][`${entity.system.level}`].max -= 1;
      }
    } else if (entity.system.level === 0 && spellCastingType === "prepared") {
      Object.assign(spellUpdates, {
        "system.preparation.max": spellMultiple,
        "system.preparation.value": spellMultiple,
      });
    }
  }

  /**
   * Process the spell DC
   *
   * @param {ItemSpellPF} entity          The entity item
   * @param {number} spellDC              The spell DC
   * @param {string} spellBookType        The spellbook type
   */
  async processSpellDC(entity, spellDC, spellBookType) {
    let spellDCOffset;

    // Calculate the DC in the Actor
    const spellCastingAbility = window.SBC.actor.system.attributes.spells.spellbooks[spellBookType].ability;
    const spellCastingAbilityModifier = window.SBC.actor.system.abilities[spellCastingAbility].mod;
    const spellDCInActor = 10 + +entity.system.level + +spellCastingAbilityModifier;

    spellDCOffset = +spellDC - +spellDCInActor;

    // Try to get the action to adjust the DC
    if (spellDC !== -1 && entity.system.actions.length > 0) {
      let actions = entity.system.actions;
      const spellAction = entity.actions.get(entity.actions.contents[0].id);

      if (spellAction) {
        await spellAction.update({
          "save.dc": spellDCOffset,
        });
        actions[0] = spellAction;
        await entity.update({
          "system.actions": actions,
        });
      }
    }
  }

  /**
   * Parse special abilities of spellbooks
   *
   * @param {string} value          The value to parse
   * @param {number} line           The line number
   * @param {string} spellBookType  The spellbook type
   * @returns {Promise<string>}     The special row
   */
  async parseSpellbookSpecials(value, line, spellBookType) {
    let specialRow = null;
    let name = "";
    let type = "";
    let match;
    let nameCount = 0;
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;
    const searchEntity = {
      search: new Set(),
      type: "feat",
    };
    const searchEntityOptions = {
      itemType: "feat",
      itemSubType: "classFeat",
      classes: this.app.processData.characterData.classes,
      race: this.app.processData.characterData.race,
    };

    const actor = window.SBC.actor;
    do {
      // Check for Domains
      if (
        (match = value.match(/(?!Domain spell)Domains?\s*(.*?)(?:;|$)/i)) !== null ||
        (match = value.match(/Domain\s*spells*\s*\((.*?)\)\s*(?:;|$)/i)) !== null
      ) {
        name = sbcUtils.translate("spellFeature.domain") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          let subTerm = term.match(/\((.*) subdomain\)/i);
          if (subTerm) {
            searchEntity.search.add(`${subTerm[1].capitalize()} Subdomain`);
            searchEntity.search.add(subTerm[1].capitalize());
          } else {
            searchEntity.search.add(`${term.capitalize()} Domain`);
            searchEntity.search.add(`${term.capitalize()} Subdomain`);
            searchEntity.search.add(term.capitalize());
          }

          searchEntity.item = term;
          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }

        // Create Class Feature for the Domain
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "domains";
        break;
      }

      // Check for Mysteries
      if ((match = value.match(/Myster[y|ies]\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.mystery") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} Mystery`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Mystery
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "mysteries";
        break;
      }

      // Check for Spirits
      if ((match = value.match(/(?!Spirit magic spell)Spirits?\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.spirit") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} Spirit`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Spirit
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "spirits";
        break;
      }

      // Check for Bloodline
      if ((match = value.match(/Bloodlines?\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.bloodline") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} Bloodline`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Bloodline
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "bloodlines";
        break;
      }

      // Check for Patron
      if ((match = value.match(/Patrons?\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.patron") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} Patron`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Bloodline
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "patrons";
        break;
      }

      // Check for Opposition Schools
      if ((match = value.match(/(Opposition|Prohibited) Schools\s(.*?)(?:;|$)/i)) !== null) {
        let [_a, oppositionTag, oppositionSchools] = match;
        // Create Class Feature for the Opposition school
        name = sbcUtils.translate(`spellFeature.${oppositionTag.toLowerCase()}`) + ": " + oppositionSchools;
        type = "oppositions";
      }

      // Check for Thassilonian Specialist
      if ((match = value.match(/Thassilonian Specialization\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.thassilonianSpecialist") + ": ";
        if (match[1]) {
          await actor.update({
            [`system.attributes.spells.spellbooks.${spellBookType}.domainSlotValue`]: 2,
          });
        }
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} School`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Thassilonian Specialization
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "thassilonianSpecialist";
        break;
      }

      // Check for Inquisition
      if ((match = value.match(/Inquisitions?\s*(.*?)(?:;|$)/i)) !== null) {
        name = sbcUtils.translate("spellFeature.inquisition") + ": ";
        specialRow = {
          label: name.replace(":", ""),
          value: match[1],
        };
        for (let term of match[1].split(",")) {
          term = sbcUtils.parseSubtext(term)[0];

          searchEntity.search.add(`${term.capitalize()} Inquisition`);
          searchEntity.search.add(term.capitalize());
          searchEntity.item = term;

          const entity = await compendiumOps.findEntityInCompendia(searchEntity, searchEntityOptions);
          searchEntity.search.clear();

          if (entity) {
            await sbcUtils.addAssociatedClass(entity, actor.itemTypes.class);
            await createItem(entity);
          } else {
            name += `${term}, `;
            nameCount++;
          }
        }
        // Create Class Feature for the Bloodline
        name = nameCount > 0 ? name.slice(0, -2) : "";
        type = "inquisition";
        break;
      }
      // eslint-disable-next-line no-constant-condition
    } while (false);

    // If we found something, create a placeholder entity
    if (name !== "") {
      let placeholder = await sbcUtils.generatePlaceholderEntity({ name, type }, line);
      await createItem(placeholder);
    }

    // Add the special row to the notes
    return specialRow;
  }

  /**
   *
   * @param {string} spell  The spell text to process
   * @returns {Array<string|boolean>}       An array with the spell name, alt name, domain spell and mythic spell
   * @static
   */
  static processSpellName(spell) {
    let isDomainSpell = false;
    let isMythicSpell = false;
    let spellText = sbcUtils.parseSubtext(spell);
    let spellName = spellText[0].trim();
    let spellContext = spellText[1]?.trim() ?? "";
    let spellLeftover = spell
      .replace(new RegExp(`(${escapeForRegex(spellName)}\\s|\\(${escapeForRegex(spellContext)}\\))`, "g"), "")
      .trim();
    let altName = spellName;

    if (spellName === "") return [spellName, altName, spellContext, isDomainSpell, isMythicSpell]; // Skip empty spell names

    // Check if the spell is a domain spell
    if (spellName.endsWith("D")) {
      isDomainSpell = true;
      spellName = spellName.substring(0, spellName.length - 1);
    } else if (spellContext.endsWith("D")) {
      isDomainSpell = true;
      spellContext = spellContext.substring(0, spellContext.length - 1);
    } else if (spellLeftover.endsWith("D")) {
      isDomainSpell = true;
    }

    if (spellName.endsWith("M")) {
      isMythicSpell = true;
      spellName = spellName.substring(0, spellName.length - 1);
    } else if (spellContext.endsWith("M")) {
      isMythicSpell = true;
      spellContext = spellContext.substring(0, spellContext.length - 1);
    } else if (spellLeftover.endsWith("M")) {
      isMythicSpell = true;
    }

    // Filter metamagics out of the spell name
    const metamagicFeats = spellName.match(new RegExp(`(${Metamagic.join("\\b|\\b")})`, "gi"));

    if (metamagicFeats) {
      altName = spellName.replace(new RegExp(`(${metamagicFeats.join("\\b|\\b")})`, "gi"), "").trim();
    }

    return [spellName, altName, spellContext, isDomainSpell, isMythicSpell];
  }

  /**
   * Find the spell entities in the compendiums, looking for a spell and a feature version
   *
   * @param {sbcTypes.SearchEntityData} searchEntity  The search entity object
   * @param {string} type       The type of entity to search for (spell, classFeat, both)
   * @returns {Promise<Array>}  An array with the spell entity and the feature entity
   */
  async findSpellEntities(searchEntity, type = "both") {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    // Search for the spell in the compendium
    let [spellEntity, featureEntity] = [null, null];

    if (type === "both" || type === "spell") {
      // Search for the spell
      spellEntity = await compendiumOps.findEntityInCompendia(searchEntity, {
        itemTypes: ["spell"],
      });
    }

    if (type === "both" || type === "classFeat") {
      // Search for the feature
      featureEntity = await compendiumOps.findEntityInCompendia(searchEntity, {
        itemTypes: ["feat"],
        itemSubTypes: ["classFeat", "misc"],
        //        classes: this.app.processData.characterData.classes,
        race: this.app.processData.characterData.race,
      });
    }

    return [spellEntity, featureEntity];
  }

  /**
   * Get the default spellbook data for when we need to define a new book or reset an existing one
   *
   * @returns {object}  The default spellbook data
   * @static
   */
  static getDefaultSpellbookData() {
    return {
      name: "",
      inUse: false,
      castPerDayAllOffsetFormula: "",
      preparedAllOffsetFormula: "",
      casterType: "high",
      class: "",
      cl: {
        formula: "",
        total: 0,
      },
      concentrationFormula: "",
      concentrationNotes: "",
      clNotes: "",
      ability: "int",
      autoSpellLevelCalculation: true,
      autoSpellLevels: true,
      psychic: false,
      arcaneSpellFailure: true,
      hasCantrips: true,
      spellPreparationMode: "spontaneous",
      baseDCFormula: "10 + @sl + @ablMod",
      spellPoints: {
        useSystem: false,
        value: 0,
        maxFormula: "",
        restoreFormula: "",
      },
      spells: {
        spell0: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell1: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell2: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell3: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell4: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell5: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell6: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell7: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell8: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
        spell9: {
          castPerDayOffsetFormula: "",
          preparedOffsetFormula: "",
        },
      },
      spellSlotAbilityBonusFormula: "",
      domainSlotValue: 1,
      concentration: {
        total: 0,
      },
      isSchool: true,
    };
  }

  /**
   * Get the class postfix labels from the spellbook name
   * For example, the spellbook name "Domain" will return ["CLE", "DRU", "INQ"] because it is used for Clerics, Druids and Inquisitors
   *
   * @param {string} spellBookName  The spellbook name
   * @returns {Array<string>}       An array with the class postfix labels
   * @static
   */
  static getClassPostfixFromSpellbookName(spellBookName) {
    if (!spellBookName) return [];
    spellBookName = spellBookName.toLowerCase();
    if (spellBookName.includes("domain")) return ["CLE", "DRU", "INQ"];
    else if (spellBookName.includes("bloodline")) return ["SOR", "BLO"];
    else if (spellBookName.includes("school")) return ["WIZ"];
    else if (spellBookName.includes("mystery")) return ["ORA"];
    else if (spellBookName.includes("patron")) return ["WIT"];
    else if (spellBookName.includes("spirit")) return ["SHA"];
    else return [];
  }

  /**
   * Processes the spontaneous class to adjust the spells known
   *
   * @param {string} tag    The tag of the class
   * @param {number} level  The level of the class
   * @returns {object}      The updates object to be merged in.
   */
  static getHighestSpellLevelFreeSlots(tag, level) {
    let spellLevel = 0;

    switch (tag) {
      case "bloodrager":
        spellLevel = Math.max(0, Math.ceil((level - 6) / 3));
        break;
      case "sorcerer":
        spellLevel = Math.max(0, Math.ceil(level - 2) / 2);
        break;
      case "oracle":
        spellLevel = Math.max(0, Math.floor(level / 2));
        break;
      case "psychic":
        spellLevel = Math.max(1, Math.floor(level / 2));
        break;
      default:
        break;
    }

    let updates = {};
    if (spellLevel > 0) {
      for (let i = 1; i <= spellLevel; i++) {
        updates[`spell${i}.preparedOffsetFormula`] = "1";
      }
    }

    return updates;
  }

  /**
   * Set the uses of a feature in place of the spell's uses in a given period
   *
   * @param {import("../../../pf1/module/documents/item/item-pf.mjs").ItemPF} item  The item to update
   * @param {number} quantity   The quantity to set
   * @param {string} period     The period to set
   */
  static async setFeatureUses(item, quantity, period) {
    let updates = {
      "system.uses.value": quantity,
      "system.uses.max": quantity,
    };

    if (!item.system.uses.maxFormula) {
      updates["system.uses.maxFormula"] = `${quantity}`;
    }

    switch (period) {
      case SpellFrequencyTypes.Weekly:
        updates["system.uses.per"] = "week";
        break;
      default:
        updates["system.uses.per"] = "day";
        break;
    }

    await item.update(updates);
  }
}
