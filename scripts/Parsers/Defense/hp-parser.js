import { sbcUtils } from "../../sbcUtils.js";
import { parserMapping } from "../parser-mapping.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// const HDTypes = Object.freeze({
//   RacialOnly: Symbol("RacialOnly"),
//   ClassOnly: Symbol("ClassOnly"),
//   ClassAndRacial: Symbol("ClassAndRacial"),
// });

// Parse HP, HD and special HD/HP Abilities (like Regeneration or Fast Healing)
export default class HPParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "hitPoints";
  }

  /**
   * Parses a hit points string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    // Check the current Items for classes and racialHD
    /** @type {Array<import("../../../pf1/module/documents/item/item-class.mjs").ItemClassPF>} */
    let classItems = window.SBC.actor.itemTypes.class;
    let maximzizeFirstHD = false;

    // Calculate the HP for the statblock
    const [_hpValue, hdInput, hdPool, hpBonus, hpAbilities] = this.processHPString(value);
    sbcUtils.log(`HP: ${hdPool} and Bonus: ${hpBonus}`);
    let hdTotal = 0;
    let hdTotalRacial = 0;
    let calculatedStatblockHP =
      hdPool.reduce((result, hd) => {
        const [hdCount, hdSize] = hd.split("d").map((value) => parseInt(value));
        hdTotal += hdCount;
        return result + hdCount * sbcUtils.getDiceAverage(hdSize);
      }, 0) + hpBonus;

    // Calculate the number of racial hit dice by subtracting all class levels
    // This will do nothing if there is no racial class item.
    hdTotalRacial =
      hdTotal -
      classItems.reduce(
        (result, classItem) => result + (classItem.system.subType === "mythic" ? 0 : classItem.system.level),
        0
      );
    const racialHDItem = classItems.find((classItem) => classItem.system.subType === "racial");
    if (racialHDItem) {
      await racialHDItem.update({
        "system.level": hdTotalRacial,
        "system.hp": hdTotalRacial * sbcUtils.getDiceAverage(racialHDItem.system.hd),
      });
    }

    // The HP as it exists on the actor, after classes, Constitution or Charisma bonus, and items
    let calculatedActorHP = window.SBC.actor.system.attributes.hp.value;
    let hpDifferenceStatblockActor = calculatedStatblockHP - calculatedActorHP;

    // Find the first class that *could* be a favored class (it's a pretty safe bet that this is so)
    // If it's a base class, check for maximizing to adjust the HP difference.
    // If the HP difference is equal to the level, set the favored class HP to the level.
    const favoredClass = classItems.find(
      (classItem) => classItem.system.subType === "base" || classItem.system.subType === "npc"
    );
    if (favoredClass) {
      if (favoredClass.system.subType === "base") {
        if (!maximzizeFirstHD) {
          maximzizeFirstHD = true;
          sbcUtils.log(
            `Maximizing first HD for ${favoredClass.name} - ${favoredClass.system.hd} - ${sbcUtils.getDiceAverage(favoredClass.system.hd)}`
          );
          const offsetHPAverage = favoredClass.system.hd - sbcUtils.getDiceAverage(favoredClass.system.hd);
          calculatedStatblockHP += offsetHPAverage;
          hpDifferenceStatblockActor += offsetHPAverage;
        }
      }

      if (hpDifferenceStatblockActor === favoredClass.system.level) {
        await favoredClass.update({ "system.fc.hp.value": favoredClass.system.level });
        hpDifferenceStatblockActor -= favoredClass.system.level;
        calculatedActorHP += favoredClass.system.level;
      }
    }

    // Calculate the HP using the system HP settings.
    // These can be accessed using the following command:
    // game.settings.get("pf1", "healthConfig")
    const calculatedSystemHP = await calculateSystemHP(window.SBC.actor, classItems);
    sbcUtils.log(
      `HP Data: Statblock: ${calculatedStatblockHP}, Actor: ${calculatedActorHP}, System: ${calculatedSystemHP}`
    );

    sbcUtils.log(`HP Difference between Statblock and Actor: ${hpDifferenceStatblockActor}`);
    const hpDifferenceSystemActor = calculatedSystemHP - calculatedActorHP;
    sbcUtils.log(`HP Difference between System and Actor: ${hpDifferenceSystemActor}`);
    const hpDifferenceStatblockSystem = calculatedStatblockHP - calculatedSystemHP;
    sbcUtils.log(`HP Difference between Statblock and System: ${hpDifferenceStatblockSystem}`);

    // Account for the the HP difference between the statblock and the system-calculated HP
    const hpTotal = roundHPTotal(calculatedStatblockHP + (_hpValue - calculatedStatblockHP + hpDifferenceSystemActor));
    this.app.processData.characterData.conversionValidation.attributes["hpTotal"] = hpTotal;

    // Save Total HP and HD for the preview
    this.app.processData.notes.defense["hpTotal"] = hpTotal;
    this.app.processData.notes.defense["hdTotal"] = hdTotal;
    this.app.processData.notes.defense["hdPool"] = hdInput;

    await this.resetRacialClassesWithNoLevels();

    // Set the current value of the actor hp
    //window.SBC.actor.update({ "system.attributes.hp.value": +hpValue });

    // If there is data after the hd in brackets, add it as a special hdAbility
    if (!hpAbilities.trim().length) return true;

    const hdAbilities = await checkHDAbilities(this.splitText(hpAbilities, false), line);

    this.app.processData.notes.defense["hdAbilities"] = hdAbilities.join(", ");

    return true;
  }

  /**
   * Processes the HP string and returns the HP and HD
   *
   * @param {string} value                  The value to process
   * @returns {[number, string, RegExpMatchArray, number, string]}  The HP value, HD and HP bonus
   */
  processHPString(value) {
    const input = this.splitText(value, false);

    // Get the HitDice and the bonus HP
    const [hpValue, hdInput] = sbcUtils.parseSubtext(input[0]);

    // Get the total hp (only numbers!)
    const hdPool = hdInput.match(/(\d+d\d+)/g);

    // HP Bonus Pool
    const hpBonusPool = hdInput.match(/(\b[^d+\s]*\d+[^\sd+]*\b)(?!\s*HD)/gi);
    // Check, if there are Bonus HP
    const hpBonus = hpBonusPool?.reduce((result, bonus) => result + parseInt(bonus), 0) ?? 0;

    return [hpValue, hdInput, hdPool, hpBonus, input[1] || ""];
  }

  /**
   * Reset the saving throws for all racial classes that have no levels
   *
   * @returns {Promise<void>}
   */
  async resetRacialClassesWithNoLevels() {
    /** @type {import("../../../pf1/module/documents/item/item-class.mjs").ItemClassPF[]} */
    let classesToDelete = [];

    window.SBC.actor.itemTypes.class
      .filter((classItem) => classItem.system.level === 0)
      .forEach(async (classItem) => {
        if (classItem.system.subType === "racial") {
          classesToDelete.push(classItem);
        } else {
          await classItem.update({
            "system.savingThrows": {
              fort: {
                value: "",
                base: 0,
                good: classItem.system.savingThrows.fort.good,
              },
              ref: {
                value: "",
                base: 0,
                good: classItem.system.savingThrows.ref.good,
              },
              will: {
                value: "",
                base: 0,
                good: classItem.system.savingThrows.will.good,
              },
            },
          });
        }
      });

    if (classesToDelete.length) {
      let race = window.SBC.actor.itemTypes.race.length;
      if (!race) {
        let firstClass = classesToDelete[0];
        const nameData = sbcUtils.parseSubtext(firstClass.name);
        let raceEntityData = {
          name: nameData[0],
          creatureType: nameData[0].toLowerCase(),
          desc: firstClass.system.description.value,
          img: firstClass.img,
          size: window.SBC.actor.system.traits.size.base,
          subTypes: nameData.length > 1 ? nameData[1].replace(/\s/g, "").split(",") : "",
          type: "race",
        };

        await createItem(await sbcUtils.generatePlaceholderEntity(raceEntityData));
      }

      window.SBC.actor.deleteEmbeddedDocuments(
        "Item",
        classesToDelete.map((item) => item.id)
      );
    }
  }
}

/**
 * Calculates the HP according to system settings
 *
 * @param {import("../../../pf1/module/documents/actor/actor-pf.mjs").ActorPF} actor          The actor to calculate the HP for
 * @param {Array<import("../../../pf1/module/documents/item/item-pf.mjs").ItemPF>} classItems The class items to calculate the HP for
 * @returns {Promise<number>} The calculated HP
 */
async function calculateSystemHP(actor, classItems) {
  let calculatedSystemHP = 0;

  /** @type {sbcTypes.hpSettings} */
  const systemConfig = game.settings.get("pf1", "healthConfig");

  /** @type {sbcTypes.hpSettings} */
  const hpSettings = {
    continuous: systemConfig.continuous,
    maximized: systemConfig.maximized,
    rounding: systemConfig.rounding,
    maximizedProgress: 0,
  };

  // Loop through all classes and calculate the HP according to the system settings
  for (const classItem of classItems) {
    const classType = classItem.system.subType;
    /** @type {sbcTypes.hdSettings} */
    const hdSettings = game.settings.get("pf1", "healthConfig").getClassHD(classItem);

    switch (classType) {
      // These are coverd by the system settings, which are generically gathered in hdSettings
      case "base":
      case "prestige":
      case "npc":
      case "racial": {
        let hdCount = classItem.system.level;
        let hdCountMaximized = 0;

        // Check if we maximize this class type AND if we have any levels left to maximize on the actor
        if (hdSettings.maximized && hpSettings.maximizedProgress < hdSettings.maximized) {
          hdCountMaximized = Math.min(hpSettings.maximized - hpSettings.maximizedProgress, hdCount);
          hdCount -= hdCountMaximized;
          hpSettings.maximizedProgress += hdCountMaximized;
        }

        const classHP = calculateSystemHPForClass(
          hpSettings,
          hdSettings,
          classItem.system.hd,
          hdCount,
          hdCountMaximized
        );
        calculatedSystemHP += classHP + classItem.system.fc.hp.value;

        // Update the classItem with the new HP total
        await classItem.update({ "system.hp": classHP });

        break;
      }
      // Nothing to touch here, as Mythic HP was already calculated and isn't affected by system settings.
      case "mythic":
        calculatedSystemHP += classItem.system.hp;
        break;
      default:
        break;
    }
  }

  // Adjust calculated system HP to include the ability modifier
  const hdTotal = actor.system.attributes.hd.total;
  const hpAbility = actor.system.attributes.hpAbility;
  calculatedSystemHP += hdTotal * actor.system.abilities[hpAbility].mod;

  // Adjust calculated system HP to include changes from items
  calculatedSystemHP += await sbcUtils.getTotalFromChanges(actor, "mhp");

  return calculatedSystemHP;
}

/**
 * Calculates the HP for a class based on the system settings
 *
 * @param {sbcTypes.hpSettings} hpSettings      The HP settings
 * @param {sbcTypes.hdSettings} hdSettings      The HD settings
 * @param {number} hdSize                       The size of the HD
 * @param {number} hdCount                      The number of HD
 * @param {number} hdCountMaximized             The number of maximized HD
 * @returns {number}                            The calculated HP
 */
function calculateSystemHPForClass(hpSettings, hdSettings, hdSize, hdCount, hdCountMaximized) {
  let hpTotal = 0;
  const hdAverage = sbcUtils.getDiceAverage(hdSize, hdSettings.rate);

  let continuity = hpSettings.continuous;
  if (!continuity) {
    // We're discreet, so we round per level
    for (let i = 0; i < hdCount; i++) {
      hpTotal += roundHPTotal(hdAverage);
    }
  } else {
    // We're continuous, so we round at the end
    hpTotal = roundHPTotal(hdAverage * hdCount);
  }

  // Add the maximized HD
  hpTotal += hdCountMaximized * hdSize;

  return hpTotal;
}

/**
 * Rounds the HP total according to the system settings
 *
 * @param {number} value  The value to round
 * @returns {number}      The rounded value
 * @private
 */
function roundHPTotal(value) {
  switch (game.settings.get("pf1", "healthConfig").rounding) {
    case "up":
      value = Math.ceil(value);
      break;
    case "down":
      value = Math.floor(value);
      break;
    case "nearest":
      value = Math.round(value);
      break;
    default:
      break;
  }

  return value;
}

/**
 * Checks the input for fast-healing and regeneration abilities
 *
 * @param {string[]} input              The input line with HP data
 * @param {number} line                 The line number of the input
 * @returns {Promise<Array<string>>}    The list of hdAbilities
 */
async function checkHDAbilities(input, line) {
  let hdAbilities = [];

  let hdAbilitiesPattern = new RegExp("\\b(regeneration|fast[ -]healing)\\b", "gi");

  for (let tempInput of input) {
    tempInput = tempInput.trim();

    if (tempInput.length === 0) {
      continue;
    }

    // Check, if the input matches "regeneration" or "fast healing"
    if (tempInput.search(hdAbilitiesPattern) === -1) {
      // Generate a placeholder for every hdAbility that is not accounted for in the character sheet
      const hdAbility = {
        name: tempInput,
        type: "misc",
      };

      hdAbilities.push(hdAbility.name);
      const placeholder = await sbcUtils.generatePlaceholderEntity(hdAbility, line);

      await createItem(placeholder);
      continue;
    }

    // Input the hdAbility into the correct places in the sheet
    const hpAbilityType = tempInput.match(hdAbilitiesPattern)[0].toLowerCase();

    switch (hpAbilityType) {
      case "regeneration": {
        const parser = parserMapping.map.defense.regeneration;
        await parser.parse(tempInput, line);
        break;
      }

      case "fast healing":
      case "fast-healing": {
        const parser = parserMapping.map.defense.fastHealing;
        await parser.parse(tempInput, line);
        break;
      }

      default:
        break;
    }

    hdAbilities.push(tempInput);
  }

  return hdAbilities;
}
