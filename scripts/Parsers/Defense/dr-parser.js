import { AbstractParser } from "../abstract-parser.js";
import { sbcUtils } from "../../sbcUtils.js";
import { damageTypesRegex } from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse Damage Reductions
export default class DRParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "damageReduction";
  }

  /**
   * Parses a damage reduction string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    const rawInput = value.replace(/(^[,;\s]*|[,;\s]*$)/g, "");
    const input = this.splitText(rawInput);

    this.app.processData.notes.defense.dr = [];

    // Get the current DR data
    /** @type {Array<sbcTypes.DamageReductionEntry>} */
    let drValues = window.SBC.actor.system.traits.dr.value;
    /** @type {string} */
    let drCustom = window.SBC.actor.system.traits.dr.custom;

    for (let reduction of input) {
      reduction = reduction.replace(/Effects/gi, "").trim();

      // Execute a regex to parse the DR string into groups
      const re = /^(?<amount>\d+)\s*\/\s*(?<type1>.+?)(?:\s*(?<operator>or|and)\s*(?<type2>.+?))?$/g.exec(reduction);
      let { amount, type1, operator, type2 } = re.groups;
      console.log(
        sbcUtils.translate("parser.damageReduction.parseInfo"),
        amount,
        type1,
        operator,
        type2,
        type1?.search(damageTypesRegex()),
        type2?.search(damageTypesRegex())
      );

      // If we found one or two types and at least one of them is a valid damage type,
      // then we can add a new DR entry
      if ((type1 && type1.match(damageTypesRegex())) || (type2 && type2.match(damageTypesRegex()))) {
        type1 = type1?.match(damageTypesRegex())?.[0];
        type2 = type2?.match(damageTypesRegex())?.[0];
        const types = [type1, type2].filter((x) => !!x).map((x) => pf1.utils.createTag(x));

        drValues.push({
          amount: amount,
          types: types,
          operator: operator === "or",
        });

        this.app.processData.notes.defense.dr.push(
          [
            amount,
            "/",
            this.getDamageTypePhrase(types[0]),
            ...(types[1] ? [" ", sbcUtils.translate(operator), " ", this.getDamageTypePhrase(types[1])] : []),
          ].join("")
        );
      }
      // It's a custom reduction, as there is no place for that, just put it into custom reductions
      else {
        drCustom += `${reduction.capitalize()};`;
        this.app.processData.notes.defense.dr.push(reduction.capitalize());
      }
    }

    await window.SBC.actor.update({
      "system.traits.dr.value": drValues,
      "system.traits.dr.custom": drCustom.replace(/;$/, ""),
    });
    return true;
  }

  /**
   * Get the corresponding damage type phrase for the damage type
   *
   * @param {string} damageType       The key of the damage type
   * @returns {string}                The phrase for the damage type
   */
  getDamageTypePhrase(damageType) {
    if (pf1.config.damageResistances[damageType]) {
      return pf1.config.damageResistances[damageType];
    }

    const materialTypeLabels = pf1.registry.materials.getLabels();
    if (materialTypeLabels[damageType]) {
      return materialTypeLabels[damageType];
    }

    const damageTypeLabels = pf1.registry.damageTypes.getLabels();
    if (damageTypeLabels[damageType]) {
      return damageTypeLabels[damageType];
    }

    return "";
  }
}
