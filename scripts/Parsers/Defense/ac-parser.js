import { AbstractParser } from "../abstract-parser.js";
import { sbcUtils } from "../../sbcUtils.js";
import { acTypesRegex } from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse AC Types
export default class ACParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Defense";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "armorClass";
  }

  /**
   * Parses an AC string
   *
   * @param {string} value        The value to parse
   * @param {number} _line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    // Separate Context Notes from ACTypes
    const rawAcTypes = value.split(";");

    // If there are context notes, set them in the actor
    if (rawAcTypes.length > 1) {
      this.app.processData.miscNotes.acNotes.push(rawAcTypes[1].trim());
    }

    const foundAcTypes = rawAcTypes[0].split(",");
    // Get supported AC Types

    this.app.processData.notes.defense.acTypes = [];

    for (const foundAc of foundAcTypes) {
      const foundAcType = foundAc.match(acTypesRegex())?.[0]?.toLowerCase();
      let foundAcTypeValue = parseInt(foundAc.match(/[+-]\d+/)?.[0]) ?? 0;
      let foundAcTypeNote = foundAcTypeValue;

      console.log(
        sbcUtils.translate("parser.armorClass.parseInfo", {
          type: foundAcType,
          value: foundAcTypeValue,
        })
      );

      switch (foundAcType) {
        case "natural":
          foundAcTypeValue -= await sbcUtils.getTotalFromChanges(window.SBC.actor, "nac");
          if (foundAcTypeValue !== 0) this.app.processData.miscNotes.naturalAC = foundAcTypeValue;
          break;

        case "size":
        case "dex":
          // Ignore these cases, as they are handled by foundry
          break;

        // Armor and Shield need to be validated against the ac changes in equipment
        case "armor":
        case "shield":
        case "base":
        case "enhancement":
        case "dodge":
        case "inherent":
        case "deflection":
        case "morale":
        case "luck":
        case "sacred":
        case "insight":
        case "resistance":
        case "profane":
        case "trait":
        case "racial":
        case "competence":
        case "circumstance":
        case "alchemical":
        case "penalty":
        case "rage":
        case "monk":
        case "wis":
        case "untyped":
        case "untypedPerm":
          /* Try to put these in the front of the conversion,
           * so that acNormal, etc. get handled after handling
           * acTypes */
          foundAcTypeValue -= await sbcUtils.getTotalFromChanges(window.SBC.actor, "ac", foundAcType);
          if (foundAcTypeValue !== 0) {
            this.app.processData.characterData.conversionValidation.attributes[foundAcType] = foundAcTypeValue;
          }
          break;

        default:
          break;
      }

      this.app.processData.notes.defense.acTypes.push({
        type: sbcUtils.translate(`armorTypes.${foundAcType}`),
        value: foundAcTypeNote >= 0 ? `+${foundAcTypeNote}` : foundAcTypeNote,
      });
    }

    return true;
  }
}
