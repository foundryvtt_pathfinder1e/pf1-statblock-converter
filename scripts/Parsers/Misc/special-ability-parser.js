import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";
import { specialAbilityRegex, specialAbilityTypesRegex } from "../../sbcRegex.js";

// Parse Special Abilities
export default class SpecialAbilityParser extends AbstractParser {
  constructor(app) {
    super(app);
    this.specialAbilityCount = 0;
  }

  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Misc";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "specialAbility";
  }

  /**
   * Parses a special ability string
   *
   * @param {string} value                  The value to parse
   * @param {number} line                   The line number
   * @param {string} _type                  The type of value
   * @returns {Promise<boolean>}            The operation result
   */
  async _parse(value, line, _type = undefined) {
    // Try to find the name of the special ability
    let { type, name, description } = this.checkAbilityData(value, line);

    // Create a placeholder for the special ability using the data found
    const specialAbility = {
      name: name || "Special Ability",
      specialAbilityType: type.toLowerCase(),
      type: "misc",
      desc: `<p>${description.replaceAll("\n\n", "</p><p>")}</p>`,
    };

    const placeholder = await sbcUtils.generatePlaceholderEntity(specialAbility, line);

    // Check for an existing aura (the only time we need to worry about special abilities pre-existing AFAIK)
    const existingAura = window.SBC.actor.itemTypes.feat.find(
      (i) => i.name === `Aura: ${placeholder.name}` && i.system.subType === "racial"
    );
    if (existingAura) {
      await existingAura.update({
        "system.description.value": specialAbility.desc,
      });
    } else await createItem(placeholder);

    this.app.processData.notes.specialAbilities.push({
      name: name,
      type: type,
      typeName: pf1.config.abilityTypes[type].short,
      description: sbcUtils.convertStringUnits(description),
    });

    return true;
  }

  /**
   * Check the data for the special ability
   *
   * @param {string} value  The value to parse
   * @param {number} _line   The line number
   * @returns {{[key: string]: string}}    The name, description, and type of the special ability
   */
  checkAbilityData(value, _line) {
    // (1) Hopefully, the statblock includes the abilityType, e.g. Su, Ex, or Sp
    //     Because we can separate the name from that
    // (2) If no abilityType is found, things start to get fuzzy,
    //     Sometimes the name will be anything up to the first word which first letter is lowercase
    //     Other times the name will include an "of" or "the" with a lower letter, which will break the name finding
    //     So, try to find the first word starting with a lowercase letter, check if its one of the keywords [of, the, etc.]
    //     And put that into the name

    if (value.search(specialAbilityTypesRegex()) !== -1) {
      // (1) Hopefully, the statblock includes the abilityType, e.g. Su, Ex, or Sp
      //     Because we can separate the name from that

      let { description, name, type } = specialAbilityRegex().exec(value).groups;
      type = type.toLowerCase().replace(/[()]*/g, "");
      if (type === "--") {
        type = "na";
      }
      return { description: description.capitalize(), name: sbcUtils.capitalize(name), type };
    }

    // (2) If no abilityType is found, things aren't solvable.
    //     Sometimes the name will be anything up to the first word which first letter is lowercase
    //     Other times the name will include an "of" or "the" with a lower letter, which will break the name finding
    //     Sometimes, the name isn't present because this line is actually part of the previous ability.
    //     Removing this code until WIP: split abilities by titles instead of by line

    let name = sbcUtils.translate("parser.specialAbility.placeholderName", { value: ++this.specialAbilityCount });
    let description = value.trim();
    let type = "na";

    this.throwError(undefined, "parser.specialAbility.matchError");
    return { name, description, type };
  }
}
