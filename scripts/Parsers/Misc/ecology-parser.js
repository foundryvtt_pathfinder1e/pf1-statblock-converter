import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// Parse Ecology
export default class EcologyParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Misc";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "ecology";
  }

  /**
   * Parses an ecology string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    sbcUtils.log(sbcUtils.translate("parser.ecology.status", value));

    const ecologyDesc = `<div><strong>${value.name}</strong>: ${value.entry}</div>`;

    // Check, if there already is an ecology item
    /** @type {import("../../../pf1/module/action-use/action-use.mjs").ItemPF} */
    const ecologyItem = window.SBC.actor.items.contents.find((item) => item.name === "Ecology");

    // Update the existing item or create a new one
    if (ecologyItem) {
      const tempDesc = ecologyItem.system.description.value;
      await ecologyItem.update({
        "system.description.value": tempDesc + ecologyDesc,
      });
    } else {
      const ecologyEntry = {
        name: "Ecology",
        type: "misc",
        desc: ecologyDesc,
        img: "icons/environment/wilderness/tree-oak.webp",
      };

      const placeholder = await sbcUtils.generatePlaceholderEntity(ecologyEntry, line);

      await createItem(placeholder);
    }

    return true;
  }
}
