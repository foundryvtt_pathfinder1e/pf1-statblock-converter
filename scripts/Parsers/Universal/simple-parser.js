import { sbcUtils } from "../../sbcUtils.js";
import { parseValueToDocPath } from "../../sbcParser.js";
import { AbstractParser } from "../abstract-parser.js";

/**
 * SimpleParser is a class that writes a given value into all
 * fields defined in parserMapping
 *
 * @augments AbstractParser
 */
export default class SimpleParser extends AbstractParser {
  /**
   * Writes a given value into all fields defined in parserMapping
   *
   * @param {sbcTypes.InputDialog} app The InputDialog instance.
   * @param {Array.<string>} targetFields Foundry Actor's fields where this is pointing to.
   * @param {"number"|"string"} supportedType The type of data this value is.
   * @throws {Error} If the 'targetFields' or 'supportedType' arguments are missing.
   */
  constructor(app, targetFields, supportedType) {
    super(app);

    if (!targetFields) {
      throw new Error(sbcUtils.translate("parser.simple.missingArgument", { arg: "targetFields" }));
    }
    this.targetFields = targetFields;

    if (!supportedType) {
      throw new Error(sbcUtils.translate("parser.simple.missingArgument", { arg: "supportedType" }));
    }
    if (!["number", "string"].includes(supportedType)) {
      throw new Error(sbcUtils.translate("parser.simple.invalidArgumentType"));
    }
    this.supportedType = supportedType;
  }

  /**
   *
   * @param {string|number} value The value to parse.
   * @param {number} line The line where the parsing is occurring.
   * @param {string} _type The type of the value.
   * @returns {Promise.<boolean>} `true` if correctly parsed.
   */
  async parse(value, line, _type = undefined) {
    if (value === undefined) {
      throw new Error(sbcUtils.translate("parser.simple.missingArgument", { arg: "value" }));
    }
    if (line === undefined) {
      throw new Error(sbcUtils.translate("parser.simple.missingArgument", { arg: "line" }));
    }
    if (value === "") {
      return false;
    }

    sbcUtils.log(
      sbcUtils.translate("parser.simple.status", { value: value, targetFields: this.targetFields.join(", ") })
    );
    // Check if the given value is one of the supported ones
    if (typeof value === this.supportedType || value === "NaN") {
      try {
        for (const field of this.targetFields) {
          await parseValueToDocPath(window.SBC.actor, field, value === "NaN" ? null : value);
        }
        return true;
      } catch (err) {
        this.throwError(err, "parser.simple.error", value, line, { targetFields: this.targetFields.join(", ") });
        return false;
      }
    } else {
      this.throwError(undefined, "parser.simple.inputNotOfSupportedType", value, line, {
        supportedType: this.supportedType,
      });
      return false;
    }
  }
}
