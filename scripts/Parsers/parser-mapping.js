import { EntityParser, NotesParser, SimpleParser } from "./Universal/index.js";
import {
  AuraParser,
  ClassParser,
  CreatureParser,
  RaceParser,
  SensesParser,
  MythicRankParser,
} from "./BaseData/index.js";
import {
  ACParser,
  DRParser,
  HPParser,
  ImmunityParser,
  ResistanceParser,
  SavesParser,
  SRParser,
  WeaknessParser,
} from "./Defense/index.js";
import { EcologyParser, SpecialAbilityParser, TacticsParser } from "./Misc/index.js";
import { AttackParser, SpeedParser, SpellBooksParser } from "./Offense/index.js";
import { AbilityParser, GearParser, LanguageParser, SkillParser } from "./Statistics/index.js";
import AgeParser from "./BaseData/age-parser.js";

export class parserMapping {
  /** @type {parserMap} */
  static map = {};

  /* ------------------------------------ */
  /* Initialize Parser Mapping            */

  /* ------------------------------------ */
  /**
   * Initialize the parser mapping
   *
   * @param {sbcTypes.InputDialog} app  The SBC application
   * @returns {void}
   */
  static initMapping(app) {
    parserMapping.map = {
      types: {
        npc: ["base", "defense", "offense", "tactics", "statistics", "ecology", "special abilities", "description"],
        character: [
          "base",
          "defense",
          "offense",
          "tactics",
          "statistics",
          "ecology",
          "special abilities",
          "description",
        ],
        vehicle: ["vehicleBase", "vehicleDefense", "vehicleOffense", "vehicleDescription"],
        trap: ["trap"],
        haunt: ["hauntBase"],
      },
      trap: {
        name: new SimpleParser(app, ["name", "prototypeToken.name"], "string"),
        cr: new SimpleParser(app, ["system.details.cr.base", "system.details.cr.total"], "number"),
        type: new SimpleParser(app, ["system.details.type"], "string"),
        perception: new SimpleParser(app, ["system.attributes.perception.total"], "number"),
        disableDevice: new SimpleParser(app, ["system.attributes.disableDevice.total"], "number"),
        trigger: new NotesParser(app, ["base.trigger"]),
        reset: new NotesParser(app, ["base.reset"]),
        effect: new NotesParser(app, ["base.effect"]),
        source: new NotesParser(app, ["base.source"]),
        description: new NotesParser(app, ["description.long"], "string"),
      },
      haunt: {
        name: new SimpleParser(app, ["name", "prototypeToken.name"], "string"),
        cr: new SimpleParser(app, ["system.details.cr.base", "system.details.cr.total"], "number"),
        alignment: new SimpleParser(app, ["system.details.alignment"], "string"),
        source: new NotesParser(app, ["base.source"]),
        description: new NotesParser(app, ["description.long"], "string"),
      },
      base: {
        name: new SimpleParser(app, ["name", "prototypeToken.name"], "string"),
        cr: new SimpleParser(app, ["system.details.cr.base", "system.details.cr.total"], "number"),
        mr: new MythicRankParser(app),
        level: new NotesParser(app, ["system.details.level.value"]), // gets calculated by foundry
        xp: new NotesParser(app, ["system.details.xp.value"]), // gets calculated by foundry
        gender: new SimpleParser(app, ["system.details.gender"], "string"),
        age: new AgeParser(app),
        race: new RaceParser(app),
        classes: new ClassParser(app),
        source: new NotesParser(app, ["base.source"]), // used in the notes section
        alignment: new SimpleParser(app, ["system.details.alignment"], "string"),
        size: new SimpleParser(app, ["system.traits.size"], "string"),
        space: new SimpleParser(app, ["prototypeToken.height", "prototypeToken.width"], "number"),
        scale: new SimpleParser(app, ["prototypeToken.texture.scaleX", "prototypeToken.texture.scaleY"], "number"),
        creatureType: new CreatureParser(app),
        init: new SimpleParser(app, ["system.attributes.init.total"], "number"),
        senses: new SensesParser(app),
        aura: new AuraParser(app),
      },
      defense: {
        acNormal: new SimpleParser(app, ["system.attributes.ac.normal.total"], "number"),
        acFlatFooted: new SimpleParser(app, ["system.attributes.ac.flatFooted.total"], "number"),
        acTouch: new SimpleParser(app, ["system.attributes.ac.touch.total"], "number"),
        acTypes: new ACParser(app),
        hp: new HPParser(app),
        regeneration: new SimpleParser(app, ["system.traits.regen"], "string"),
        fastHealing: new SimpleParser(app, ["system.traits.fastHealing"], "string"),
        saves: new SavesParser(app),
        immune: new ImmunityParser(app),
        resist: new ResistanceParser(app),
        weakness: new WeaknessParser(app),
        defensiveAbilities: new EntityParser(app),
        dr: new DRParser(app),
        sr: new SRParser(app),
      },
      offense: {
        speed: new SpeedParser(app),
        attacks: new AttackParser(app),
        specialAttacks: new EntityParser(app),
        space: new SimpleParser(app, ["prototypeToken.height", "prototypeToken.width"], "number"),
        stature: new SimpleParser(app, ["system.traits.stature"], "string"),
        spellBooks: new SpellBooksParser(app),
      },
      tactics: new TacticsParser(app),
      statistics: {
        str: new AbilityParser(app, ["system.abilities.str.value"], ["system.abilities.str.mod"], "number"),
        dex: new AbilityParser(app, ["system.abilities.dex.value"], ["system.abilities.dex.mod"], "number"),
        con: new AbilityParser(app, ["system.abilities.con.value"], ["system.abilities.con.mod"], "number"),
        int: new AbilityParser(app, ["system.abilities.int.value"], ["system.abilities.int.mod"], "number"),
        wis: new AbilityParser(app, ["system.abilities.wis.value"], ["system.abilities.wis.mod"], "number"),
        cha: new AbilityParser(app, ["system.abilities.cha.value"], ["system.abilities.cha.mod"], "number"),
        bab: new SimpleParser(app, ["system.attributes.bab.total"], "number"),
        cmb: new SimpleParser(app, ["system.attributes.cmb.total"], "number"),
        cmd: new SimpleParser(app, ["system.attributes.cmd.total"], "number"),
        feats: new EntityParser(app),
        skills: new SkillParser(app),
        languages: new LanguageParser(app),
        sq: new EntityParser(app),
        gear: new GearParser(app),
      },
      ecology: new EcologyParser(app),
      specialAbilities: new SpecialAbilityParser(app),
      description: new NotesParser(app, ["description.long"], "string"),
    };
  }

  /**
   * Get the necessary parsing categories for a given actor type
   *
   * @param {string} actorType    The type of actor
   * @returns {Array<string>}     The necessary parsing categories
   */
  static getNeededCategories(actorType) {
    switch (actorType) {
      case "character":
      case "npc":
        return ["defense", "offense", "statistics"];
      case "vehicle":
        return ["vehicleDefense", "vehicleOffense"];
      case "trap":
      case "haunt":
        return [];
    }
  }
}

/**
 * @typedef BaseMapping
 * @property {SimpleParser} name            The name of the entity
 * @property {SimpleParser} cr              The challenge rating of the entity
 * @property {MythicRankParser} mr          The mythic rank of the entity
 * @property {NotesParser} level            The level of the entity
 * @property {NotesParser} xp               The experience points of the entity
 * @property {SimpleParser} gender          The gender of the entity
 * @property {AgeParser} age                The age of the entity
 * @property {RaceParser} race              The race of the entity
 * @property {ClassParser} classes          The classes of the entity
 * @property {NotesParser} source           The source of the entity
 * @property {SimpleParser} alignment       The alignment of the entity
 * @property {SimpleParser} size            The size of the entity
 * @property {SimpleParser} space           The space of the entity
 * @property {SimpleParser} scale           The scale of the entity
 * @property {CreatureParser} creatureType  The creature type of the entity
 * @property {SimpleParser} init            The initiative of the entity
 * @property {SensesParser} senses          The senses of the entity
 * @property {AuraParser} aura              The aura of the entity
 */

/**
 * @typedef DefenseMapping
 * @property {SimpleParser} acNormal              The normal armor class of the entity
 * @property {SimpleParser} acFlatFooted          The flat-footed armor class of the entity
 * @property {SimpleParser} acTouch               The touch armor class of the entity
 * @property {ACParser} acTypes                   The armor class types of the entity
 * @property {HPParser} hp                        The hit points of the entity
 * @property {SimpleParser} regeneration          The regeneration of the entity
 * @property {SimpleParser} fastHealing           The fast healing of the entity
 * @property {SavesParser} saves                  The saves of the entity
 * @property {ImmunityParser} immune              The immunities of the entity
 * @property {ResistanceParser} resist            The resistances of the entity
 * @property {WeaknessParser} weakness            The weaknesses of the entity
 * @property {EntityParser} defensiveAbilities    The defensive abilities of the entity
 * @property {DRParser} dr                        The damage reduction of the entity
 * @property {SRParser} sr                        The spell resistance of the entity
 */

/**
 * @typedef OffenseMapping
 * @property {SpeedParser} speed            The speed of the entity
 * @property {AttackParser} attacks         The attacks of the entity
 * @property {EntityParser} specialAttacks  The special attacks of the entity
 * @property {SpellBooksParser} spellBooks  The spell books of the entity
 * @property {TacticsParser} tactics        The tactics of the entity
 */

/**
 * @typedef StatisticsMapping
 * @property {AbilityParser} str          The strength of the entity
 * @property {AbilityParser} dex          The dexterity of the entity
 * @property {AbilityParser} con          The constitution of the entity
 * @property {AbilityParser} int          The intelligence of the entity
 * @property {AbilityParser} wis          The wisdom of the entity
 * @property {AbilityParser} cha          The charisma of the entity
 * @property {SimpleParser} bab           The base attack bonus of the entity
 * @property {SimpleParser} cmb           The combat maneuver bonus of the entity
 * @property {SimpleParser} cmd           The combat maneuver defense of the entity
 * @property {EntityParser} feats         The feats of the entity
 * @property {SkillParser} skills         The skills of the entity
 * @property {LanguageParser} languages   The languages of the entity
 * @property {EntityParser} sq            The special qualities of the entity
 * @property {GearParser} gear            The gear of the entity
 */

/**
 * @typedef parserMap
 * @property {object} types                           Mapping for the different types of entities
 * @property {Array<string>} types.npc                Mapping for NPCs
 * @property {Array<string>} types.pc                 Mapping for PCs
 * @property {Array<string>} types.vehicle            Mapping for Vehicles
 * @property {Array<string>} types.trap               Mapping for Traps
 * @property {Array<string>} types.haunt              Mapping for Haunts
 * @property {BaseMapping} base                       Mapping for the base data
 * @property {DefenseMapping} defense                 Mapping for the defense data
 * @property {OffenseMapping} offense                 Mapping for the offense data
 * @property {StatisticsMapping} statistics           Mapping for the statistics data
 * @property {EcologyParser} ecology                  Mapping for the ecology data
 * @property {SpecialAbilityParser} specialAbilities  Mapping for the special abilities data
 * @property {NotesParser} description                Mapping for the description data
 */
