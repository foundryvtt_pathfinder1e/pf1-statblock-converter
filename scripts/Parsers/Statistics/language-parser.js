import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { languagesRegex } from "../../sbcRegex.js";

// Parse Languages
export default class LanguageParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Statistics";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "language";
  }

  /**
   * Parse a string as languages
   *
   * @param {string} value        The value to parse
   * @param {number} _line        The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, _line, _type = undefined) {
    const languages = value.split(/[,;]/g);
    const specialLanguages = [];
    const realLanguages = [];
    let languageContext = "";

    this.app.processData.notes.statistics.languages = [];

    for (let i = 0; i < languages.length; i++) {
      let language = languages[i].trim();
      let checkForLanguageContext = sbcUtils.parseSubtext(language);

      // Check for language context information and add this to the custom language field if available
      if (checkForLanguageContext.length > 1) {
        language = checkForLanguageContext[0];
        languageContext = checkForLanguageContext[1];

        specialLanguages.push(`${language} (${languageContext})`);
        continue;
      }

      if (language.search(languagesRegex()) !== -1) {
        let languageKey = sbcUtils.getKeyByValue(pf1.config.languages, language);
        realLanguages.push(languageKey);
      } else {
        specialLanguages.push(language);
      }
    }

    await window.SBC.actor.update({
      "system.traits.languages": realLanguages
        .concat(specialLanguages)
        .concat(window.SBC.actor.system.traits.languages.base),
    });

    this.app.processData.notes.statistics.languages.push(...realLanguages, ...specialLanguages);

    return true;
  }
}
