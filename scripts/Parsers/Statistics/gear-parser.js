import { createItem } from "../../sbcParser.js";
import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import {
  fluffGearTermRegex,
  itemModificationsRegex,
  magicalAbilitiesRegex,
  materialAddonsRegex,
  materialsNonBasicRegex,
  materialsRegex,
  quantityOfRegex,
  sizeRegex,
  supportedSpellsRegex,
  techColorRegex,
  techTierRegex,
} from "../../sbcRegex.js";

// Parse Gear
export default class GearParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Statistics";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "gear";
  }

  /**
   * Parse a string as gear
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    // Skip if the value is "none"
    if (value === "none") return true;

    // Split ranged weapons and ammunition
    value = value
      .replace(/\b(?:and|with)\s*(\d+)/gi, ", $1")
      .replace(/ioun stones \([^)]+\)/i, (match) =>
        match
          .substring(match.indexOf("(") + 1, match.length - 1)
          .split(",")
          .map((i) => {
            const parts = i.split("[");
            return parts[0].trim() + " Ioun Stone";
          })
          .join(", ")
      )
      .replace(/(scrolls \([^)]+\))/gi, (match) => {
        return match
          .substring(9, match.length - 1)
          .split(/,? and |, /g)
          .map((i) => "scroll of " + i)
          .join(", ");
      })
      .replace(/implanted ioun stones? \(((?:[^,)]+,?)+)\)/gi, (match) =>
        match
          .substring(0, match.length - 1)
          .split("(")[1]
          .split(",")
          .map((i) => {
            const amount = i.match(/(\d+)/)?.[1] || 1;
            const effectiveName = i.replace(/\d+/, "").trim().replace(/s$/, "");
            return `Ioun Stone ${effectiveName} (${amount}, Implanted)`;
          })
          .join(", ")
      );

    const money = { pp: 0, gp: 0, sp: 0, cp: 0 };
    let gears = this.splitText(value, false);
    let placeholdersGenerated = [];

    let gearItemTypes = ["consumable", "equipment", "loot", "weapon", "container", "implant"];

    await sbcUtils.runParallelOps(gears, async (input) => {
      let requiresNameChange = false;
      let regexMatch = null;

      input = input
        .trim()
        .replaceAll("*", "")
        .replaceAll("[", "(")
        .replaceAll("]", ")")
        .replace(/(\d+)\s*doses(?! of)/i, "$1")
        .replace(/^pair of /i, "2 ")
        .replace(/\(as\s+(?:an?\s+)?/i, "(")
        .replace(/^And\s/i, "");

      // If we're left with a number at the start, and it's *not* a currency string,
      // assume it's a quantity and move it to the end of the string
      if (/^\d+\s/.test(input) && !sbcUtils.getMoneyFromString(input)) {
        input = input.replace(/^(\d+)(.*)/, "$2 ($1)").trim();
      }

      // Extract item worth
      let goldValue = 0;
      let goldIsTotal = false;
      if (input.match(/\d+\s+[pscg]p\s+in\b/i)) {
        let worth;
        [worth, input] = input.split(/\bin\b/i);
        input = `${input} worth ${worth}`;
      }

      if (input.match(/\bworth\b/i)) {
        let worthText;
        [input, worthText] = input.split(/\bworth\b/i);

        const { worth, rest } =
          /(?<worth>(?:\s*\d+\s*[pgsc]p\s*){1,4})(?:each|for the pair|total|in all)?(?<rest>.*)/g.exec(
            worthText
          ).groups;

        goldIsTotal = worth.match(/for the pair|total|in all/i) !== null;

        input = (input + rest)
          .replace(/,\s*,\s*/g, ", ")
          .replace(/\(\s*,\s*/g, "(")
          .replace(/\s*,\s*\)/g, ")")
          .replace(/\s{2,}/, " ")
          .replace(/\(\s*\)/g, "")
          .trim()
          .trim();
        goldValue = sbcUtils.convertMoneyToGold(sbcUtils.getMoneyFromString(worth));
      }

      // Handle composite longbow max. strength
      let maxStrength = 0;
      if (/\+?\d+ Str\b/i.test(input)) {
        maxStrength = +input.match(/(\d+) Str\b/i)[1].trim();
        input = input.replace(/\s*\(\+?\d+ Str\)/i, "").trim();
      }

      let splitInput = sbcUtils.parseSubtext(input);
      let gearText = splitInput[0];
      let gearSubtext = splitInput[1];

      const searchTerms = new Set([input, gearText, gearSubtext].filter((i) => i));

      searchTerms.add(gearText.replace(/(^\d+|masterwork|mwk)/g, "").trim());

      if ((regexMatch = input?.match(/(?:\b|\()(?:focus\s+)?for\b/))) {
        // Probably a spell component, but indicates a use for something, so that use case should be preserved
        requiresNameChange = true;
        const withoutPurposeName = input.split(regexMatch[0])[0].trim();
        searchTerms.add(withoutPurposeName);
      }

      let quantity = 1;
      let material = "";
      let baseMaterial = "";
      let addons = [];
      let enhancementValue = 0;
      let magicAbilities = {};
      let size = "med";
      let fluffTerm = "";
      let charges = null;

      [...searchTerms].forEach((searchTerm) => {
        // Singularize terms like "Elixirs of XYZ"
        searchTerms.add(searchTerm.replace(/s of\b/i, " of"));
      });

      const sillyGearMap = window.SBC.config.sillyGear;
      const sillyGearKeys = Object.keys(sillyGearMap);
      [...searchTerms].forEach((searchTerm) => {
        const sillyGearMatch = sbcUtils.haystackRegexSearch(sillyGearKeys, searchTerm);
        if (sillyGearMatch) {
          const sillyGearRegex = new RegExp(sillyGearMatch, "i");
          searchTerms.add(searchTerm.replace(sillyGearRegex, sillyGearMap[sillyGearMatch]));
          requiresNameChange = true;
        }
      });

      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = searchTerm.match(/(\d+) charges?/i))) {
          charges = +regexMatch[1];
          searchTerms.add(
            searchTerm
              .replace(regexMatch[0], "")
              .replace(/\(\s*,\s*/, "(")
              .replace(/\(\s*\)/, "")
              .trim()
          );
        }
      });

      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = searchTerm.match(sizeRegex())) !== null) {
          size = sbcUtils.getKeyByValue(pf1.config.actorSizes, regexMatch[0]) || "med";
          searchTerms.add(searchTerm.replace(regexMatch[0], "").trim());
        }
      });

      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = searchTerm.match(fluffGearTermRegex())) !== null) {
          searchTerms.add(searchTerm.replace(regexMatch[0], "").trim());
          fluffTerm = regexMatch[0];
        }
      });

      // Preserve text inside ( ) for a backup search
      if (gearSubtext?.length) {
        const quantityMatch = input.match(/\((\d+)\s*(?:\)\s*$|,\s*)/);
        if (quantityMatch) {
          // && !supportedSpellsRegex().test(gearText)) {
          quantity = +quantityMatch[1];

          searchTerms.add(
            gearSubtext
              .replace(new RegExp(`^${quantityMatch[1]}`), "")
              .replace(/^\s*,\s*/, "")
              .trim()
          );

          searchTerms.add(
            input
              .replace(new RegExp(`\\(\\s*${quantityMatch[1]}`), "(")
              .replace(/\(\s*,\s*/, "(")
              .replace("()", "")
              .trim()
          );
        }
      }

      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.toLowerCase().includes("rod") && searchTerm.toLowerCase().includes("metamagic")) {
          const kind = searchTerm.match(/(lesser|normal|greater)/i)?.[0] || "";
          searchTerm = searchTerm
            .replace(/rod(?: of)?/i, "")
            .replace(/metamagic/i, "")
            .replace(kind, "")
            .trim();

          if (searchTerm.toLowerCase().includes("elemental")) {
            searchTerm = "Elemental";
            requiresNameChange = true;
          }

          searchTerm = `${searchTerm} Metamagic Rod, ${sbcUtils.capitalize(kind)}`;
          searchTerms.add(searchTerm);
        }
      });

      [...searchTerms].forEach((searchTerm) => {
        let holySymbolName;
        holySymbolName = searchTerm.toLowerCase();

        if (holySymbolName.includes("holy symbol")) {
          let material = holySymbolName.match(/\b(?:wood|silver|gold|iron|platinum|flask|tattoo)/gi)?.[0] || "wood";
          if (holySymbolName.startsWith(material.toLowerCase())) {
            holySymbolName = holySymbolName.split(" ").slice(1).join(" ");
          }

          if (holySymbolName.includes("unholy")) {
            requiresNameChange = true;
          }

          // Unholy symbols and symbols of a random material require a name change
          // Also switch to iron, because it has a nicer image and is probably closer
          // to the target material than wood
          if (!holySymbolName.split(" ")[0].includes("holy")) {
            requiresNameChange = true;
            material = "iron";
          }

          // Holy symbol is of a specific listed god, so preserve that name on the final item
          if (holySymbolName.includes(" of ")) {
            requiresNameChange = true;
          }

          // Grab a matching holy symbol from the compendium, or wood as fallback
          // If no material is specified, we're assuming it is wooden. If it is
          // of a specific random material, value has already been parsed elsewhere
          searchTerms.add(`Holy Symbol (${material.capitalize()})`);
        }
      });

      // Handle tech items being color-coded
      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = searchTerm.match(techColorRegex())) !== null) {
          searchTerms.add(searchTerm.replace(techColorRegex(), "").trim());
          searchTerms.add(searchTerm.replace(techColorRegex(), "").trim() + ` (${regexMatch[0]})`);
        }
      });

      // Handle the different tiers of tech items
      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = searchTerm.match(techTierRegex())) !== null) {
          searchTerms.add(searchTerm.replace(regexMatch[0], "").trim() + ` (${regexMatch[0]})`);
          searchTerms.add(searchTerm.replace(regexMatch[0], "").trim() + ` ${regexMatch[0]}`);
        }
      });

      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = quantityOfRegex().exec(searchTerm)) !== null) {
          searchTerms.add(`${regexMatch[2]} (${regexMatch[1]})`);
          searchTerms.add(regexMatch[2]);
        }
      });

      // Handle materials
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(materialsRegex()) !== -1) {
          let nonBasicMaterials = searchTerm.match(materialsNonBasicRegex());
          material = nonBasicMaterials ? nonBasicMaterials[0] : material;
          material =
            pf1.registry.materials.contents.find(
              (mt) => mt.name === sbcUtils.capitalize(material ?? "") || mt.id === material
            )?.id ?? null;
          searchTerms.add(
            searchTerm
              .replace(materialsNonBasicRegex(), "")
              .replace(/\s{2,}/, " ")
              .trim()
          );
        }
      });

      // Amulet of might fists edge case
      if (input.match(/amulet of mighty fist/i)) {
        enhancementValue = +input.match(/\+[12345]/)?.[1] || 0;
        const regex = magicalAbilitiesRegex();
        while ((regexMatch = regex.exec(input)) !== null) {
          const match = regexMatch.groups;
          const magicAbility = sbcUtils.getMagicAbility(match.ability, match.prefix || match.postfix || undefined);
          magicAbilities[magicAbility.name] = magicAbility;
        }

        requiresNameChange = true;
        searchTerms.add("Amulet of Mighty Fists" + (enhancementValue ? ` +${enhancementValue}` : "+1"));

        goldValue =
          this.calculateItemPrice(
            { name: "", type: "weapon", system: { price: 0 } },
            {
              material: null,
              addons: [],
              enhancementValue: enhancementValue,
              enhancementTypes: Object.values(magicAbilities),
            }
          ) * 2;
      } else {
        // Handle enhancement bonuses
        [...searchTerms].forEach((searchTerm) => {
          if (searchTerm.match(/\+[12345]/) && !searchTerm.match(/\d$/)) {
            enhancementValue = +searchTerm.match(/[12345]/)[0].trim();
            searchTerm = searchTerm.replace(/\+[12345]/, "").trim();
            searchTerms.add(searchTerm);
          }

          // Handle the magical abilities. Only items with an enhancement bonus of at least +1 can have abilities
          if (enhancementValue) {
            const regex = magicalAbilitiesRegex();
            const regexSearchTerm = searchTerm;
            while ((regexMatch = regex.exec(regexSearchTerm)) !== null) {
              const match = regexMatch.groups;
              const magicAbility = sbcUtils.getMagicAbility(match.ability, match.prefix || match.postfix || undefined);

              if (magicAbility.hasSpecialTerm) {
                const specialTermMatch = searchTerm.match(
                  new RegExp("\\b(?:([a-z-]+)[- ])?" + magicAbility.name + "(?: \\(([^)]+)\\))?", "i")
                );
                const specialTerm = specialTermMatch[1] || specialTermMatch[2];
                if (specialTerm) {
                  magicAbility.specialTerm = specialTerm;
                }
                searchTerm = searchTerm.replace(specialTermMatch[0], "");
              } else {
                searchTerm = searchTerm.replace(regexMatch[0], "");
              }

              searchTerms.add(searchTerm.replace(/\s{2,}/g, " ").trim());
              magicAbilities[magicAbility.name] = magicAbility;
            }
          }
        });
      }

      // Handle Item Modifications
      [...searchTerms].forEach((searchTerm) => {
        if ((regexMatch = itemModificationsRegex().exec(searchTerm)) !== null) {
          searchTerms.add(searchTerm);
          searchTerms.add(searchTerm.replace(regexMatch[0], "").trim());
          requiresNameChange = true;
        }
      });

      // Handle misc modifier terms
      [...searchTerms].forEach((searchTerm) => {
        searchTerms.add(searchTerm.replace(/\W?(Integrated|Implanted|Timeworn)\W?/gi, ""));
      });

      // Special case for grenades
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(/grenades?$/gi) !== -1) {
          searchTerms.add(`Grenade (${searchTerm.replace(/s$/, "")})`);
        }
      });

      // Handle ironwood separately. We can assume the item is made or iron normally, so this is good enough to keep as normal item
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.includes("ironwood")) {
          searchTerms.add(searchTerm.replace(/\bironwood\b\s+/i, ""));
          baseMaterial = "iron";
          requiresNameChange = true;
        }
      });

      // Black Powder Horn special case
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(/black powder horn/i) !== -1) {
          searchTerms.add(searchTerm.replace(/black powder horn/i, "Powder Horn"));
        }
      });

      // Handle flail special case
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.includes("flail") && (!searchTerm.includes("light") || !searchTerm.includes("heavy"))) {
          searchTerms.add(searchTerm.replace(/flail/i, "Light Flail"));
        }
      });

      // Special case for Studded Leather armor
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(/studded leather(?!\s*armor)/i) !== -1) {
          searchTerms.add(searchTerm.replace(/studded leather/i, "Studded Leather Armor"));
        }
      });

      // Special case for Pearl of Power
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.toLowerCase().includes("pearl of power")) {
          const pearlLevel = +searchTerm.match(/(\d+)/)?.[1];
          if (pearlLevel) {
            const spellLevelEnding = ["st", "nd", "rd"][pearlLevel - 1] ?? "th";
            searchTerms.add(`Pearl of Power, ${pearlLevel}${spellLevelEnding} Level Spell`);
          }
        }
      });

      // Special case for Artisan's tools
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.match(/tool/i) && searchTerm.match(/artisan|thieves/i)) {
          const toolName = searchTerm.includes("artisan") ? "Artisan's" : "Thieves'";
          searchTerms.add(
            toolName + " Tools, " + (searchTerm.match(/\b(?:mwk|masterwork)\b/i) ? "Masterwork" : "Common")
          );
        }
      });

      // Special case for firearm bullets
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.toLowerCase().includes("bullet")) {
          searchTerms.add(searchTerm.replace(/bullet/i, "Firearm Bullet"));
        }
      });

      // Special case for bolts
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.toLowerCase().includes("bolt")) {
          searchTerms.add(searchTerm.replace(/bolt/i, "Crossbow Bolt"));
        }
      });

      // Special case for alchemical cartridges
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.toLowerCase().includes("alchemical") && searchTerm.toLowerCase().includes("cartridge")) {
          searchTerm = searchTerm
            .replace(/alchemical(.*)cartridges?/gi, "$1 Alchemical Cartridge")
            .replace(/\s{2,}/, " ")
            .trim();

          const cartridgeType = searchTerm.toLowerCase().split("alchemical cartridge")[0].trim();

          searchTerms.add(
            "Alchemical Cartridge (" +
              cartridgeType +
              (gearSubtext.includes("bullet") || cartridgeType === "paper" ? ", bullet or pellet" : "") +
              ")"
          );
        }
      });

      // Handle physical might and mental prowess stat items
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(/\b(?:physical might|mental prowess)\b/i) !== -1) {
          searchTerm = searchTerm
            .replace(/Intelligence/i, "Int")
            .replace(/Wisdom/i, "Wis")
            .replace(/Charisma/i, "Cha")
            .replace(/Strength/i, "Str")
            .replace(/Dexterity/i, "Dex")
            .replace(/Constitution/i, "Con");

          searchTerms.add(searchTerm.replace(/(Str|Dex|Con|Int|Wis|Cha),\s*(Dex|Con|Wis|Cha)/i, "$1 & $2"));
          // TODO: Remove this once system removes generic items, as we will find the specific item then
          requiresNameChange = true;
        }
      });

      // Handle addons
      [...searchTerms].forEach((searchTerm) => {
        if (searchTerm.search(materialAddonsRegex()) !== -1) {
          let addonMatches = searchTerm.match(materialAddonsRegex());
          addons = addonMatches
            .map((element) => {
              const probableId = element[0] + element.slice(1).replace(/\s/, "");
              return (
                pf1.registry.materials.find(
                  (mt) => mt.id === probableId || mt.name.toLowerCase() === element.toLowerCase()
                )?.id ?? null
              );
            })
            .filter((x) => !!x);
          searchTerms.add(searchTerm.replace(materialAddonsRegex(), ""));
        }
      });

      let gear = {
        type: "loot",
        name: gearText.replace(/^([\d+]+|\b(?:masterwork|mwk|broken|timeworn)\b)/gi, "").trim(),
        rawName: quantity > 1 ? this.stripQuantity(sbcUtils.singularize(input)) : input,
        subtext: gearSubtext,
        price: goldIsTotal ? goldValue / quantity : goldValue,
        value: 0,
        size: size,
        fluffTerm: fluffTerm,
        enhancementValue: enhancementValue,
        enhancementTypes: Object.values(magicAbilities),
        maxStrength: maxStrength,
        charges: charges,
        mwk: /\bmasterwork\b|\bmwk\b/i.test(gearText) || enhancementValue > 0,
        quantity: quantity,
        timeworn: /\btimeworn\b/i.test(gearText),
        broken: /\bbroken\b/i.test(gearText),
        material: material,
        baseMaterial: baseMaterial,
        addons: addons,
        implanted: null,
      };

      [...searchTerms].forEach((searchTerm) => {
        searchTerms.add(searchTerm?.replace(/ies\s*$/gi, "y"));
        searchTerms.add(searchTerm.replace(/ives/gi, "ife"));
        searchTerms.add(searchTerm?.replace(/(?!Charges)s\s*$/gi, ""));
      });

      console.log(input, searchTerms);

      let gearKeys = Object.keys(gear);

      /** @type {Item} */
      let entity = {};

      const gold = sbcUtils.getMoneyFromString(gearText);
      if (gold) {
        // Store all money for later, so we can bundle it into a pouch
        money.pp += gold.pp;
        money.gp += gold.gp;
        money.sp += gold.sp;
        money.cp += gold.cp;
        return;
      } else if (supportedSpellsRegex().test(gearText)) {
        supportedSpellsRegex().lastIndex = 0;
        gear.type = "consumable";

        let namePattern = gearText.match(supportedSpellsRegex());
        let consumableType = this.getConsumableType(namePattern[1]?.toLowerCase());
        // Remove spell bonus denominator from search name
        let spellName = namePattern[2].replace(/\+\d+/, "").trim();
        let spellBonus = namePattern[2].match(/\+(\d+)/)?.[1] ?? 0;
        // let charges = gearSubtext?.match(/\d+/)?.[0] ?? (/wand/i.test(consumableType) ? 50 : 1);
        let consumableQuantity = parseInt(
          gearSubtext?.match(/(\d+) charges/i)?.[1] ?? (/wand/i.test(consumableType) ? 50 : 1)
        );
        let casterLevel = gearSubtext?.match(/CL\s*(\d+)/i)?.[1] ?? -1;

        let consumable = null;
        entity = await compendiumOps.findEntityInCompendia(
          { search: new Set([`${consumableType} of ${spellName}`]), item: `${consumableType} of ${spellName}` },
          {
            itemType: "consumable",
          }
        );

        if (!entity) {
          entity = await compendiumOps.findEntityInCompendia(
            { search: new Set([spellName, sbcUtils.singularize(spellName)]), item: spellName },
            {
              itemType: "spell",
            }
          );
          if (entity) {
            const data = entity.toObject();

            if (spellBonus) {
              // Search for matching buff and determine caster level based on that
              const buff = await compendiumOps.findEntityInCompendia(
                { search: data.name, item: data.name },
                {
                  itemType: "buff",
                }
              );

              if (buff) {
                const changeFormula = buff.system.changes[0].formula;
                for (let i = 1; i <= 20; i++) {
                  const roll = new pf1.dice.RollPF(changeFormula, { item: { level: i } });
                  const result = (await roll.evaluate()).total;
                  if (result === +spellBonus) {
                    data.cl = i;
                    break;
                  }
                }
              }
            }

            consumable = await CONFIG.Item.documentClasses.spell.toConsumable(
              data,
              consumableType === "oil" ? "potion" : consumableType
            );
          }
        } else {
          consumable = entity;
        }

        if (consumable) {
          if (consumableType === "wand") gear.charges = consumableQuantity;

          if (casterLevel > 0) consumable.system.cl = parseInt(casterLevel);

          if (spellBonus) {
            consumable.name += ` +${spellBonus}`;
            gear.enhancementValue = 0;
          }

          if (consumableType === "oil") {
            consumable.name = consumable.name.replace("Potion", "Oil");
            consumable.system.unidentified.name = consumable.system.unidentified.name.replace("Potion", "Oil");
          }

          entity = new Item.implementation(consumable);
        }
      } else {
        entity = await compendiumOps.findEntityInCompendia(
          { search: searchTerms, item: input },
          { itemTypes: gearItemTypes }
        );
        if (entity?.type === "implant") {
          gear.implanted = true;
        }
      }

      if (gearSubtext && entity?.name?.toLowerCase() === gearSubtext.toLowerCase()) {
        requiresNameChange = true;
      }

      if (entity && Object.keys(entity).length !== 0) {
        const itemName = requiresNameChange ? sbcUtils.capitalize(gear.rawName) : this.buildItemName(entity, gear);

        const newGearPrice = this.calculateItemPrice(entity, gear);
        if (entity.system.price !== newGearPrice) {
          gear.price = newGearPrice;
        }

        let updates = {
          name: itemName,
          "system.identifiedName": itemName,
        };
        for (let i = 0; i < gearKeys.length; i++) {
          const key = gearKeys[i];
          const change = gear[key];

          if (!change || (Array.isArray(change) && !change.length)) {
            continue;
          }

          switch (key) {
            case "implanted":
              if (value !== null) {
                updates["system.implanted"] = change;
              }
              break;

            case "charges":
              if (entity.system?.uses?.per === "charges") {
                updates["system.uses.value"] = change;
              }
              break;

            case "material": {
              const materialInfo = pf1.registry.materials.get(change) || null;
              if (!materialInfo) {
                break;
              }
              const baseMaterials = Array.isArray(materialInfo.baseMaterial)
                ? materialInfo.baseMaterial
                : [materialInfo.baseMaterial];
              let baseMaterial =
                gear.baseMaterial ||
                entity.system?.material?.base?.value ||
                entity.system?.armor?.material?.base?.value;
              baseMaterial = baseMaterials.includes(baseMaterial) ? baseMaterial : baseMaterials[0];

              switch (entity.type) {
                case "weapon":
                  updates["system.material.normal.value"] = change;
                  updates["system.material.base.value"] = baseMaterial;
                  break;

                case "equipment":
                  if (["armor", "shield"].includes(entity.system.subType)) {
                    updates["system.armor.material.normal.value"] = change;
                    updates["system.armor.material.base.value"] = baseMaterial;
                  }
                  break;
              }
              Object.assign(updates, this.applyMaterialInfo(entity, gear, gear.material));
              break;
            }

            case "size":
              updates["system.size"] = change;
              break;

            case "maxStrength":
              if (entity.type === "weapon") {
                const actions = entity.toObject().system.actions ?? [];
                actions.forEach((action) => setProperty(action, "ability.max", change));
                updates["system.actions"] = actions;
              }
              break;

            case "addons": {
              switch (entity.type) {
                case "weapon":
                  updates["system.material.addon"] = change;
                  break;

                case "equipment":
                  if (["armor", "shield"].includes(entity.system.subType)) {
                    updates["system.armor.material.addon"] = change;
                  }
                  break;
              }
              change.forEach((addon) => Object.assign(updates, this.applyMaterialInfo(entity, gear, addon)));
              break;
            }

            case "enhancementTypes": {
              let description = entity.system.description.value;
              let casterLevel = +(gear.enhancementValue || 0) * 3;
              let auras = [];

              change.forEach((enhancementType) => {
                casterLevel = Math.max(casterLevel, enhancementType.cl);
                auras.push(enhancementType.aura);

                description +=
                  "\n<h3>" +
                  sbcUtils.translate(`specialAbilities.${enhancementType.name}.name`) +
                  "</h3>\n" +
                  sbcUtils.translate(`specialAbilities.${enhancementType.name}.description`);
              });

              updates["system.description.value"] = description;
              updates["system.cl"] = casterLevel;

              // Filter out duplicate auras
              auras = auras.filter((aura, index) => auras.indexOf(aura) === index);
              if (auras.length === 1) {
                updates["system.aura.school"] = auras[0];
              } else {
                updates["system.aura.custom"] = true;
                updates["system.aura.school"] = auras.map((aura) => sbcUtils.translate(`auras.${aura}`)).join(", ");
              }

              break;
            }

            case "enhancementValue":
              switch (entity.type) {
                case "weapon":
                  updates["system.enh"] = +change;
                  updates["system.masterwork"] = true;
                  updates["system.aura.school"] = "evo";
                  break;

                case "equipment":
                  updates["system.armor.enh"] = +change;
                  updates["system.masterwork"] = true;
                  updates["system.aura.school"] = "abj";
                  break;
              }
              updates["system.hardness"] = entity.system.hardness + change * 2;
              updates["system.hp.max"] = entity.system.hp.max + change * 10;
              updates["system.hp.value"] = entity.system.hp.value + change * 10;
              updates["system.cl"] = +change * 3;
              break;

            case "price":
              updates["system.price"] = +change;
              break;

            case "mwk":
              updates["system.masterwork"] = change;
              break;

            case "value":
              updates["system.price"] = +change;
              break;

            case "quantity":
              updates["system.quantity"] = +change;
              break;

            case "timeworn":
              updates["system.timeworn"] = change;
              break;

            case "broken":
              updates["system.broken"] = change;
              break;

            default:
              break;
          }
        }

        if (Object.keys(updates).length) {
          console.log(sbcUtils.translate("parser.gear.parseInfo"), updates);
          await entity.updateSource(updates);
        }

        await createItem(entity);
      } else {
        gear.name = input;
        const placeholder = await sbcUtils.generatePlaceholderEntity(gear, line);

        await createItem(placeholder);
        placeholdersGenerated.push(gear.name);
      }
    });

    // If we found money during the item parse, create a pouch for it
    if (money.pp || money.gp || money.sp || money.cp) {
      const moneyString = (
        (money.pp ? sbcUtils.translate("money.pp", { value: money.pp }) : "") +
        " " +
        (money.gp ? sbcUtils.translate("money.gp", { value: money.gp }) : "") +
        " " +
        (money.sp ? sbcUtils.translate("money.sp", { value: money.sp }) : "") +
        " " +
        (money.cp ? sbcUtils.translate("money.cp", { value: money.cp }) : "")
      )
        .replace(/\s{2,}/, " ")
        .trim();

      let pouch = {
        type: "container",
        name: sbcUtils.translate("moneyPouch", { money: moneyString }),
        currency: money,
      };
      const placeholder = await sbcUtils.generatePlaceholderEntity(pouch, line);
      await createItem(placeholder);
    }

    if (placeholdersGenerated.length > 0) {
      this.logInfo("parser.gear.generatedPlaceholders", placeholdersGenerated[0], line, {
        items: placeholdersGenerated.join(", "),
      });
    }

    this.app.processData.notes.statistics.gear = this.getItemNotes();

    // classItems were created successfully
    return true;
  }

  /**
   * Apply a specific materials properties to the item
   *
   * @param {Item} item The base item
   * @param {object} gear The gear object
   * @param {string} material The material to apply
   *
   * @returns {{[key: string]: any}} The properties to apply
   */
  applyMaterialInfo(item, gear, material) {
    const updates = {};
    const materialInfo = pf1.registry.materials.get(material) || null;

    if (!materialInfo) return updates;

    if (materialInfo.hardness) {
      updates["system.hardness"] = materialInfo.hardness + gear.enhancementValue * 2;
    }

    if (materialInfo.healthMultiplier) {
      updates["system.hp.max"] = Math.max(
        1,
        Math.floor(item.system.hp.max * materialInfo.healthMultiplier) + gear.enhancementValue * 10
      );
      updates["system.hp.value"] = Math.max(
        1,
        Math.floor(item.system.hp.value * materialInfo.healthMultiplier) + gear.enhancementValue * 10
      );
    }

    if (materialInfo.masterwork) {
      updates["system.masterwork"] = true;
    }

    if (materialInfo.weight.multiplier !== 1) {
      updates["system.weight.value"] = item.system.weight * materialInfo.weight.multiplier;
    }

    if (materialInfo.weight.bonusPerPound) {
      updates["system.weight.value"] = item.system.weight * (1 + materialInfo.weight.bonusPerPound);
    }

    if (item.type === "equipment") {
      const armorChanges = materialInfo[item.system.subType] || { acp: 0, maxDex: 0, asf: 0 };
      if (armorChanges.acp) {
        updates["system.armor.acp"] = Math.max(0, item.system.armor.acp - armorChanges.acp);
      }

      if (armorChanges.maxDex && item.system.armor.dex) {
        updates["system.armor.dex"] = item.system.armor.dex + armorChanges.maxDex;
      }

      if (armorChanges.asf) {
        updates["system.spellFailure"] = Math.max(0, item.system.spellFailure + armorChanges.asf);
      }
    }

    return updates;
  }

  /**
   * Dynamically builds an items name based on its properties
   *
   * @param {Item} item The item to calculate the price for
   * @param {object} gear The gear object
   * @returns {string} The final item name
   */
  buildItemName(item, gear) {
    let nameSegments = [item.name];

    if (gear.maxStrength) {
      nameSegments.unshift(`(+${gear.maxStrength} ${sbcUtils.translate("str")})`);
    }

    gear.addons.forEach((addon) => {
      const materialInfo = pf1.registry.materials.get(addon);
      if (materialInfo) {
        nameSegments.push(materialInfo.name);
      }
    });

    if (gear.material && !gear.name.match(/\bwand\b/i)) {
      const materialInfo = pf1.registry.materials.get(gear.material);
      // Attach material name only if not part of item name
      if (materialInfo && !item.name.includes(materialInfo.name)) {
        nameSegments.push(materialInfo.name);
      } else {
        // We wrongly detected material, so drop it
        gear.material = null;
      }
    }

    gear.enhancementTypes.forEach((enhancementType) => {
      nameSegments.push(
        (enhancementType.hasSpecialTerm ? sbcUtils.capitalize(enhancementType.specialTerm) + "-" : "") +
          sbcUtils.translate(`specialAbilities.${enhancementType.name}.name`)
      );
    });

    if (gear.timeworn) {
      nameSegments.push(sbcUtils.translate("timeworn"));
    }

    if (gear.broken) {
      nameSegments.push(sbcUtils.translate("broken"));
    }

    if (gear.enhancementValue && !item.name.match(new RegExp(`\\+${gear.enhancementValue}`))) {
      nameSegments.push("+" + gear.enhancementValue);
    } else if (gear.mwk) {
      nameSegments.push(sbcUtils.translate("masterwork"));
    }

    if (gear.fluffTerm) {
      nameSegments.push(sbcUtils.capitalize(gear.fluffTerm));
    }

    if (gear.size && gear.size !== "med") {
      nameSegments.push(pf1.config.actorSizes[gear.size]);
    }

    return nameSegments.reverse().join(" ");
  }

  /**
   * Calculates the price of a given item
   *
   * @param {Item} item The item to calculate the price for
   * @param {object} gear The gear object
   * @returns {number} The price of the item
   */
  calculateItemPrice(item, gear) {
    // Nothing to do here, the item price has been explicitly overwritten
    if (gear.price) return gear.price;

    let price = item.system.price || 0;
    price += this.calculateMaterialPrice(item, gear, gear.material);
    gear.addons.forEach((addon) => (price += this.calculateMaterialPrice(item, gear, addon)));

    if (gear.mwk) {
      if (gear.name.toLowerCase() === "shuriken" || gear.name.toLowerCase() === "crystal chakram") {
        price += 6;
      } else {
        price += item.type === "weapon" ? 300 : 150;
      }
    }

    // TODO: Handle double weapons

    let effectiveEnhancementBonus = gear.enhancementValue;

    gear.enhancementTypes.forEach((enhancementType) => {
      if (enhancementType.bonus) {
        effectiveEnhancementBonus += enhancementType.bonus;
      }

      if (enhancementType.staticCost) {
        price += enhancementType.staticCost;
      }
    });

    if (effectiveEnhancementBonus) {
      let enhancementMultiplier = item.type === "weapon" ? 2000 : item.type === "equipment" ? 1000 : 40;
      if (item.name.toLowerCase() === "shuriken" || item.name.toLowerCase() === "crystal chakram") {
        enhancementMultiplier = 40;
      }
      price += Math.pow(effectiveEnhancementBonus, 2) * enhancementMultiplier;
    }

    // Handle composite bow price adjust
    if (gear.maxStrength) {
      const isLongbow = gear.rawName.match(/longbow/i);

      price += gear.maxStrength * (isLongbow ? 100 : 75);
    }

    return price;
  }

  /**
   * Calculates the price of a given item
   *
   * @param {Item} item The item to calculate the price for
   * @param {object} gear The gear object
   * @param {string} material The material to calculate the price for
   * @returns {number} The price of the item
   */
  calculateMaterialPrice(item, gear, material) {
    let price = parseFloat(item.system.price) || 0;
    let basePrice = price;

    const materialInfo = pf1.registry.materials.get(material);
    if (!materialInfo) return 0;

    switch (item.type) {
      case "equipment": {
        if (item.system.subType !== "armor" && item.system.subType !== "shield") {
          break;
        }

        if (materialInfo) {
          price *= materialInfo.price.multiplier || 1;

          switch (item.system.subType) {
            case "shield":
              price += materialInfo.price.shield || 0;
              break;

            case "armor":
              switch (item.system.equipmentSubtype) {
                case "lightArmor":
                  price += materialInfo.price.lightArmor || 0;
                  break;

                case "mediumArmor":
                  price += materialInfo.price.mediumArmor || 0;
                  break;

                case "heavyArmor":
                  price += materialInfo.price.heavyArmor || 0;
                  break;
              }
              break;

            default:
              price += materialInfo.price.perPound * item.system.weight.value;
              break;
          }
        }

        // Subtract masterwork price if already included in material price
        if (gear.mwk && materialInfo?.masterwork) {
          price -= 150;
        }
        break;
      }

      case "weapon": {
        if (materialInfo) {
          price *= materialInfo.price.multiplier || 1;
        }

        if (item.name.toLowerCase() === "shuriken" || item.name.toLowerCase() === "crystal chakram") {
          price += materialInfo.price.ammunition || 0;

          if (gear.mwk && materialInfo?.masterwork) {
            price -= 6;
          }
        } else {
          switch (item.system.weaponSubtype) {
            case "light":
              price += materialInfo.price.lightWeapon || 0;
              break;

            case "1h":
              price += materialInfo.price.oneHandWeapon || 0;
              break;

            case "2h":
              price += materialInfo.price.twoHandWeapon || 0;
              break;

            case "ranged":
              price += materialInfo.price.rangedTwoHandWeapon || 0;
              break;

            default:
              price += materialInfo.price.perPound * item.system.weight.value;
              break;
          }

          // Subtract masterwork price if already included in material price
          if (gear.mwk && materialInfo?.masterwork) {
            price -= 300;
          }
        }

        break;
      }

      case "loot": {
        if (item.system.subType !== "ammo") {
          break;
        }

        if (materialInfo) {
          price *= materialInfo.price.multiplier || 1;
          if (materialInfo.price.ammunition) {
            price += materialInfo.price.ammunition;
          } else {
            price += materialInfo.price.perPound * item.system.weight.value;
          }
        }

        // Apply masterwork price if not included in material
        if (gear.mwk && !materialInfo?.masterwork) {
          price += 6;
        }

        break;
      }
    }

    if (gear.enhancementValue) {
      price += materialInfo.enhancement?.weapon || 0;
    }

    return price - basePrice;
  }

  getItemNotes() {
    const itemNotes = [];
    const actor = window.SBC.actor;
    const itemGroups = ["weapon", "equipment", "consumable", "loot", "container"];

    for (const itemGroup of itemGroups) {
      actor.itemTypes[itemGroup].forEach((item) => {
        itemNotes.push({
          name: item.name,
          quantity: item.system.quantity || 1,
          charges: item.system.uses?.per === "charges" ? item.system.uses.value : null,
        });
      });
    }

    return itemNotes;
  }

  getConsumableType(name) {
    switch (true) {
      case name.search(/^(?:potion|oil)s?/i) !== -1:
        return "potion";

      case name.search(/^wands?/i) !== -1:
        return "wand";

      case name.search(/scrolls?/i) !== -1:
        return "scroll";

      default:
        return undefined;
    }
  }

  stripQuantity(string) {
    return string
      .replace(/\(\d+\s*[,)]\s*/, "(")
      .replace("()", "")
      .trim();
  }
}
