import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";

// Parse Race
export default class RaceParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "race";
  }

  /**
   * Parse a race string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    const race = {
      name: value.toLowerCase(),
      item: value,
    };
    this.app.processData.notes.base.race = race.name.capitalize();

    let raceItem = await compendiumOps.findEntityInCompendia(race, {
      itemType: "race",
    });

    if (!raceItem) {
      // Generate a placeholder for not supported race
      const placeholderRaceItem = {
        name: value,
        type: "race",
      };
      raceItem = await sbcUtils.generatePlaceholderEntity(placeholderRaceItem, line);
    }

    if (!this.app.processData.characterData.race) {
      this.app.processData.characterData.race = pf1.utils.createTag(raceItem.name);
    }

    await createItem(raceItem);
    return true;
  }
}
