import { createItem, parseValueToDocPath } from "../../sbcParser.js";
import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

/**
 * AuraParser is a class that parses auras
 *
 * @augments AbstractParser
 */
export default class AuraParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "aura";
  }

  /**
   * Parses an aura string
   *
   * @inheritDoc
   * @param {string} value        The value to parse
   * @param {number} _line        The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, _line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    this.app.processData.notes.aura = [];

    // Send the aura line to the actor's aura trait field.
    await parseValueToDocPath(window.SBC.actor, "system.traits.aura.custom", value);

    // Split the auras into individual auras
    let auras = this.splitText(value, false);
    console.log(sbcUtils.translate("parser.aura.parseInfo", { value }), auras);

    let auraCount = 0;
    // Loop through the auras and create an item for each
    for (const auraInput of auras) {
      if (auraInput === "" || auraInput === ",") {
        continue;
      }

      // Name = Everything before the opening parenthesis or first number
      const auraName = auraInput.replace(/(\(.*\)|\d.*)/, "").trim();

      // Now check if the actor has an item with the same name
      // Short of that, look for an "Aura of X" instead.
      const auraNameLC = auraName.toLowerCase();
      const testName = sbcUtils.translate("parser.aura.testName", { value: auraNameLC }).toLowerCase();

      let foundAura = window.SBC.actor.itemTypes.feat.find(
        (item) => item.name.toLowerCase() === auraNameLC || item.name.toLowerCase() === testName
      );

      let auraNote = {
        raw: auraInput,
        name: auraName,
      };

      // Range = Numbers before ".ft"
      const auraRangeMatch = auraInput.match(/([^(,;a-zA-Z]+)ft./i);
      let auraRange = 0;
      if (auraRangeMatch) {
        auraRange = parseInt(auraRangeMatch[1].trim());
        const convertedAuraRange = pf1.utils.convertDistance(auraRange);
        auraNote.range = `${convertedAuraRange[0]} ${convertedAuraRange[1]}`;
      }

      let auraSaveType = "";
      let auraDC = "";
      let auraDCNotes = "";
      let actionType = null;
      // DC = Number after "DC"
      if (/\bDC\b/.test(auraInput)) {
        auraDC = auraInput.match(/DC\s*(\d+)/)[1];
        actionType = "save";
        auraNote.dc = auraDC;

        // auraDCNotes, e.g. negates, halves
        if (/DC\s*\d+\s*([^)(,;0-9]+)/.test(auraInput)) {
          auraDCNotes = auraInput.match(/DC\s*\d+\s*([^)(,;0-9]+)/)[1];
          auraNote.dcNotes = auraDCNotes;
        }

        if (/\s+([^)(,;]+)DC\s*\d+/.test(auraInput)) {
          auraSaveType = auraInput
            .match(/\s+([^)(,;]+)DC\s*\d+/)[1]
            .trim()
            .toLowerCase();
          auraNote.saveType = sbcUtils.translate(`preview.${auraSaveType.capitalize()}`);
        }
      }

      // Remove anything that we've parsed out from the input string, so all that's left should be the notes.
      const auraEffectPatternString =
        "(\\b" +
        [auraName, auraRange, auraSaveType, auraDC, auraDCNotes, "DC"].filter((x) => !!x).join("\\b|\\b") +
        "\\b|ft\\.|[(),])";

      // Compile and run the regex to extract our effects
      const auraEffectPattern = new RegExp(auraEffectPatternString, "gi");
      const auraEffectRaw = auraInput.replace(auraEffectPattern, "").trim();
      auraNote.effect = auraEffectRaw;

      if (foundAura) {
        this.app.processData.notes.aura.push(auraNote);
      } else {
        let searchEntity = {
          search: new Set([auraName, `Aura of ${auraName}`]), // The search strings
          item: auraInput,
        };
        let foundAura = await compendiumOps.findEntityInCompendia(searchEntity, {
          itemType: "feat",
          classes: this.app.processData.characterData.classes,
          race: this.app.processData.characterData.race,
        });

        if (foundAura) {
          await sbcUtils.addAssociatedClass(foundAura, sbcInstance.actor.itemTypes.class);

          await createItem(foundAura);
          this.app.processData.notes.aura.push(auraNote);
        } else {
          const auraEffect = sbcUtils.makeValueRollable(auraEffectRaw);

          // Set up the aura action data for use in the aura item
          const auraAction = {
            ...pf1.components.ItemAction.defaultData,
            actionType: actionType,
            activation: {
              cost: null,
              type: "passive",
              unchained: {
                cost: null,
                type: "passive",
              },
            },
            duration: {
              units: "perm",
            },
            effectNotes: auraEffect,
            measureTemplate: {
              type: "circle",
              size: auraRange,
            },
            name: sbcUtils.translate("parser.aura.placeholderActionName"),
            range: {
              value: auraRange,
              units: "ft",
            },
            save: {
              dc: auraDC,
              type: auraSaveType,
              description: auraDCNotes,
            },
            target: {
              value: sbcUtils.translate("parser.aura.placeholderTarget"),
            },
          };

          // Set up the aura data to create the item on the actor
          const aura = new Item.implementation({
            name: sbcUtils.translate("parser.aura.placeholderName", { value: auraName.capitalize() }),
            type: "feat",
            system: {
              abilityType: "na",
              actions: [auraAction],
              subType: "racial",
              tag: `aura${auraCount}`,
            },
            img: "systems/pf1/icons/spells/runes-blue-3.jpg",
          });

          await createItem(aura);

          this.app.processData.notes.aura.push(auraNote);
          auraCount++;
        }
      }
    }

    this.app.processData.notes.aura = this.app.processData.notes.aura.sort((a, b) => a.name.localeCompare(b.name));
    return true;
  }
}
