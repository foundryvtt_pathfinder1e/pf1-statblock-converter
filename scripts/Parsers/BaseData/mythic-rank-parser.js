import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem, parseValueToDocPath } from "../../sbcParser.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

// Parse Classes
export default class MythicRankParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "mythic";
  }

  /**
   * Parses a creature's mythic rank
   *
   * @param {number} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    value = parseInt(value) || 0;
    if (value === 0) {
      return true;
    }

    // Put the raw value into the notes, to be used in the preview
    this.app.processData.notes.base.mr = value;
    sbcUtils.log(sbcUtils.translate("parser.mr.parseInfo", { value }));

    const rankData = {
      name: "Mythic Rank",
      item: "Mythic Rank",
    };

    /** @type {import("../../../pf1/module/documents/item/item-pf.mjs").ItemPF} */
    const rankItemData = await compendiumOps.findEntityInCompendia(rankData, {
      itemType: "class",
      classes: this.app.processData.characterData.classes,
    });

    const classItem = window.SBC.actor.itemTypes.class.find((el) => el.system.subType === "racial");
    const tierHDSize = Math.min(10, +classItem?.system?.hd ?? 6); // Mythic HP per tier caps at 10, even on a d12.

    const [_result, rankItem] = await createItem(rankItemData);

    // Prepare item updates for the actor, starting with the mythic rank item
    let itemUpdates = [
      { _id: rankItem.id, "system.level": value, "system.hp": tierHDSize * value, "system.hd": tierHDSize },
    ];

    // Now that the mythic rank is added, we should now have a couple items added to the actor to manage:
    // Mythic Power and Surge
    /** @type {import("../../../pf1/module/documents/item/item-feat.mjs").ItemFeatPF} */
    let surgeItem = window.SBC.actor.itemTypes.feat.find(
      (el) => el.system.subType === "misc" && el.system.tag === "surgeMythic"
    );
    /** @type {import("../../../pf1/module/documents/item/item-feat.mjs").ItemFeatPF} */
    let powerItem = window.SBC.actor.itemTypes.feat.find(
      (el) => el.system.subType === "misc" && el.system.tag === "mythicPowerMythic"
    );

    // Wait for the items to be created, or time out after 10 seconds
    let timeoutCounter = 0;
    while ((!surgeItem || !powerItem) && timeoutCounter < 10) {
      await new Promise((resolve) => {
        timeoutCounter++;
        setTimeout(resolve, 1000);
      });
      surgeItem = window.SBC.actor.itemTypes.feat.find(
        (el) => el.system.subType === "misc" && el.system.tag === "surgeMythic"
      );
      powerItem = window.SBC.actor.itemTypes.feat.find(
        (el) => el.system.subType === "misc" && el.system.tag === "mythicPowerMythic"
      );
    }

    // Prepare updates for the surge item with the size of the surge die
    if (surgeItem) {
      const surgeValue = 6 + 2 * Math.floor((value - 1) / 3);
      const surgeAction = surgeItem.actions.get(surgeItem.actions.contents[0].id);

      await surgeAction.update({ effectNotes: [`+[[1d${surgeValue}]] Surge`] });
    }

    // Prepare updates for the power item to link the surge item's charges to the poewr item
    if (powerItem) {
      itemUpdates.push({ _id: powerItem.id, "system.uses.maxFormula": `${value}`, "system.uses.value": +value });
    }

    // Update the items all at once
    await window.SBC.actor.updateEmbeddedDocuments("Item", itemUpdates);

    surgeItem?.prepareData();
    await powerItem?.recharge({ value: +value, maximize: true, commit: true, period: "any" });
    await powerItem?.createItemLink("charges", surgeItem, `.Item.${surgeItem.id}`);

    // Adjust the actor's CR base to normal (gotta offset the added CR from the mythic rank)
    await parseValueToDocPath(
      window.SBC.actor,
      "system.details.cr.base",
      window.SBC.actor.system.details.cr.base - Math.floor(value / 2)
    );

    // The mythic rank item was created successfully
    return true;
  }
}
