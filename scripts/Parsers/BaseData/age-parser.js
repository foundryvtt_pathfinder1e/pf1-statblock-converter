import { AbstractParser } from "../abstract-parser.js";

export default class AgeParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "age";
  }

  /**
   * Parses a sense string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    let label;
    let statValues = { phys: 0, ment: 0 };

    switch (value) {
      case "Venerable":
        label = "SBCPF1.parser.age.venerable";
        value = "venerable";
        statValues.phys = -6;
        statValues.ment = 3;

        break;

      case "Old":
        label = "SBCPF1.parser.age.old";
        value = "old";
        statValues.phys = -3;
        statValues.ment = 2;

        break;

      case "Middle[- ]Aged?":
        label = "SBCPF1.parser.age.middleAged";
        value = "middleAge";
        statValues.phys = -1;
        statValues.ment = 1;
        break;

      case "Young":
        label = "SBCPF1.parser.age.young";
        value = "young";
        break;

      default:
        label = "SBCPF1.parser.age.adult";
        value = "adult";
    }

    label = game.i18n.localize(label);

    await window.SBC.actor.update({
      "system.details.age": label,
      "system.traits.ageCategory": value,
    });

    this.app.processData.notes.age = label;
    return true;
  }

  /**
   * Get the ability score modifier based on the age category
   *
   * @param {("str"|"con"|"dex"|"int"|"wis"|"cha")} ability The ability score to get the modifier for
   * @returns {number} The modifier for the ability score
   */
  getAbilityChange(ability) {
    const isPhysical = ["str", "dex", "con"].includes(ability);
    switch (window.SBC.actor.system.traits.ageCategory.base) {
      case "venerable":
        return isPhysical ? -6 : 3;
      case "old":
        return isPhysical ? -3 : 2;
      case "middleAge":
        return isPhysical ? -1 : 1;
      case "young": {
        switch (ability) {
          case "con":
          case "str":
          case "wis":
            return -2;
          case "dex":
            return 2;
          default:
            return 0;
        }
      }
      default:
        return 0;
    }
  }
}
