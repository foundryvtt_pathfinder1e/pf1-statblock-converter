import { checkFlags, createItem, parseSubtext } from "../../sbcParser.js";
import { AbstractParser } from "../abstract-parser.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";
import { sbcUtils } from "../../sbcUtils.js";

// Parse Creature Type and Subtype
export default class CreatureParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "creatureType";
  }

  /**
   * Parses a creature type string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   */
  async _parse(value, line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;
    const tempCreatureType = parseSubtext(value);

    const creatureType = {
      name: tempCreatureType[0],
      type: "racial",
      subTypes: "",
      item: tempCreatureType[0],
    };

    if (tempCreatureType.length > 1) {
      creatureType.subTypes = tempCreatureType[1];
    }

    // Always search the english compendia for entries, so use the english creatureType instead of the localized one
    const creatureTypeItem = await compendiumOps.findEntityInCompendia(creatureType, {
      itemType: "class",
    });
    let updates = {
      system: {
        tag: pf1.utils.createTag(creatureTypeItem.name),
        useCustomTag: true,
        level: 0,
      },
    };

    // Set flags for the conversion
    switch (creatureType.name.toLowerCase()) {
      case "undead":
        this.app.processData.flags.isUndead = true;
        await checkFlags(this.app.processData, "isUndead");
        break;

      case "construct":
        this.app.processData.flags.isConstruct = true;
        break;

      default:
        break;
    }

    if (creatureType.subTypes !== "") {
      Object.assign(updates, {
        name: tempCreatureType[0].capitalize() + " (" + creatureType.subTypes.capitalize() + ")",
      });
    }

    await creatureTypeItem.updateSource(updates);

    this.app.processData.notes.creatureType = {
      type: sbcUtils.translate(`types.${sbcUtils.camelize(tempCreatureType[0])}`),
      subTypes: creatureType.subTypes.split(",").map((subType) => {
        subType = subType.trim();

        const existingSubType = sbcUtils.haystackSearch(window.SBC.config.creatureSubTypes, subType);
        if (existingSubType) {
          return sbcUtils.translate(`subTypes.${sbcUtils.camelize(existingSubType)}`);
        }

        return subType.capitalize();
      }),
    };
    await createItem(creatureTypeItem);

    if (!this.app.processData.characterData.race) {
      this.app.processData.characterData.race = creatureTypeItem.system.tag;
    }

    return true;
  }
}
