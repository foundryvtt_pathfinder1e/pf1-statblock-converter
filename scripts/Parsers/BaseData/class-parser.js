import { sbcUtils } from "../../sbcUtils.js";
import { AbstractParser } from "../abstract-parser.js";
import { createItem } from "../../sbcParser.js";
import {
  baseClassesRegex,
  classesRegex,
  mythicPathRegex,
  prestigeClassesRegex,
  wizardClassesRegex,
} from "../../sbcRegex.js";

// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "../../sbcTypes.js";

const ClassType = Object.freeze({
  PrestigeClass: "PrestigeClass",
  WizardClass: "WizardClass",
  SupportedClass: "SupportedClass",
  MythicPath: "MythicPath",
});

// Parse Classes
export default class ClassParser extends AbstractParser {
  /**
   * @inheritDoc
   */
  get parserGroup() {
    return "Parser/Base";
  }

  /**
   * @inheritDoc
   */
  get parserType() {
    return "class";
  }

  /**
   * Parses a class string
   *
   * @param {string} value        The value to parse
   * @param {number} line         The line number
   * @param {string} _type        The type of value
   * @returns {Promise<boolean>}  Always true, unless an error occurs
   * @private
   */
  async _parse(value, line, _type = undefined) {
    /** @type {sbcTypes.SBC} */
    const sbcInstance = window.SBC;
    const compendiumOps = sbcInstance.compendiumSearch;

    // Put the raw class info into the notes, to be used in the preview
    this.app.processData.notes.base.classes = [];

    value = value.replaceAll(/\sArchetype|\sPrestige Class/gi, "");
    // split the classes and handle each class separately
    let classes = value.split(/\//).map((el) => el.trim());

    for (let classInput of classes) {
      const classInfo = classesRegex().exec(classInput).groups;
      let isExClass = !!classInfo.exClass;

      let classType = null;
      if (classInput.match(prestigeClassesRegex())) {
        classType = ClassType.PrestigeClass;
      } else if (classInput.match(baseClassesRegex())) {
        classType = ClassType.SupportedClass;
      } else if (classInput.match(wizardClassesRegex())) {
        classType = ClassType.WizardClass;
      } else if (classInput.match(mythicPathRegex())) {
        classType = ClassType.MythicPath;
      }

      if (!classType) {
        this.throwError(undefined, "parser.class.failedToCreate", { class: classInput });
        continue;
      }
      sbcUtils.log(sbcUtils.translate("parser.class.parseInfo", { classType: classType }));

      // Supported Class, Prestige Class or Wizard Class found
      const tempClassName = sbcUtils.parseSubtext(classInput.trim());
      if (tempClassName.length > 2) tempClassName[0] += " " + tempClassName[2][0];

      const classData = {
        name: classType === ClassType.WizardClass ? "wizard" : classInfo.class,
        deity: classInfo.deity,
        wizardClass: classType === ClassType.WizardClass ? classInfo.class : "",
        // Match archetype only if it includes no number, otherwise it's a source
        archetype: classInfo.archetype?.match(/\d+/) ? "" : classInfo.archetype || "",
        level: parseInt(classInfo.level),
        item: tempClassName[0].replaceAll(/[0-9]/g, "").trim(),
      };
      console.log(classData);

      const classItem = await compendiumOps.findEntityInCompendia(classData, {
        itemType: "class",
        classes: this.app.processData.characterData.classes,
      });

      if (!classItem) {
        sbcUtils.log(sbcUtils.translate("parser.class.notFound", { className: classData.name }));
        continue;
      }
      if (classItem && classItem.system.subType !== "racial" && classData.level === 0) continue;

      let className = classData.name;
      let updates = {};

      if (classData.deity) {
        classData.name += ` of ${classData.deity}`;
        await window.SBC.actor.update({ "system.details.deity": classData.deity });
      }

      // If the suffix contains an "of" the probability it names a deity is high. So, set that and hope for the best
      if (classData.archetype) {
        const archetypes = this.splitText(classData.archetype, false)
          .map((el) => el.trim().capitalize())
          .filter((el) => el !== "");

        await sbcUtils.runParallelOps(archetypes, async (archetype) => {
          const fullArchetypeName = `${classData.name.capitalize()} (${archetype})`;
          const archetypeItem = await compendiumOps.findEntityInCompendia(
            { name: fullArchetypeName, item: fullArchetypeName },
            {
              itemType: "feat",
              itemSubType: "misc",
            }
          );
          if (archetypeItem) {
            await createItem(archetypeItem);
          }
        });
        className = classData.name.capitalize() + " (" + archetypes.join(", ") + ")";
        sbcInstance.app.processData.characterData.archetypes.push(...archetypes);
      } else if (classData.wizardClass) {
        className = sbcUtils.translate(`wizardSchools.${classData.wizardClass.toLowerCase()}`);
        Object.assign(updates, {
          "system.tag": "wizard",
          "system.useCustomTag": true,
        });
      } else {
        className = classData.name.capitalize();
      }

      const classSubtype = classItem.system.subType;
      if (classSubtype !== "mythic") {
        const hdMaximized = this.app.processData.characterData.hdMaximized;
        const hdAverage = sbcUtils.getDiceAverage(+classItem.system.hd);
        classData.hp =
          !hdMaximized && classSubtype === "base"
            ? +classItem.system.hd + +Math.floor(hdAverage * (classData.level - 1))
            : hdAverage * classData.level;

        if (!hdMaximized && classSubtype === "base") {
          this.app.processData.characterData.hdMaximized = true;
        }
      } else {
        classData.hp = +classItem.system.hd * classData.level;
      }

      Object.assign(updates, {
        name: (isExClass ? "Ex-" : "") + className,
        "system.level": classData.level,
        "system.hp": classData.hp,
      });

      // Save the Class Associations to restore later (this prevents supplement items being added)
      // If supplements are important, they'll be in the statblock and added by SBC itself.
      let classAssociations = [];
      if (classItem.system.links?.classAssociations && !window.SBC.settings.getSetting("createAssociations")) {
        classAssociations = classItem.system.links.classAssociations;

        Object.assign(updates, {
          "system.links.-=classAssociations": null,
        });
      }

      // Update the classItem with the new data
      await classItem.updateSource(updates);

      let [_type, book] = await createItem(classItem);
      this.app.processData.notes.base.classes.push(`${isExClass ? "Ex-" : ""}${className} ${classData.level}`);

      // Add the class identifier to the list of classes on the actor.
      if (!this.app.processData.characterData.classes.includes(book.system.tag)) {
        this.app.processData.characterData.classes.push(book.system.tag);
      }

      // Restore the class associations
      if (classAssociations.length > 0) {
        await book.update({
          "system.links.classAssociations": classAssociations,
        });
      }
    }

    // classItems were created successfully
    return true;
  }
}
