// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

import {
  CreatureSubTypes,
  CreatureTypes,
  Immunities,
  ItemModifications,
  MagicalAbilities,
  Metamagic,
  SillyGear,
  Sources,
  TechColors,
} from "./Content/index.js";

/**
 * This file contains the configuration for the PF1 Statblock Converter module.
 *
 * @type {sbcTypes.ConfigData} sbcConfig   The configuration data for the PF1 Statblock Converter module
 */
export const sbcConfig = {
  modData: {
    version: "5.0.0",
    mod: "pf1-statblock-converter",
    modName: "sbc | PF1 Statblock Converter",
  },
  const: {
    lineheight: 20,
    crFractions: {
      "1/8": 0.125,
      "1/6": 0.1625,
      "1/4": 0.25,
      "1/3": 0.3375,
      "1/2": 0.5,
    },
    actorType: {
      0: "npc",
      1: "character",
      // 2: "trap",
      // 3: "haunt",
      // 4: "vehicle",
    },
    suffixMultiples: ["st", "nd", "rd", "th"],
  },
  sources: Sources,
  lineCategories: [
    "Defense",
    "Offense",
    "Statistics",
    "Special Abilities",
    "Description",
    "Tactics",
    "Ecology",
    "([a-z ]*)?Spells",
    "([a-z ]*)?Extracts",
    "([a-z ]*)?Spell-Like",
  ],
  lineStarts: [
    "Source",
    "CR",
    "XP",
    "[LC][GNE]", // LG, CG, NG, LN, CN, N, LE, CE
    "N[GE]?", // N, NG, NE
    "Init",
    "Aura",
    "Senses",
    "Defense",
    "AC",
    "HP",
    "Fort",
    "Resist",
    "Immun(?:e|ities)", // Immune, Immunities
    "DR",
    "Offense",
    "Speed",
    "Spd",
    "Melee",
    "Ranged",
    "Space",
    "Special",
    "^Spell-Like",
    "^(.*) Spell-Like",
    "^Spells",
    "^(.*) Spells",
    "^Extracts",
    "^(.*) Extracts",
    "Constant",
    "At[- ]will",
    "[456789]th", // 4th, 5th, 6th, 7th, 8th, 9th
    "3rd",
    "2nd",
    "1st",
    "Cantrips",
    "Orisons",
    "0",
    "D\\s*domain spell",
    "Domains?",
    "Myster[y|ies]",
    "S\\s*spirit magic spell",
    "Spirits?",
    "Bloodlines?",
    "Thassilonian Specialization",
    "Patrons?",
    "Statistics",
    "Str",
    "Base",
    "Feats",
    "Skills",
    "Languages?", // Language, Languages
    "Ecology",
    "Environment",
    "Organization",
    "Treasure",
    "S[QR]", //SQ, SR
    "(?:Other |Combat )?Gear", // Other Gear, Combat Gear
    "Gear",
    "(?:M|Fem)ale", // Male, Female
    "Tactics",
    "(?:Before|During) Combat",
    "Morale",
    "(?:Opposition|Prohibited) School", // Opposition School, Prohibited School
    "Defensive Abilities",
    "Immune",
    "Weaknesses",
    "Description",
    "\\d+\\s*(?:rounds)?\\/(day|month|year)",
    ".*\\((S[PU]|EX|\\-\\-)\\)", // (Sp), (Su), (Ex), (--),
    "\\*",
  ],
  armorBonusTypes: [
    "touch",
    "flat-footed",
    "natural",
    "size",
    "dex",
    "armor",
    "shield",
    "base",
    "enhancement",
    "dodge",
    "inherent",
    "deflection",
    "morale",
    "luck",
    "sacred",
    "insight",
    "resistance",
    "profane",
    "trait",
    "racial",
    "competence",
    "circumstance",
    "alchemical",
    "penalty",
    "rage",
    "monk",
    "wis",
    "untyped",
  ],
  techColors: TechColors,
  techTiers: [
    "Mark (?:I{1,3}|I?V)", // Mark I, Mark II, Mark III, Mark IV, Mark V
    "Grade (?:I{1,3}|I?V)", // Grade I, Grade II, Grade III, Grade IV, Grade V
  ],
  magicalAbilities: MagicalAbilities,
  itemModifications: ItemModifications,
  weaponGroups: {},
  metamagic: Metamagic,
  creatureTypes: CreatureTypes,
  creatureSubTypes: CreatureSubTypes,
  immunities: Immunities,
  sillyGear: SillyGear,
  races: [],
  classes: [],
  mythicPaths: [],
  racialClasses: [],
  prestigeClassNames: [],
  templates: [],
  naturalAttacks: [],
  initializeConfig: async function () {
    // /* ------------------------------------ */
    // /* sbc configuration               		*/
    // /* ------------------------------------ */
    // If there are customCompendiums, given as a string in the module settings,
    // split them and add them to the searchableCompendiums
    let searchableCompendiums = window.SBC.compendiumSearch.defaultCompendia;
    /** @type {string} */
    let customCompendiumSettings = window.SBC.settings.getSetting("customCompendiums");

    // Gather and add all the custom compendiums
    if (customCompendiumSettings !== "") {
      customCompendiumSettings = customCompendiumSettings.replace(/\s/g, "");
      searchableCompendiums = searchableCompendiums.concat(customCompendiumSettings.split(/[,;]/g));
    }

    // Search through all the compendiums to process the class items
    await indexCompendiums(searchableCompendiums);
    this.classes.sort();
    this.mythicPaths.sort();
    this.racialClasses.sort();
    this.prestigeClassNames.sort();

    // Grab the natural attacks introduced in the system.
    let naturalAttacksIndex = await game.packs.get("pf1.monster-abilities").index;
    for (let entry of naturalAttacksIndex) {
      if (entry.name !== "") {
        sbcConfig.naturalAttacks.push(entry.name);
      }
    }

    // Add the weapon groups to the config
    for (const [group, name] of Object.entries(pf1.config.weaponGroups)) {
      let fixedName = name.toLowerCase().replace(/(.*), (.*)/, "$2 $1");
      if (!Object.values(sbcConfig.weaponGroups).includes(name)) sbcConfig.weaponGroups[group] = fixedName;
    }
  },
};

// Wrap the entire operation in an async function to use await
/**
 * Indexes the compendiums to get the classes, mythic classes, racial classes, and prestige
 *
 * @param {Array<string>} searchableCompendiums   The compendiums to search through
 */
async function indexCompendiums(searchableCompendiums) {
  let classSet = new Set();
  let prestigeClassSet = new Set();
  let mythicClassSet = new Set();
  let racialClassSet = new Set();
  let raceSet = new Set();
  let templateSet = new Set();

  // Use map to iterate and return an array of promises
  const compendiumPromises = searchableCompendiums.map(async (compendium) => {
    let pack = await game.packs.get(compendium);
    if (!pack) {
      return;
    }

    let index = pack.index;
    // Use Promise.all to wait for all index entries to be processed
    await Promise.all(
      index.map(async (entry) => {
        if (entry.name === "") {
          return;
        }

        switch (entry.type) {
          case "class":
            switch (entry.system?.subType) {
              case "prestige":
                prestigeClassSet.add(entry.name);
                break;

              case "mythic":
                mythicClassSet.add(entry.name);
                break;

              case "racial":
                racialClassSet.add(entry.name);
                break;

              default:
                classSet.add(entry.name);
                break;
            }
            break;

          case "race":
            raceSet.add(entry.name);
            break;

          case "feat":
            switch (entry.system?.subType) {
              case "template":
                templateSet.add(entry.name.split("(")[0].replace("Creature", "").trim());
                break;
            }
        }
      })
    );
  });

  // Wait for all compendiums to be processed
  await Promise.all(compendiumPromises);

  // Add the classes to the config after all compendiums have been processed
  sbcConfig.classes = Array.from(classSet).filter((el) => !!el);
  sbcConfig.mythicPaths = Array.from(mythicClassSet).filter((el) => !!el);
  sbcConfig.racialClasses = Array.from(racialClassSet).filter((el) => !!el);
  sbcConfig.prestigeClasses = Array.from(prestigeClassSet).filter((el) => !!el);
  sbcConfig.races = Array.from(raceSet);
  sbcConfig.templates = Array.from(templateSet);
}
