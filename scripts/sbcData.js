// eslint-disable-next-line no-unused-vars
import * as sbcTypes from "./sbcTypes.js";

export class sbcData {
  /**
   * Get the default process data
   *
   * @returns {sbcTypes.ProcessData} Default process data
   */
  static get defaultData() {
    return {
      actorReady: false,
      appData: {},
      characterData: {
        conversionValidation: {
          context: {},
          attributes: {},
          skills: {},
          spellBooks: {},
        },
        weaponFocus: [],
        weaponSpecialization: [],
        weaponFocusGreater: [],
        weaponSpecializationGreater: [],
        classes: [],
        archetypes: [],
        race: null,
        weaponGroups: {},
        hdMaximized: false,
        prestigeClassCasting: new Map(),
      },
      changes: {},
      errors: [],
      flags: {
        hasNoCMB: false,
        hasNoCMD: false,
        noStr: false,
        noDex: false,
        noCon: false,
        noInt: false,
        noWis: false,
        noCha: false,
        isUndead: false,
        isConstruct: false,
        hasWeaponFinesse: false,
      },
      foundCategories: 0,
      parsedCategories: 1,
      imported: false,
      input: {
        data: [],
        text: "",
        html: "",
        prepared: {
          data: [],
          success: false,
        },
        parsedSuccess: false,
      },
      miscNotes: {
        acNotes: [],
        cmdNotes: "",
        saveNotes: "",
        sr: 0,
        srNotes: "",
        naturalAC: 0,
        spellbookNotes: [],
      },
      notes: {},
      treasureParsing: {
        treasureToParse: "",
        lineToRemove: 0,
        statisticsStartLine: 0,
      },
      progressBar: {
        class: "",
        text: "",
        width: 0,
      },
    };
  }
}
