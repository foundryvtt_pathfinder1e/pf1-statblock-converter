import { NaturalAttacks, WizardSchoolClasses } from "./Content/index.js";

/**
 * @param {Array<string>|{[key: string]: string}} list        An item list to include in the regex.
 * @param {object} options                                    Regex configuration options
 * @param {boolean} [options.pluralize]                       Whether to pluralize the items in the list.
 * @param {boolean} [options.matchStart]                      Whether to match the start of the string.
 * @param {boolean} [options.matchEnd]                        Whether to match the end of the string.
 * @param {boolean} [options.wordBoundary]                    Whether to include word boundaries.
 * @param {boolean|string} [options.capture]                  Whether to capture the match, alternatively a string to name the capture group.
 * @param {string} [options.append]                           Text to append to the regex.
 * @param {string} [options.prepend]                          Text to prepend to the regex.
 * @param {string} [options.flags]                            Regular expression flags.
 * @param {boolean} [options.escape]                          Whether to escape the items in the list.
 * @param {string} [options.appendMatch]                      Text to append each item in the list.
 *
 * @returns {RegExp}                                          The final regular expression.
 */
function basicAlterationRegex(list, options = {}) {
  options = Object.assign(
    {
      pluralize: false,
      matchStart: false,
      matchEnd: false,
      wordBoundary: true,
      capture: true,
      escape: true,
      append: "",
      prepend: "",
      flags: "gi",
      appendMatch: "",
    },
    options
  );

  if (!Array.isArray(list)) list = Object.values(list);
  list = list
    .filter((x, i, a) => a.indexOf(x) === i) // Unique only
    .sort((a, b) => b.length - a.length) // Sort by length, desc
    .map((x) => (options.escape ? escapeForRegex(x) : x)) // Escape regex characters
    .map((x) => (options.appendMatch ? x + options.appendMatch : x)) // Escape regex characters
    .filter((x) => x.length > 0); // Remove empty strings

  return new RegExp(
    (options.matchStart ? "^" : "") +
      options.prepend +
      (options.wordBoundary ? "\\b" : "") +
      (options.capture ? (typeof options.capture === "string" ? `(?<${options.capture}>` : "(") : "(?:") +
      (list.length ? list.join(options.pluralize ? "s?|" : "|") : "_") +
      (options.pluralize ? "s?" : "") +
      ")" +
      (options.wordBoundary ? "\\b" : "") +
      options.append +
      (options.matchEnd ? "$" : ""),
    options.flags
  );
}

/**
 * Escapes a given string for regex
 *
 * @param {string} string     String to escape
 * @returns {string} string     Escaped string
 */
export function escapeForRegex(string) {
  return string.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
}

/**
 *
 * @param {boolean} includeExtras   Whether to include extra class information
 * @returns {RegExp} A regular expression.
 */
export function classesRegex(includeExtras = true) {
  const possibleClasses = window.SBC.config.classes
    .concat(window.SBC.config.prestigeClasses)
    .concat(window.SBC.config.mythicPaths)
    .concat(WizardSchoolClasses);

  return basicAlterationRegex(possibleClasses, {
    capture: "class",
    prepend: includeExtras ? "(?<exClass>Ex-)?" : "",
    append: includeExtras ? "(\\s+of\\s+(?<deity>[a-z- ]+))?\\s*(?: \\((?<archetype>.*)\\))?\\s+(?<level>\\d+)" : "",
  });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function baseClassesRegex() {
  return basicAlterationRegex(window.SBC.config.classes);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function prestigeClassesRegex() {
  return basicAlterationRegex(window.SBC.config.prestigeClasses);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function wizardClassesRegex() {
  return basicAlterationRegex(WizardSchoolClasses);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function mythicPathRegex() {
  return basicAlterationRegex(window.SBC.config.mythicPaths);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function acTypesRegex() {
  return basicAlterationRegex(window.SBC.config.armorBonusTypes);
}

/**
 * @returns {RegExp} A regular expression.
 *
 * @param {boolean} [wholeWord] Whether to match whole words only, default is false.
 */
export function damageTypesRegex(wholeWord = false) {
  const systemSupportedDamageTypes = Object.values(pf1.registry.damageTypes.getLabels())
    .concat(Object.values(pf1.registry.materials.getLabels()))
    .concat(Object.values(pf1.config.damageResistances));
  return basicAlterationRegex(systemSupportedDamageTypes, wholeWord ? { matchStart: true, matchEnd: true } : {});
}

/**
 * @returns {RegExp} A regular expression.
 */
export function conditionTypesRegex() {
  return basicAlterationRegex(pf1.config.conditionTypes);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function conditionRegistryTypesRegex() {
  return basicAlterationRegex(pf1.registry.conditions.getLabels());
}

/**
 * @returns {RegExp} A regular expression.
 */
export function naturalAttacksRegex() {
  // Natural Attack Pattern for finding attacks
  const naturalAttackItems = window.SBC.config.naturalAttacks.concat(Object.keys(NaturalAttacks));
  return basicAlterationRegex(naturalAttackItems, { pluralize: true, wordBoundary: false });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function materialsRegex() {
  return basicAlterationRegex(pf1.registry.materials.contents.filter((mt) => mt.addon === false).map((mt) => mt.name));
}

/**
 * @returns {RegExp} A regular expression.
 */
export function materialsBasicRegex() {
  return basicAlterationRegex(pf1.registry.materials.contents.filter((mt) => mt.basic).map((mt) => mt.name));
}

/**
 * @returns {RegExp} A regular expression.
 */
export function materialsNonBasicRegex() {
  return basicAlterationRegex(
    pf1.registry.materials.contents.filter((mt) => mt.addon === false && mt.basic === false).map((mt) => mt.name)
  );
}

/**
 * @returns {RegExp} A regular expression.
 */
export function materialAddonsRegex() {
  return basicAlterationRegex(pf1.registry.materials.contents.filter((mt) => mt.addon === true).map((mt) => mt.name));
}

/**
 * @returns {RegExp} A regular expression.
 */
export function skillsRegex() {
  return basicAlterationRegex(pf1.config.skills, { wordBoundary: false });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function racialModifiersRegex() {
  return basicAlterationRegex(pf1.config.skills, {
    capture: "name",
    prepend: "\\+?(?<value>-?\\d*)\\s*",
    append: "(?<context>.*)",
    flags: "i",
    wordBoundary: false,
  });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function parensMergeRegex() {
  return /\d+d\d+|\d+\s?(?:hp|damage|minutes?)|[AD]C\s?\d+|\d+\s*(?:rounds)?\/(?:day|week|month|year)|at[ -]will|constant|\d+-ft|\d+\s*natural|(lesser|greater|minor|major)|[-+]?\d/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function frequencyWithPeriodRegex() {
  return /(?<uses>\d+(?!\d*,\s*))\s*(?<useTerm>.*?)?\/(?<period>day|week|month|year)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function languagesRegex() {
  const systemSupportedLanguages = Object.values(pf1.config.languages).map((x) => x.toLowerCase());
  return basicAlterationRegex(systemSupportedLanguages);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function supportedSpellsRegex() {
  return /(potion|wand|scroll|oil)s?\s+of\s+(.+)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function techColorRegex() {
  return basicAlterationRegex(window.SBC.config.techColors, {
    append: "(?!\\s+Powder)",
  });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function techTierRegex() {
  return basicAlterationRegex(window.SBC.config.techTiers, {
    escape: false,
  });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function quantityOfRegex() {
  // TODO: There's probably more terms that we need to transform like this
  return /^(Vial|Keg|Dose|Cup)s? of (.*)$/gi;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function magicalAbilitiesRegex() {
  const magicAbilityKeys = Object.keys(window.SBC.config.magicalAbilities.Abilities).map((magicalAbilityKey) => {
    return magicalAbilityKey
      .replace("_", "")
      .replace(/([A-Z])/g, " $1")
      .trim()
      .replaceAll(" ", "[ -]?");
  });
  const prefixes = window.SBC.config.magicalAbilities.Prefixes;

  return basicAlterationRegex(magicAbilityKeys, {
    escape: false,
    // wordBoundary: false,
    capture: "ability",
    prepend: `(?<prefix>(?:${prefixes.join("|")})\\s+)?`,
    append: `(?<postfix>(?:\\s+\\(|,\\s+)(?:${prefixes.join("|")})\\)?)?`,
  });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function itemModificationsRegex() {
  return basicAlterationRegex(window.SBC.config.itemModifications, { escape: false });
}

/**
 * @returns {RegExp} A regular expression.
 */
export function goldRegex() {
  return /(?:(?<pp>\d+)\s*pp)?\s*(?:(?<gp>\d+)\s*gp)?\s*(?:(?<sp>\d+)\s*sp)?\s*(?:(?<cp>\d+)\s*cp)?/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function specialAbilityTypesRegex() {
  return /\((--|S[UP]|EX)\)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function specialAbilityRegex() {
  return /^(?<name>.+?)\s*\((?<type>--|S[UP]|EX)\)\s*(?<description>.+?)$/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function savesRegex() {
  return /(?<type>Fort|Ref|Will)\s*(?<value>[+-]?\d+)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function sizeRegex() {
  return basicAlterationRegex(pf1.config.actorSizes);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function genderRegex() {
  return /\b(?:fe)?male\b/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function alignmentRegex() {
  return /^(Any Alignment|\\*A|[LNC][GE]|[LC]?N)\b/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function creatureTypeRegex() {
  return basicAlterationRegex(window.SBC.config.creatureTypes);
}

/**
 * @returns {RegExp} A regular expression.
 */
export function creatureTypeAndSizeRegex() {
  return new RegExp(
    "\\b(?:" +
      Object.values(pf1.config.actorSizes).join("|") +
      ")\\b\\s*\\b(" +
      Object.values(window.SBC.config.creatureTypes).join(".*|") +
      ")\\b",
    "i"
  );
}

/**
 * @returns {RegExp} A regular expression.
 */
export function abilityDCregex() {
  return /(?<type>Fortitude|Reflex|Will)?\s*DC\s*(?<dc>\d+)?\s*(?<effect>.*)?/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function abilityDamageRegex() {
  return /(?<formula>\d+d\d+\s*\+?\s*\d*)\s*(?<type>.*damage?)?\s*(?<effect>.*)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function abilityRangeRegex() {
  return /(?<range>\d+).*?ft.*\s*(?<type>cone|ray|radius|diameter)/i;
}

/**
 * @returns {RegExp} A regular expression.
 */
export function fluffGearTermRegex() {
  return basicAlterationRegex(["battered", "ragged and torn", "tattered"], {
    matchStart: true,
    wordBoundary: false,
  });
}
