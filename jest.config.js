/** @type {import("jest").Config} */
export default {
  collectCoverageFrom: ["scripts/**/*.js", "utils/*.js"],
  coverageReporters: ["cobertura", "html", "text", "lcovonly"],
  setupFilesAfterEnv: [
    "<rootDir>/tests/setupFoundryGlobals.js",
    "<rootDir>/tests/setupExtensions.js",
    "<rootDir>/tests/setUp.js",
  ],
  // Refer https://jestjs.io/docs/configuration#testenvironment-string
  testEnvironment: "jsdom",
};
